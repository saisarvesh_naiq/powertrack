<?php
  require '../includes/session_script.php';
  ?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Powertrac Corporation">
  <meta name="author" content="Powertrac Corporation">
  <title>Powertrac | Admin Home</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">

  <link rel="stylesheet" href="../assets/css/style.css" type="text/css">
</head>

<body>

  <?php
  include '../examples/sidebar.php';
  ?>
  
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    
  <?php 
  require '../navbar.php';
  ?>  
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
<!--               <h6 class="h2 text-white d-inline-block mb-0">Sellers</h6> -->
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Products</a></li>
                  <li class="breadcrumb-item active" aria-current="page">View Products</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">New</a>
              <a href="#" class="btn btn-sm btn-neutral">Filters</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <h1 class="mb-0">VIEW ALL PRODUCTS</h1>
              <hr style="margin-bottom: 0;padding-bottom: 0;" />
            </div>


            <!-- ADD PRODUCT -->

            <div class="view_product">
               
               <div class="row">
                 <div class="col-md-12 view_product_details text-center" style="background-color: #5e72e2 !important;">
                   
                   <div class="row">
                     <div class="col-md-1" style="border-right: 1px solid white;">
                       <p style="color: #FFFFFF !important;margin-bottom: 0px;"><strong>Product C id</strong></p>
                     </div>
                     <div class="col-md-2" style="border-right: 1px solid white;">
                       <p style="color: #FFFFFF !important;margin-bottom: 0px;"><strong>Product name</strong></p>
                     </div>
                     <div class="col-md-3" style="border-right: 1px solid white;">
                       <p style="color: #FFFFFF !important;margin-bottom: 0px;"><strong>Product description</strong></p>
                     </div>
                     <div class="col-md-1" style="border-right: 1px solid white;">
                       <p style="color: #FFFFFF !important;margin-bottom: 0px;"><strong>Product Cost</strong></p>
                     </div>
                     <div class="col-md-1" style="border-right: 1px solid white;">
                       <p style="color: #FFFFFF !important;margin-bottom: 0px;"><strong>Product Qty</strong></p>
                     </div>
                       <div class="col-md-2" style="border-right: 1px solid white;">
                       <p style="color: #FFFFFF !important;margin-bottom: 0px;"><strong>Product Category</strong></p>
                     </div>
                     <div class="col-md-2">
                       <p style="color: #FFFFFF !important;margin-bottom: 0px;"><strong>EDIT / DELETE</strong></p>
                     </div>
                   </div>

                 </div>
               </div>


                 <?php 

                      $stmt = $conn->prepare("SELECT * FROM products_db WHERE status='1'");
                      $stmt->execute();
                      $stmt->setFetchMode(PDO::FETCH_ASSOC);
                      if($stmt->rowCount()>0){
                        foreach(($stmt->fetchAll()) as $k1=>$row) {

                      $stmt2 = $conn->prepare("SELECT * FROM pro_main_category WHERE cat_id='".$row['main_category']."'");
                      $stmt2->execute();
                      $stmt2->setFetchMode(PDO::FETCH_ASSOC);
                      if($stmt2->rowCount()>0){
                        foreach(($stmt2->fetchAll()) as $k1=>$row2) {


                          echo ' <div class="row" style="margin-top: 10px;">
                 <div class="col-md-12 view_product_details text-center">
                   
                   <div class="row">
                     <div class="col-md-1">
                       <p style="margin-bottom: 0px;"><strong>'.$row['prod_c_id'].'</strong></p>
                     </div>
                     <div class="col-md-2">
                       <p style="margin-bottom: 0px;"><strong>'.$row['prod_name'].'</strong></p>
                     </div>
                     <div class="col-md-3" id="product_details_div">
                       <p style="margin-bottom: 0px;"><strong>'.$row['prod_details'].'</strong></p>
                     </div>
                     <div class="col-md-1">
                       <p style="margin-bottom: 0px;"><strong>'.$row['prod_price'].'</strong></p>
                     </div>
                     <div class="col-md-1">
                       <p style="margin-bottom: 0px;"><strong>'.$row['pro_qty'].'</strong></p>
                     </div>
                       <div class="col-md-2">
                       <p style="margin-bottom: 0px;"><strong>'.$row2['cat_name'].'</strong></p>
                     </div>
                     <div class="col-md-2 btn-group">
                      <div class="btn-group" style="margin:0 auto;">
                       <button type="button" style="margin-right: 10px;" id="edit_btn" onclick=\'editfunc("' .$row["prod_id"]. '")\'>Edit</button>
                       <button type="button" id="delete_btn" onclick=\'deletefunc("' .$row["prod_id"]. '")\' >Delete</button>
                      </div>
                     </div>
                   </div>

                 </div>
               </div> ';


                        }
                      }
                    }
                  }

                      ?>



                





            </div>



          </div>
        </div>
      </div>

      <!-- Footer -->
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
               &copy; <?php echo date("Y"); ?> <a href="" class="font-weight-bold ml-1" target="_blank">Powertrac Corporation</a>
            </div>
          </div>
          <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
              <li class="nav-item">
                <a href="" class="nav-link" target="_blank">Home</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link" target="_blank">About Us</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link" target="_blank">Blog</a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>


  <script type="text/javascript">
  
  function deletefunc(id)
  { 
    var r = confirm("Do you really want to delete the product?!");
    if (r == true) {
      

     var dataString = 'id='+ id;


     $.ajax({
      type: "POST",
      url: "delete_product.php",
      data: dataString,
      cache: false,
      success:function(data){
        if(data == 1){
        
         alert("Product has been deleted from the database")
         window.location.reload();
         
       }
       else{
          
        alert("Something went wrong..Product could not be deleted...")
        
       }

     }
   });

   }
 }


 function editfunc(id)
 { 
   window.location.href='update_product.php?id='+id;
 }

</script>




</body>

</html>