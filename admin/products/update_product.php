<?php
  require '../includes/session_script.php';
  ?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Powertrac Corporation">
  <meta name="author" content="Powertrac Corporation">
  <title>Powertrac | Admin Home</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">

  <link rel="stylesheet" href="../assets/css/style.css" type="text/css">
</head>

<body>

  <?php
  include '../examples/sidebar.php';
  ?>
  
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    
  <?php 
  require '../navbar.php';
  ?>  
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
<!--               <h6 class="h2 text-white d-inline-block mb-0">Sellers</h6> -->
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Products</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Update Products</li>
                </ol>
              </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
              <a href="#" class="btn btn-sm btn-neutral">New</a>
              <a href="#" class="btn btn-sm btn-neutral">Filters</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <h1 class="mb-0">Update product information</h1>
              <hr style="margin-bottom: 0;padding-bottom: 0;" />
            </div>


            <!-- ADD PRODUCT -->

            <div class="add_product">
               
               <div class="row">
                 <div class="col-md-12">
                   
                   <h1 class="mb-0">Product Information</h1>
                   <div class="row" style="margin-top: 20px;">
                     <div class="col-md-4">
                      

                       <?php
      $prod_id=$_GET['id'];
      $sql= $conn->prepare("SELECT * FROM products_db WHERE prod_id='".$prod_id."'");
      $sql->execute();
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      if($sql->rowCount()>0){
        foreach (($sql->fetchAll()) as $key => $row) {


            echo '
                       <input type=button id="prod_id" style="display:none;" value="'.$prod_id.'">
                       <img src="'.$row['pro_image_1'].'" id="img1" width="100%" height="100%">
                    <div><input type="hidden" name="image1" id="image1" disabled></div>
                     </div>
                     <div class="col-md-4">
                       <button type="button" class="btn btn-primary form-control" style="top:80%;"><input type="file" id="blog_pic" name="file" /></button> 
                     </div>
                     <div class="col-md-4"></div>
                   </div>
                  


                   <div class="row" style="margin-top: 20px;">
                     <div class="col-md-10">
                       <span>Product Name:</span>
                       <input type="text" class="form-control" name="" id="prod_name" value="'.$row['prod_name'].'"> 
                     </div>
                     <div class="col-md-2"></div>
                   </div>

                     <div class="row" style="margin-top: 20px;">
                     <div class="col-md-10">
                     <span>Product Details:</span>
                       <textarea class="form-control" placeholder="Write details about the product" id="prod_details" >'.$row['prod_details'].'</textarea>
                     </div>
                     <div class="col-md-2"></div>
                   </div>

                      <div class="row" style="margin-top: 20px;">
                     <div class="col-md-10">
                     <span>Product Price:</span>
                       <input type="number" class="form-control" name="" id="prod_price" value="'.$row['prod_price'].'">
                     </div>
                     <div class="col-md-2"></div>
                   </div>

                          <div class="row" style="margin-top: 20px;">
                     <div class="col-md-10">
                     <span>Product Quantity:</span>
                       <input type="number" class="form-control" name=""  id="prod_qty" value="'.$row['pro_qty'].'">
                     </div>
                     <div class="col-md-2"></div>
                   </div>

                    <div class="row" style="margin-top: 20px;">
                      <div class="col-md-10">
                        <button type="button" class="btn btn-primary form-control" id="update_product_btn">Update Product</button>
                      </div>
                      <div class="col-md-2"></div>
                    </div>
                    

                 </div>
               </div>

            </div> ';


        }
      }
        ?>

          </div>
        </div>
      </div>


      <!-- Footer -->
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
               &copy; <?php echo date("Y"); ?> <a href="" class="font-weight-bold ml-1" target="_blank">Powertrac Corporation</a>
            </div>
          </div>
          <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
              <li class="nav-item">
                <a href="" class="nav-link" target="_blank">Home</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link" target="_blank">About Us</a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link" target="_blank">Blog</a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>


  <script>
    
   $("#update_product_btn").click(function(){

  var prod_id=$("#prod_id").val();
  var product_pic=$("#image1").val();
  var prod_name=$("#prod_name").val();
  var prod_details=$("#prod_details").val();
  var prod_price=$("#prod_price").val();
  var prod_qty=$("#prod_qty").val();
  

  var datastr='prod_id='+prod_id+'&product_pic='+product_pic+'&prod_name='+prod_name+'&prod_details='+prod_details+'&prod_price='+prod_price+'&prod_qty='+prod_qty;

  if (product_pic=='') {
   alert("Please upload a Product Image");

 }

else if (prod_name=='') {
 alert("Please insert product Name")
}

else if (prod_details=='') {
 alert("Please provide product Description")
}

else if (prod_price=='') {
 alert("Please enter product Price");

}

else if (prod_qty=='') {
 alert("Please enter product Quantity");

}



else{

 $.ajax({


  type: "POST",
  url: "update_product_GET.php",
  data: datastr,
  cache: false,
  success: function(res){
    if(res==1)
    {
      alert("Product Updated Successfully!");
      window.location.href="view_product.php";

    }
    else{

     alert("Something went wrong...");

   }

 }
}); 
}

});
  </script>


    <script>
    $('#blog_pic').change(function(){

      var fd = new FormData();
      var files = $('#blog_pic')[0].files[0];
      fd.append('file',files);

      $.ajax({
        url: 'upload_blog.php',
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        success: function(response){
          if(response != 0){
            $("#img1").attr("src",response); 
            $('#image1').val(response);
                    $(".preview img").show(); // Display image element
                }else{
                  alert('file not uploaded');
                }
            },
        });
    });

  </script>
</body>

</html>