<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/site_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
<style>
.container-outer img{
margin-top:-55px;
height:100px;
}
</style>
    <div class="page-wrapper">
		<!-- Header Here -->
		<?php bottom_menu(); ?>
		<?php site_top_menu(); ?>
		<?php slide_slider(); ?>
        <!-- End Hidden Bar -->   
		<?php site_slider(); ?>   

<!--Start breadcrumb area-->     
<section class="breadcrumb-area style2" style="background-image: url(css/css/images/resources/breadcrumb-blog-single.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="single-post-info text-center clearfix">
                    <p>July 31, 2020</p>
                    <div class="title">
                       <h1 class="blog-title">Why it is Necessary to Have Inverter at Home?</h1>
                    </div>
                    <div class="meta-box">
                        <div class="author-icon">
                            <span class="icon-user"></span>    
                        </div>  
                        <ul class="meta-info">
                            <li><a href="#"><b>Authour Name</b></a></li>
                            <li><a href="#">Category</a></li>
                            <li><a href="#">0 Comments</a></li>
                        </ul> 
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>
<!--End breadcrumb area-->   

<!--Start blog single area-->
<section id="blog-area" class="blog-single-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="blog-post">
                    <div class="single-blog-post">
                        <div class="top-text-box">
                            <p>Application of analytics, amplify its power, and steer companies away from risky outcomes denounce with righteous indignation who are so beguiled and demoralized by the charms of pleasure of the moment  denounce with righteous indignation.</p>
                            <p>Charms of pleasure the moment there anyone who loves pursues odesires circumstances.</p>
                            <p>Men who are sobeguiled and demoralized by the charms of pleasure of the moment there anyone who loves or pursues or desires to obtain pain of itself, occasionally circumstances occur in which toil and pain can procure him some great pleasure  and dislike men who are sobeguiled and demoralized by the charms of pleasure of the moment there anyone who loves.</p>   
                        </div>
                        <div class="author-quote-box text-center">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12">
                                    <div class="text">
                                        <div class="icon"><img src="css/images/icon/quote.png" alt="Quote Icon"></div>
                                        <p>To take a trivial example, which ever undertakes laborious physical exercise, except obtainsome advantage but in certain circumstances.</p>
                                        <div class="name">
                                            <h3>Jessica Mcdade</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </div>
                        <div class="main-image-holder">
                            <img src="24.jpg" alt="Awesome Image">
                        </div>
                        <div class="quality-with-integrity-box">
                            <h2>Quality With Integrity</h2>
                            <div class="inner-content">
                                <h3>Best quality only happens when you care enough to do your best.</h3>
                                <p>Steer companies away from risky outcomes denounce with righteous indignation who are so beguiled and demoralized by pleasure of the moment perfectly have to be repudiated and annoyances accepted wise man therefore always.</p>
                                <ul>
                                    <li>Indignation who are so beguiled and demoralized by pleasure of the moment.</li>
                                    <li>Denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms.</li>
                                    <li>Prevents our being able to do what we like best, every pleasure is to be welcomed every,</li>
                                </ul>
                                <ol>
                                    <li>Righteous indignation and dislike men demoralized.</li>
                                    <li>Equal blame belongs to those who fail in their duty.</li>
                                    <li>One who avoids pain that produces resultant pleasure.</li>
                                </ol>
                            </div>    
                        </div>
                        
                        <div class="tag-with-social-links-box">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="inner-content">
                                        <div class="tag-box pull-left">
                                            <p><i class="fa fa-tags" aria-hidden="true"></i>Tags:</p> 
                                            <ul>
                                                <li><a href="#">Business</a></li>
                                                <li><a href="#">Consulting</a></li>
                                                <li><a href="#">Creative</a></li>
                                            </ul>   
                                        </div>
                                        <div class="social-links-box pull-right">
                                            <ul class="sociallinks-style-two float-left fix">
                                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                            </ul>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        <!--Start prev next option-->
                        <div class="blog-prev-next-option">
                            <div class="single prev">
                                <div class="image-thumb">
                                    <img src="css/images/blog/prev-thumb-1.png" alt="Image">
                                </div>
                                <div class="title">
                                    <h3>Rising Oil Price Should Re-Focus US On Solar Energy Growth</h3>
                                    <a href="#">prev<span class="icon-null"></span></a>
                                </div>
                            </div>
                            <div class="single next">
                                <div class="image-thumb">
                                    <img src="css/images/blog/next-thumb-1.png" alt="Image">
                                </div>
                                <div class="title">
                                    <h3>Tax and Duties Are Acting As Road Block for Power for All!</h3>
                                    <a href="#">Next<span class="icon-null"></span></a>
                                </div>
                            </div>     
                        </div>
                        <!--End prev next option-->
                    </div>
                          
                    <div class="author-box-holder">
                        <div class="inner-box">
                            <div class="img-box">
                                <img src="css/images/blog/author.png" alt="Awesome Image">
                            </div>
                            <div class="text">
                                <h3>Author Name</h3>
                                <span>Author</span>
                                <p>Denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of that  of the truth.</p>
                                <div class="author-social-links">
                                    <ul class="sociallinks-style-two fix">
                                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inner-comment-box">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="single-blog-title-box">
                                    <h2>Read Comments</h2>
                                </div>
                                <!--Start single comment outer box-->
                                <div class="single-comment-outer-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="single-comment-box">
                                        <div class="img-box">
                                            <img src="css/images/blog/comment-1.jpg" alt="Awesome Image">
                                        </div>
                                        <div class="text-box">
                                            <div class="top">
                                                <div class="name">
                                                    <h3>Steven Rich</h3>
                                                    <span>Dec 17, 2018</span>
                                                </div>
                                                <div class="reply-button">
                                                    <a href="#"><span class="icon-reload"></span>Reply</a>
                                                </div>
                                            </div>
                                            <div class="text">
                                                <p>Omnis iste natus error sit voluptatem accusantium nam libero tempore, cum soluta nobis est eligendi optio<br> cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--End single comment outer box-->
                                <!--Start single comment outer box-->
                                <div class="single-comment-outer-box mar-left wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1500ms">
                                    <div class="single-comment-box">
                                        <div class="img-box">
                                            <img src="css/images/blog/comment-2.jpg" alt="Awesome Image">
                                        </div>
                                        <div class="text-box">
                                            <div class="top">
                                                <div class="name">
                                                    <h3>William Cobus</h3>
                                                    <span>Sep 17, 2018</span>
                                                </div>
                                                <div class="reply-button">
                                                    <a href="#"><span class="icon-reload"></span>Reply</a>
                                                </div>
                                            </div>
                                            <div class="text">
                                                <p>Voluptatem accusantium nam libero tempore, cum soluta nobis est eligendi optio<br> cumque nihil impedit quo minus id quod maxime placeat.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--End single comment outer box-->
                                <!--Start single comment outer box-->
                                <div class="single-comment-outer-box wow fadeInUp" data-wow-delay="400ms" data-wow-duration="1500ms">
                                    <div class="single-comment-box">
                                        <div class="img-box">
                                            <img src="css/images/blog/comment-3.jpg" alt="Awesome Image">
                                        </div>
                                        <div class="text-box">
                                            <div class="top">
                                                <div class="name">
                                                    <h3>Van Wimbilton</h3>
                                                    <span>Aug 21, 2018</span>
                                                </div>
                                                <div class="reply-button">
                                                    <a href="#"><span class="icon-reload"></span>Reply</a>
                                                </div>
                                            </div>
                                            <div class="text">
                                                <p>Natus error sit voluptatem accusantium nam libero tempore, cum soluta nobis est eligendi optio cumque nihil<br> impedit quo minus id quod maxime placeat facere possimus.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--End single comment outer box-->
                            </div>
                        </div>
                    </div>
                    <!--Start add comment box-->
                    <div class="add-comment-box">
                        <div class="single-blog-title-box">
                            <h2>Add Your Comments</h2>
                        </div>
                        <form id="add-comment-form" action="#">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input type="text" name="fname" value="" placeholder="Name" required="">
                                        </div>
                                        <div class="col-md-4">
                                            <input type="email" name="form_email" value="" placeholder="Email" required="">
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" name="fphone" value="" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <textarea name="comment" placeholder="Your Comments" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button class="btn-three" type="submit">Submit Now<span class="icon-null"></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!--End add comment box-->
                    
                </div>
            </div>
        </div>
    </div>
    
</section> 
<?php site_footer(); ?>