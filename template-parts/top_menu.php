<?php function site_top_menu() { ?>

  <!-- quickview-modal -->
  <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky-menu stricky header-style-four">
    <div class="container clearfix">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="logo-box clearfix">
        <a class="side-nav-toggler nav-toggler hidden-bar-opener" href="#">
          <i class="energy-icon-menu"></i>
        </a>
        <button class="menu-toggler" data-target="#main-nav-bar-stricky">
          <span class="fa fa-bars"></span>
        </button>
      </div><!-- /.logo-box -->
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="main-navigation" id="main-nav-bar-stricky">
       <ul class="navigation-box">
<!--                                 <li class="current"><a href="index.php">Home</a>
</li> -->
<li><a href="online_ups.php">Online Ups</a>
  <ul class="sub-menu">
    <?php
    require 'connect.php';
    $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='1'");
    $sql->execute();
    $sql->setFetchMode(PDO::FETCH_ASSOC);
    if($sql->rowCount()>0){
      foreach (($sql->fetchAll()) as $key => $row) {

       echo '<li><a href="single_product.php?prod_id='.$row['prod_id'].'">'.$row['prod_name'].'</a></li>';

     }
   }
   ?>
 </ul>
</li>
<li><a href="luminous_solar_division.php">Luminuous Solar Division</a>
  <ul class="sub-menu">
    <?php
    require 'connect.php';
    $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='2'");
    $sql->execute();
    $sql->setFetchMode(PDO::FETCH_ASSOC);
    if($sql->rowCount()>0){
      foreach (($sql->fetchAll()) as $key => $row) {

       echo '<li><a href="single_product.php?prod_id='.$row['prod_id'].'">'.$row['prod_name'].'</a></li>';

     }
   }
   ?>
 </ul>
</li>
<li><a href="stabilizers.php">Stabilizers</a>
  <ul class="sub-menu">
    <?php
    require 'connect.php';
    $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='3'");
    $sql->execute();
    $sql->setFetchMode(PDO::FETCH_ASSOC);
    if($sql->rowCount()>0){
      foreach (($sql->fetchAll()) as $key => $row) {

       echo '<li><a href="single_product.php?prod_id='.$row['prod_id'].'">'.$row['prod_name'].'</a></li>';

     }
   }
   ?>
 </ul>
</li>
<li><a href="inverters_and_ups.php">Inverters & Ups</a>
  <ul class="sub-menu">
    <?php
    require 'connect.php';
    $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='4'");
    $sql->execute();
    $sql->setFetchMode(PDO::FETCH_ASSOC);
    if($sql->rowCount()>0){
      foreach (($sql->fetchAll()) as $key => $row) {

       echo '<li><a href="single_product.php?prod_id='.$row['prod_id'].'">'.$row['prod_name'].'</a></li>';

     }
   }
   ?>
 </ul>
</li>
<li><a href="genset_division.php">Genset Division</a>
  <ul class="sub-menu">
<?php
    require 'connect.php';
    $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='5'");
    $sql->execute();
    $sql->setFetchMode(PDO::FETCH_ASSOC);
    if($sql->rowCount()>0){
      foreach (($sql->fetchAll()) as $key => $row) {

       echo '<li><a href="single_product.php?prod_id='.$row['prod_id'].'">'.$row['prod_name'].'</a></li>';

     }
   }
   ?>
  </ul>
</li>

<li><a href="batteries.php">Batteries</a>
  <ul class="sub-menu">
    <li><a href="batteries.php">Industrial Batteries</a></li>
    <li><a href="automotive_batteries.php">Automotive Batteries</a></li>
  </ul>
</li>

<li><a href="batteries.php">MORE</a>
  <ul class="sub-menu">
    <li><a href="power_transformers.php">Power Transformers</a></li>
  </ul>
</li>

</ul>

</div><!-- /.navbar-collapse -->
<div class="right-side-box">
                   <!--
                   --><a href="#test-search" class="search-btn popup-with-zoom-anim"><i class="energy-icon-musica-searcher"></i></a>
<!----------------------------WISHLIST COUNTER-------------------------------->                   

          <?php
          if (isset($_SESSION['user_id'])) {
           
           require 'connect.php';
           $data = $conn->query("SELECT count(wishlist_id) as Total1 FROM wishlist WHERE user_id='".$_SESSION["user_id"]."' AND status='1'")->fetchAll();
           foreach ($data as $row3) {  
             
             echo '<a href="wishlist.php" class="cart-btn"><i class="icon-heart"></i><span class="number">'.$row3['Total1'].'</span></a>';

           }
         }
         else{

          echo '<a href="wishlist.php" class="cart-btn"><i class="icon-heart"></i><span class="number">0</span></a>';

        }
        ?>
<!---------------------------------CART COUNTER-------------------------------->
       <?php
        if (isset($_SESSION['user_id'])) {
         
         require 'connect.php';
         $data2 = $conn->query("SELECT count(cust_order_id) as Total2 FROM cust_order_data WHERE user_id='".$_SESSION["user_id"]."' AND status='1'")->fetchAll();
         foreach ($data2 as $row4) {  
           
           echo '<a href="cart.php" class="cart-btn"><i class="icon-cart"></i><span class="number">'.$row4['Total2'].'</span></a>';

         }
       }
       else{

        echo '<a href="cart.php" class="cart-btn"><i class="icon-cart"></i><span class="number">0</span></a>';

      }
      ?>

        </div>
        <!-- /.right-side-box -->
      </div>
      <!-- /.container -->
    </nav>



    <?php } ?>