<?php function site_slider() { ?>
<style>
.carousel-caption{
	margin-top:80px;
}
</style>
                <div id="minimal-bootstrap-carousel" class="carousel slide carousel-fade slider-content-style slider-home-two">
            <ol class="carousel-indicators">
                <li data-target="#minimal-bootstrap-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#minimal-bootstrap-carousel" data-slide-to="1"></li>
                <li data-target="#minimal-bootstrap-carousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="carousel-item active slide-1 bg-pos-left-center" style="background-image: url(css/images/home-pages/bannre-2-1.jpg);">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="box valign-middle">
                                <div class="content text-left">
                                    <h3 data-animation="animated fadeInUp">Energy is <br> future-make <br> it bright.</h3>
                                    <p data-animation="animated fadeInDown">These cases are perfectly simple and easy to distinguish <br> when our power off choice is untrammelled.</p>
                                    <a data-animation="animated fadeInUp" href="about.php" class="thm-btn"><span class="text-line">Read More</span><i class="energy-icon-right-arrow"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item slide-2 bg-pos-right-center" style="background-image: url(css/images/home-pages/bannre-2-2.jpg);">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="box valign-middle">
                                <div class="content text-left">
                                    <div class="float-right">
                                        <h3 data-animation="animated fadeInUp">Light Up Your <br>Home With <br> US. </h3>
                                        <p data-animation="animated fadeInDown">These cases are perfectly simple and easy to distinguish <br> when our power off choice is untrammelled.</p>
                                        <a data-animation="animated fadeInUp" href="about.php" class="thm-btn"><span class="text-line">Read More</span><i class="energy-icon-right-arrow"></i></a>
                                    </div><!-- /.float-right -->                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item slide-2 bg-pos-left-center" style="background-image: url(css/images/home-pages/bannre-2-3.jpg);">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="box valign-middle">
                                <div class="content text-left clearfix">
                                    <h3 data-animation="animated fadeInUp">Empower Your <br> Bussinss <br> with Us.</h3>
                                    <p data-animation="animated fadeInDown">These cases are perfectly simple and easy to distinguish <br> when our power off choice is untrammelled.</p>
                                    <a data-animation="animated fadeInUp" href="about.php" class="thm-btn"><span class="text-line">Read More</span><i class="energy-icon-right-arrow"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Controls -->
            <a class="carousel-control-prev carousel-control-one-prev" href="#minimal-bootstrap-carousel" role="button" data-slide="prev">
                <i class="fa fa-angle-left"></i>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next carousel-control-one-next" href="#minimal-bootstrap-carousel" role="button" data-slide="next">
                <i class="fa fa-angle-right"></i>
                <span class="sr-only">Next</span>
            </a>
        </div>
<?php } ?>