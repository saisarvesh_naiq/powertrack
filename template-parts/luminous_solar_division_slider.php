<?php function luminous_solar_division_slider() { ?>
<style>
.carousel-caption{
	margin-top:80px;
}

.carousel-item{
    height: 480px !important;
}

.carousel-item img{
    height: 100% !important;
    width: auto;
    display: block;
    object-fit: cover;
    }
</style>




<div class="container-fluid">
    <div class="row">
        <div class="col-md-12" style="padding: 0 !important;">
            


    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active" data-interval="3000">
      <img src="assets/images/banners/luminous_solar_division_banner.png" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item" data-interval="3000">
      <img src="assets/images/banners/luminous_solar_division_banner.png" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item" data-interval="3000">
      <img src="assets/images/banners/luminous_solar_division_banner.png" class="d-block w-100" alt="...">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>






        </div>
    </div>
</div>





<?php } ?>