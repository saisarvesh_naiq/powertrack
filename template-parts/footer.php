<?php function site_footer() { ?>
         <footer class="site-footer footer-two footer-four">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="footer-widget about-widget">
						<div class="title-line">
                               <a href="assets/index.html"> <h3> POWERTRAC </h3></a>
                            </div>
                           
                            <p>S No. 3, Sita Smriti Bldg, </br>St Inez, Near St Inez Church, </br>Opp Carpenter Choice.  </p>
                            <ul class="contact-lists">
                                <li>
                                    <i class="energy-icon-world"></i>
                                   Panjim Goa
                                </li>
                                <li>
                                    <i class="energy-icon-call-answer"></i>
                                    
+ (91) 0832 2424484 / 2424514
                                </li>
                                <li>
                                    <i class="energy-icon-black-envelope"></i>
                                   
info@powertraccorporation.com

                                </li>
                            </ul><!-- /.contact-lists -->
                        </div><!-- /.footer-widget -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <div class="footer-widget links-widget">
                            <div class="title-line">
                                <h3>Usefull Links</h3>
                            </div><!-- /.title-line -->
                            <div class="row">
                                <div class="col-sm-6">
                                    <ul class="links-list has-sep">
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>About Company</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Meet Experts</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Get a Quote</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Testimonials</a></li>
                                    </ul><!-- /.links-list -->
                                </div><!-- /.col-sm-6 -->
                                <div class="col-sm-6">
                                    <ul class="links-list">
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Our Services</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Latest News</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Privacy Policy</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Contact Us</a></li>
                                    </ul><!-- /.links-list -->
                                </div><!-- /.col-sm-6 -->
                            </div><!-- /.row -->
                        </div><!-- /.footer-widget -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <div class="footer-widget">
                            <div class="title-line">
                                <h3>Latest News</h3>
                            </div><!-- /.title-line -->
                            <div class="post-wrapper">
                                <div class="single-post-content">
                                    <div class="image-box">
                                        <img src="assets/images/home-pages/lp-f4-1-1.png" alt="Awesome Image"/>
                                    </div><!-- /.image-box -->
                                    <div class="text-block">
                                        <a href="#" class="date-line"><i class="energy-icon-calendar"></i> 08th July 2020</a>
                                        <h3><a href="#">Uptake of Renewable Energy in the UK Increases.</a></h3>
                                    </div><!-- /.text-block -->
                                </div><!-- /.single-post-content -->
                                <div class="single-post-content">
                                    <div class="image-box">
                                        <img src="assets/images/home-pages/lp-f4-1-2.png" alt="Awesome Image"/>
                                    </div><!-- /.image-box -->
                                    <div class="text-block">
                                        <a href="#" class="date-line"><i class="energy-icon-calendar"></i> 27th June 2020</a>
                                        <h3><a href="#">2020 The Record Breaking Year for Renewables.</a></h3>
                                    </div><!-- /.text-block -->
                                </div><!-- /.single-post-content -->
                            </div><!-- /.post-wrapper -->
                        </div><!-- /.footer-widget -->
                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
            <a class="back-to-top scroll-to-target" data-target="html" href="#">Back to Top <i class="energy-icon-arrow-pointing-north"></i></a>
        </footer><!-- /.site-footer footer-two -->
        <div class="footer-bottom-home-two home-four">
            <div class="container clearfix">
                <div class="container-outer clearfix">
                    <div class="left-content float-left">
                        <p>&copy; 2020 All Rights Reserved by <a href="#">Powertech.</a></p>
                    </div><!-- /.left-content -->
                    <div class="right-content float-right">
                        <ul class="footer-menu">
                            <li><a href="#">Terms & Conditions</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </ul><!-- /.footer-menu -->
                    </div><!-- /.right-content -->
                </div><!-- /.container-outer clearfix -->
            </div><!-- /.container -->
        </div><!-- /.footer-bottom-home-two -->
    </div><!-- /.page-wrapper -->
    <div class="search_area zoom-anim-dialog mfp-hide" id="test-search">
        <div class="search_box_inner">
            <div class="input-box">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-box-btn"> <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button> </span>
            </div>
        </div>
    </div>
	<script>
	$(document).ready(function () {
    var carousel = $("#owl-demo");
    carousel.owlCarousel({
    navigation:true,
    navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
  });  
});
	</script>
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/jquery.enllax.min.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/owl.js"></script>
    <script src="assets/js/wow.js"></script>
    <script src="assets/assets/language-switcher/jquery.polyglot.language.switcher.js"></script>
    <script src="assets/js/isotope.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/bootstrap-select.min.js"></script>
    <script src="assets/js/jquery.mcustomscrollbar.concat.min.js"></script>
    <!-- thm custom script -->
    <script src="assets/js/custom.js"></script>
    <script src="assets/js/home-scripts.js"></script>
</html>
<?php } ?>