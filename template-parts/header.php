<?php function site_header() { ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Power Solutions in Goa | UPS and Inverters Goa | Stabilizers</title>
	<!-- responsive meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- master stylesheet -->
    <link rel="stylesheet" href="assets/css/hover-min.css">
    <link rel="stylesheet" href="assets/css/style.css">
	<!-- Responsive stylesheet -->
    <link rel="stylesheet" href="assets/css/responsive.css">
    <link rel="stylesheet" href="assets/css/navigation-responsive.css">
	<link rel="stylesheet" href="assets/css/home-page-responsive.css">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="Powertrac-Logo.ico">
    <link rel="apple-touch-icon" sizes="60x60" href="Powertrac-Logo.ico">
    <link rel="apple-touch-icon" sizes="72x72" href="Powertrac-Logo.ico">
    <link rel="icon" type="image/png" sizes="32x32" href="Powertrac-Logo.ico">
	
	
	<!--  <link rel="stylesheet" href="assets/vendor/bootstrap-4.2.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="assets/vendor/owl-carousel-2.3.4/assets/owl.carousel.min.css">-->
      <link rel="stylesheet" href="css/css/style.css">
      <!-- js --><script src="css/vendor/jquery-3.3.1/jquery.min.js"></script>
	  <script src="css/vendor/owl-carousel-2.3.4/owl.carousel.min.js"></script>
	  <script src="css/vendor/nouislider-12.1.0/nouislider.min.js"></script>
	  <script src="css/js/main.js"></script>


      <link rel="stylesheet" href="assets/css/sweetalert.css">
      <script src="assets/js/sweetalert.min.js"></script>

	 
    <!-- Fixing Internet Explorer-->
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script src="js/html5shiv.js"></script>
    <![endif]-->
</head>
<?php } ?>