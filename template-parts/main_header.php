<?php function bottom_menu() { ?>

  <?php
  require "session_script.php";
  ?>
  <!--   <div class="preloader style-four"></div>-->
  <header class="site-header header-four">
    <div class="upper-header">
      <div class="container ">
        <div class="container-outer clearfix">
          <div class="left-logo float-left">
            <a href="index.php"><img src="Powertrac-Logo.png" alt="Powertrac"/></a>
          </div><!-- /.left-logo -->
          <div class="right-block float-right">
            <div class="single-right-block contact-block">
              <div class="icon-block">
                <i class="energy-icon-envelope-of-white-paper"></i>
              </div><!-- /.icon-block -->
              <div class="text-block">
                <p><span>Mail Us On</span>info@powertraccorporation.com</p>
              </div><!-- /.text-block -->
            </div><!-- /.contact-block -->
            <div class="single-right-block contact-block">
              <div class="icon-block">
                <i class="energy-icon-headset"></i>
              </div><!-- /.icon-block -->
              <div class="text-block">
                <p><span>Call Us On</span>+ (91) 0832 2424484 / 2424514</p>
              </div><!-- /.text-block -->
            </div><!-- /.contact-block -->
            <div class="single-right-block contact-block">

             <?php
             
             if (isset($_SESSION['email'])) {
              echo '
              <a href="user_profile.php"><button class="profile_btn2"><i class="far fa-user-circle"></i>PROFILE</button></a>
              <a href="logout.php" class="thm-btn home-four hvr-sweep-to-left" id="logout_btn">Logout</a>
              ';

            }
            else{
              echo '
              <a href="login.php" class="thm-btn home-four hvr-sweep-to-left">Login/SignUp</a>';
              
            }
            
            ?>

            <!-- <a href="register.php" class="thm-btn home-four hvr-sweep-to-left">Login/SignUp</a> -->
            <!-- /.thm-btn home-four -->
          </div><!-- /.contact-block -->
        </div><!-- /.right-block float-right -->
      </div><!-- /.container-outer -->
    </div><!-- /.container -->
  </div><!-- /.upper-header -->
  <nav class="navbar navbar-expand-lg navbar-light header-navigation header-style-four">
    <div class="container clearfix">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="logo-box clearfix">
        <a class="side-nav-toggler nav-toggler hidden-bar-opener" href="#"><i class="energy-icon-menu"></i></a>
        <button class="menu-toggler" data-target="#main-nav-bar">
          <span class="fa fa-bars"></span>
        </button>
      </div><!-- /.logo-box -->
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="main-navigation" id="main-nav-bar">
        <ul class="navigation-box">

                            <!--     <li class="current"><a href="index.php">Home</a>
                            </li> -->
                            <li><a href="online_ups.php">Online Ups</a>
                              <ul class="sub-menu">
                                <?php
                                require 'connect.php';
                                $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='1'");
                                $sql->execute();
                                $sql->setFetchMode(PDO::FETCH_ASSOC);
                                if($sql->rowCount()>0){
                                  foreach (($sql->fetchAll()) as $key => $row) {

                                   echo '<li><a href="single_product.php?prod_id='.$row['prod_id'].'">'.$row['prod_name'].'</a></li>';

                                 }
                               }
                               ?>
                               
                             </ul>
                           </li>
                           <li><a href="luminous_solar_division.php">Luminuous Solar Division</a>
                            <ul class="sub-menu">
                             <?php
                             require 'connect.php';
                             $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='2'");
                             $sql->execute();
                             $sql->setFetchMode(PDO::FETCH_ASSOC);
                             if($sql->rowCount()>0){
                              foreach (($sql->fetchAll()) as $key => $row) {

                               echo '<li><a href="single_product.php?prod_id='.$row['prod_id'].'">'.$row['prod_name'].'</a></li>';

                             }
                           }
                           ?>
                           
                         </ul>
                       </li>
                       <li><a href="stabilizers.php">Stabilizers</a>
                        <ul class="sub-menu">
                         <?php
                         require 'connect.php';
                         $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='3'");
                         $sql->execute();
                         $sql->setFetchMode(PDO::FETCH_ASSOC);
                         if($sql->rowCount()>0){
                          foreach (($sql->fetchAll()) as $key => $row) {

                           echo '<li><a href="single_product.php?prod_id='.$row['prod_id'].'">'.$row['prod_name'].'</a></li>';

                         }
                       }
                       ?>
                       
                     </ul>
                   </li>
                   <li><a href="inverters_and_ups.php">Inverters & Ups</a>
                    <ul class="sub-menu">
                     <?php
                     require 'connect.php';
                     $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='4'");
                     $sql->execute();
                     $sql->setFetchMode(PDO::FETCH_ASSOC);
                     if($sql->rowCount()>0){
                      foreach (($sql->fetchAll()) as $key => $row) {

                       echo '<li><a href="single_product.php?prod_id='.$row['prod_id'].'">'.$row['prod_name'].'</a></li>';

                     }
                   }
                   ?>
                   
                 </ul>
               </li>
               <li><a href="genset_division.php">Genset Division</a>
                <ul class="sub-menu">
                  <?php
                  require 'connect.php';
                  $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='5'");
                  $sql->execute();
                  $sql->setFetchMode(PDO::FETCH_ASSOC);
                  if($sql->rowCount()>0){
                    foreach (($sql->fetchAll()) as $key => $row) {

                     echo '<li><a href="single_product.php?prod_id='.$row['prod_id'].'">'.$row['prod_name'].'</a></li>';

                   }
                 }
                 ?>
               </ul>
             </li>

             <li><a href="batteries.php">Batteries</a>
              <ul class="sub-menu">
                <li><a href="batteries.php">Industrial Batteries</a></li>
                <li><a href="automotive_batteries.php">Automotive Batteries</a></li>
              </ul>
            </li>

            <li><a href="batteries.php">MORE</a>
              <ul class="sub-menu">
                <li><a href="power_transformers.php">Power Transformers</a></li>
              </ul>
            </li>





          </ul>



        </div><!-- /.navbar-collapse -->
        <div class="right-side-box">

				  <!--
          --><a href="#test-search" class="search-btn popup-with-zoom-anim"><i class="energy-icon-musica-searcher"></i></a>

<!----------------------------------WISHLIST COUNTER-------------------------------------------->
          <?php
          if (isset($_SESSION['user_id'])) {

           require 'connect.php';
           $data = $conn->query("SELECT count(wishlist_id) as Total1 FROM wishlist WHERE user_id='".$_SESSION["user_id"]."' AND status='1'")->fetchAll();
           foreach ($data as $row3) {  

             echo '<a href="wishlist.php" class="cart-btn"><i class="icon-heart"></i><span class="number">'.$row3['Total1'].'</span></a>';

           }
         }
         else{

          echo '<a href="wishlist.php" class="cart-btn"><i class="icon-heart"></i><span class="number">0</span></a>';

        }
        ?>

<!--------------------------------------------CART COUNTER-------------------------------------->
        <?php
        if (isset($_SESSION['user_id'])) {

         require 'connect.php';
         $data2 = $conn->query("SELECT count(cust_order_id) as Total2 FROM cust_order_data WHERE user_id='".$_SESSION["user_id"]."' AND status='1'")->fetchAll();
         foreach ($data2 as $row4) {  

           echo '<a href="cart.php" class="cart-btn"><i class="icon-cart"></i><span class="number">'.$row4['Total2'].'</span></a>';

         }
       }
       else{

        echo '<a href="cart.php" class="cart-btn"><i class="icon-cart"></i><span class="number">0</span></a>';

      }
      ?>


    </div>
    <!-- /.right-side-box -->
  </div>
  <!-- /.container -->


</nav>

<div class="dropdown_content">
  <div class="row">
    <div class="col-md-2 text-center dropdown_div">
      <img src="assets/images/icon/inverter.png">
      <p style="margin-top: 34px;font-size: 14px;">Home UPS and Inverter</p>
    </div>
    <div class="col-md-2 text-center dropdown_div">
      <img src="assets/images/icon/high_inverter.png">
      <p style="margin-top: 34px;font-size: 14px;">High capacity inverter and UPS</p>
    </div>
    <div class="col-md-2 text-center dropdown_div">
      <img src="assets/images/icon/batteries.png">
      <p style="margin-top: 28px;font-size: 14px;">Inverter Batteries</p>
    </div>
    <div class="col-md-2 text-center dropdown_div">
      <img src="assets/images/icon/stabilizers.png">
      <p style="margin-top: 40px;font-size: 14px;">Stabilizers</p>
    </div>
    <div class="col-md-2 text-center dropdown_div">
      <img src="assets/images/icon/other_batteries.png">
      <p style="margin-top: 32px;font-size: 14px;">Other Batteries</p>
    </div>
    <div class="col-md-2 text-center dropdown_div">
      <img src="assets/images/icon/online_ups.png">
      <p style="margin-top: 20px;font-size: 14px;">Online UPS</p>
    </div>

  </div>
</div>
</header><!-- /.site-header -->



<?php } ?>