<?php function slide_slider() { ?>
	   <!-- Hidden Navigation Bar -->
        <section class="hidden-bar right-align">
            <div class="hidden-bar-closer">
                <button><span class="flaticon-cancel-button"></span></button>
            </div>
            <div class="hidden-bar-wrapper">
                <div class="logo">
                    <a href="new/solartech-demo/index.html"><img src="logo.png" alt=""/></a>
                </div>
                <div class="hiddenbar-about-us">
                    <h3>About Us</h3>
                    <div class="text">
                        <p>We continue to drive forward, offering new technology, products, and business methods. Our vast product offering encompasses electrical, Critical Power Backup, 
						Power Storage, Power Generation, and network power systems and components.</p>
                    </div>    
                </div>
                <div class="contact-info-box">
                    <h3>Contact Info</h3>
                    <ul>
                        <li>
                            <h5>Address</h5>
                            <p>S No. 3, Sita Smriti Bldg, St Inez, Near St Inez Church, Opp Carpenter Choice <br>  Panjim Goa.</p>
                        </li>
                        <li>
                            <h5>Phone</h5>
                            <p>Phone 1: 
+ (91) 0832 2424484 / 2424514</p>
                        </li>
                        <li>
                            <h5>Email</h5>
                            <p>info@powertraccorporation.com</p>
                        </li>
                    </ul>
                </div>       
                <div class="newsletter-form-box">
                    <h3>Newsletter Subscribe</h3>
                    <form action="#">
                        <div class="row">
                            <div class="col-xl-12">
                                <input type="email" name="email" placeholder="Email Address..."> 
                                <button type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>    
                            </div>
                        </div>
                    </form>
                </div>
                <div class="copy-right-text">
                    <p>© Powertrack 2020, All Rights Reserved.</p>
                </div> 
            </div>
        </section>
<?php } ?>