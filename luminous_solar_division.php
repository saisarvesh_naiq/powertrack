<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/luminous_solar_division_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
<style>
.container-outer img{
margin-top:-55px;
height:100px;
}
</style>
    <div class="page-wrapper">
		<!-- Header Here -->
		<?php bottom_menu(); ?>
		<?php site_top_menu(); ?>
		<?php slide_slider(); ?>


    <div style="margin-top: 170px;">
    	<?php luminous_solar_division_slider(); ?>
    </div>
				

	<div class="container">
	   

	   <div class="product_intro">
	   	<div class="row">
	   		<div class="col-md-12 text-center">
	   				   	<h1>Luminous Solar division</h1>
	   				   	<p>Choosing the right Solar Inverter for your unique need is a must. With Luminous’ wide portfolio of Solar Inverters and UPS you can do just that! Use this section to find the perfect fit in Solar inverters and UPS for your home.</p>
	   		</div>
	   	</div>
	   </div>


       <!-- <div class="product_filter">
           <div class="row">
               <div class="col-md-12 text-center">
                   <div class="row">
                       <div class="col-md-4">
                           <div class="row">
                            <div class="col-md-1"></div>
                               <div class="col-md-3">
                                   <img src="assets/images/products/load_requirenment.png">
                               </div>
                               <div class="col-md-8">
                                   <p>Select By</p>
                                   <h1>Load Requirenment</h1>
                               </div>
                           </div>

                           <div class="row">
                             <div class="col-md-1"></div>
                               <div class="col-md-3"></div>
                               <div class="col-md-8">
                                    <p style="margin-top: 20px;"><a href="">Below 1500VA</a></p>
                           <p><a href=""> 1500VA - 4000VA</a></p>
                            <p><a href="">  Above 4000VA</a></p>
                             <p><a href="">  Below 2KW</a></p>
                              <p><a href="">  2KW - 5KW</a></p>
                               <p><a href="">  Above 5KW</a></p>

                               </div>
                           </div>                    
                       </div>

                    <div class="col-md-4">
                           <div class="row">
                            <div class="col-md-1"></div>
                               <div class="col-md-3">
                                   <img src="assets/images/products/inverter_type.png">
                               </div>
                               <div class="col-md-8">
                                   <p>Select By</p>
                                   <h1>Inverter Type</h1>
                               </div>
                           </div>

                           <div class="row">
                               <div class="col-md-4"></div>
                               <div class="col-md-8">
                                    <p style="margin-top: 20px;"><a href=""> Off Grid Inverter</a></p>
                           <p><a href=""> On Grid Inverter</a></p>
                               </div>
                           </div>                    
                       </div>

                    <div class="col-md-4">
                           <div class="row">
                            <div class="col-md-1"></div>
                               <div class="col-md-3">
                                   <img src="assets/images/products/technology.png">
                               </div>
                               <div class="col-md-8">
                                   <p>Select By</p>
                                   <h1>Technology</h1>
                               </div>
                           </div>

                           <div class="row">
                               <div class="col-md-4"></div>
                               <div class="col-md-8">
                                    <p style="margin-top: 20px;"><a href=""> PWM</a></p>
                           <p><a href="">  MPPT</a></p>
                               </div>
                           </div>                    
                       </div>
               



                   </div>
                   <button class="view_all_btn">View all products</button>
               </div>
           </div>
       </div>
 -->


        <?php
     require 'connect.php';
      $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='2' AND status='1'");
      $sql->execute();
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      if($sql->rowCount()>0){
        foreach (($sql->fetchAll()) as $key => $row) {
             
             echo '        <div class="product_details">
          <div class="container">
            
             <div class="row">
              <div class="col-md-6 text-center" style="padding-left:50px;padding-right:50px;">
                <img src="admin/products/'.$row['pro_image_1'].'">
              </div>
              <div class="col-md-6 product_details_content">
                <h1>'.$row['prod_name'].'</h1>
                <p>'.$row['prod_details'].'</p>

                    <div class="row" style="margin-top: 60px">
                      <div class="col-md-2"></div>
                      <div class="col-md-4 text-center">
                       <a href="single_product.php?prod_id='.$row['prod_id'].'"><button class="brochure_btn" >View Product</button></a>
                      </div>
                      <div class="col-md-4 text-center">
                        <button class="quote_button">Get A Quote</button>
                      </div>
                      <div class="col-md-2"></div>
                    </div>

              </div>
             </div>

            
            <div class="product_features">
             <div class="row">
              <div class="col-md-2"></div>
                 
                 <div class="col-md-8">
                  <div class="row">
                    <div class="col-md-3 text-center feature_divs">
                      <img src="assets/images/products/highly_reliable.png">
                    </div>
                    <div class="col-md-3 text-center feature_divs">
                      <img src="assets/images/products/selectable_charging.png">
                    </div>
                    <div class="col-md-3 text-center feature_divs">
                      <img src="assets/images/products/cold_start.png">
                    </div>
                    <div class="col-md-3 text-center feature_divs">
                      <img src="assets/images/products/AVR.png">
                    </div>
                  </div>
                 </div>

              <div class="col-md-2"></div>
             </div>
             </div>

          </div>
        </div>';

        }
      }
      ?>










      
        

	</div>































<?php site_footer(); ?>
