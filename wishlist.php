<?php
error_reporting(0);
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/online_ups_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
  <style>
    .container-outer img{
      margin-top:-55px;
      height:100px;
    }
  </style>
  <div class="page-wrapper">
    <!-- Header Here -->
    <?php bottom_menu(); ?>
    <?php site_top_menu(); ?>
    <?php slide_slider(); ?>


    <style>
      .wishlist{
        margin-top: 200px;
      }

      .wishlist_title{
        padding-top: 10px;
        padding-bottom: 5px;
      }

      .wishlist_title p{
       margin: 0;
       font-size: 20px;
       font-weight: 600;
     }


     #rmv_btn{
       background:linear-gradient(90deg, #ff9966 0%,#ff5e62 100% ) !important;
       border:none !important;
       border-radius: 6px !important;
       color: #ffffff;
     }

     #addto_cart_btn{
      background:linear-gradient(90deg, #dce35b 0%,#45b649 100% );
      border:none !important;
      border-radius: 6px !important;
      color: #ffffff;
    }

    @media (max-width: 991.98px) {
      .wishlist{
        margin-top: 20px;
      }
    }

  </style>





  <section>
    <div class="wishlist">
     <div class="container" style="padding-top: 40px;padding-bottom: 40px;">


       <div class="row">          
         <div class="col-md-12 text-center">
           <img src="assets/images/icon/heart.png">
           <h1 style="color: black;font-size: 40px !important;">My Wishlist</h1>
         </div>
       </div>

       <div class="row" style="margin-top: 30px;">
         <div class="col-md-4 text-center wishlist_title">
           <p>Product Name</p>
         </div>
         <div class="col-md-2 text-center wishlist_title">
           <p>Unit Price</p>
         </div>
         <div class="col-md-2 text-center wishlist_title">
           <p>Stock Status</p>
         </div>
         <div class="col-md-4 text-center wishlist_title"></div>
       </div>
       <hr style="padding: 0;margin: 0;" />


      <?php
      require 'connect.php';
      $sql= $conn->prepare("SELECT * FROM wishlist WHERE user_id='".$_SESSION['user_id']."' AND status='1'");
      $sql->execute();
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      if($sql->rowCount()>0){
        foreach (($sql->fetchAll()) as $key => $row) {

      $sql2= $conn->prepare("SELECT * FROM products_db WHERE prod_id='".$row['prod_id']."'");
      $sql2->execute();
      $sql2->setFetchMode(PDO::FETCH_ASSOC);
      if($sql2->rowCount()>0){
        foreach (($sql2->fetchAll()) as $key => $row2) {

          if ($row2['pro_qty']>0) {
             
             $stock="In Stock";

          }
          else if ($row2['pro_qty']==0) {
             $stock="Out of stock";
          }


          echo '<div class="row" style="margin-top: 30px;margin-bottom: 10px;">
         <div class="col-md-4 wishlist_content">
          <div class="row">
            <div class="col-md-4">
              <img src="admin/products/'.$row2['pro_image_1'].'">
            </div>
            <div class="col-md-5">
            <p>'.$row2['prod_name'].'</p>
            </div>
            <div class="col-md-3"></div>
          </div>
        </div>
        <div class="col-md-2 text-center wishlist_content">
         <p>&#8377; '.$row2['prod_price'].'</p>
       </div>
       <div class="col-md-2 text-center wishlist_content">
         <p>'.$stock.'</p>
       </div>
       <div class="col-md-4 text-center wishlist_content">
         <div class="btn-group">
           <button  class="btn btn-primary" style="margin-right: 10px;" id="rmv_btn" onclick=\'deletewishfunc("' .$row["wishlist_id"]. '")\' >REMOVE</button>
           <button  class="btn btn-primary" id="addto_cart_btn" onclick=\'cartaddfunc("' .$row["wishlist_id"]. '")\' >ADD TO CART</button>
         </div>
       </div>
     </div>
     <hr style="padding: 0;margin: 0;" />';


            
        }
      }

              }
            }
            else{
              echo '<h3 style="margin-top:40px;margin-bottom:40px;">There are no items in your wishlist</h3>';
            }

          ?>

       


   </div>
 </div>
</section>



<!-----------------Deleting product from wishlist-------------------->
<script type="text/javascript">
  function deletewishfunc(id)
  { 
    var r = confirm("Remove Product Wishlist?");
    if (r == true) {


     var dataString = 'id='+ id;


     $.ajax({
      type: "POST",
      url: "remove_wish_product.php",
      data: dataString,
      cache: false,
      success:function(data){
        if(data == 1){

          swal("", "Product Removed From Wishlist.", "info");

          setTimeout(function(){
           window.location.reload();
         }, 2000);



        }
        else{

          swal("Error occured..");


        }

      }
    });

   }
 }

</script>


<!-----------------Moving products to cart-------------------->

<script type="text/javascript">
  function cartaddfunc(id)
  { 
    var r = confirm("Move product to cart?");
    if (r == true) {


     var dataString = 'id='+ id;


     $.ajax({
      type: "POST",
      url: "wishlist_to_cart_GET.php",
      data: dataString,
      cache: false,
      success:function(data){
        if(data == 1){

          swal("", "Product moved to cart!.", "info");

          setTimeout(function(){
           window.location.reload();
         }, 2000);



        }
        else{

          swal("Error occured..");


        }

      }
    });

   }
 }

</script>






<?php site_footer(); ?>
