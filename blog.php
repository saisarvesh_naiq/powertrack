<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/site_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
<style>
.container-outer img{
margin-top:-55px;
height:100px;
}
</style>
    <div class="page-wrapper">
		<!-- Header Here -->
		<?php bottom_menu(); ?>
		<?php site_top_menu(); ?>
		<?php slide_slider(); ?>                 
<!--End mainmenu area-->    

<!--Start breadcrumb area-->     
<section class="breadcrumb-area" style="background-image: url(css/images/resources/breadcrumb-bg.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title float-left">
                       <h1>Our Blogs</h1>
                    </div>
                    <div class="breadcrumb-menu float-right">
                        <ul class="clearfix">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Our Blogs</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>
<!--End breadcrumb area-->

<!--Start blog area-->
<section id="blog-area" class="blog-default-area">
    <div class="container">
        <div class="row">
            <!--Start single blog post-->
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-blog-post wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <img src="css/images/blog/v1-1.jpg" alt="Awesome Image">
                        <div class="overlay-style-two"></div>
                        <div class="post-date">
                            <h3><span>Jan</span><br> 31</h3>
                        </div>
                    </div>
                    <div class="text-holder">
                        <h3 class="blog-title"><a href="blog-single.html">Rising Oil Price Should Re-Focus US On Solar Energy Growth</a></h3>
                        <div class="meta-box">
                            <ul class="meta-info">
                                <li><a href="#"><b>Rubin Santro</b></a></li>
                                <li><a href="#">Environment</a></li>
                            </ul> 
                            <div class="author-icon">
                                <span class="icon-user"></span>    
                            </div>  
                        </div> 
                        <div class="text">
                            <p>Same as saying through shrinking from toil & pain these cases perfectly simple...</p>
                            <a class="btn-two" href="#">Read More<span class="icon-null"></span></a>
                        </div>  
                    </div>
                </div>
            </div>
            <!--End single blog post-->
            <!--Start single blog post-->
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-blog-post wow fadeInLeft" data-wow-delay="200ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <img src="css/images/blog/v1-2.jpg" alt="Awesome Image">
                        <div class="overlay-style-two"></div>
                        <div class="post-date">
                            <h3><span>Jan</span><br> 06</h3>
                        </div>
                    </div>
                    <div class="text-holder">
                        <h3 class="blog-title"><a href="blog-single.html">Tax and Duties Are Acting As Road Block for Power for All!</a></h3>
                        <div class="meta-box">
                            <ul class="meta-info">
                                <li><a href="#"><b>Richard Antony</b></a></li>
                                <li><a href="#">Solar System</a></li>
                            </ul> 
                            <div class="author-icon">
                                <span class="icon-user"></span>    
                            </div>  
                        </div> 
                        <div class="text">
                            <p>Our power of choice is untrammelled and do when nothing prevents all our being...</p>
                            <a class="btn-two" href="#">Read More<span class="icon-null"></span></a>
                        </div>  
                    </div>
                </div>
            </div>
            <!--End single blog post-->
            <!--Start single blog post-->
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-blog-post wow fadeInLeft" data-wow-delay="400ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <img src="css/images/blog/v1-3.jpg" alt="Awesome Image">
                        <div class="overlay-style-two"></div>
                        <div class="post-date">
                            <h3><span>Dec</span><br> 29</h3>
                        </div>
                    </div>
                    <div class="text-holder">
                        <h3 class="blog-title"><a href="blog-single.html">A Decline in Solar Growth: Root Cause Analysis</a></h3>
                        <div class="meta-box">
                            <ul class="meta-info">
                                <li><a href="#"><b>Rubin Santro</b></a></li>
                                <li><a href="#">Environment</a></li>
                            </ul> 
                            <div class="author-icon">
                                <span class="icon-user"></span>    
                            </div>  
                        </div> 
                        <div class="text">
                            <p>Frequently occur that pleasures have to be all repudiated & annoyances accepted...</p>
                            <a class="btn-two" href="#">Read More<span class="icon-null"></span></a>
                        </div>  
                    </div>
                </div>
            </div>
            <!--End single blog post-->
            
            <!--Start single blog post-->
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-blog-post wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <img src="css/images/blog/v1-4.jpg" alt="Awesome Image">
                        <div class="overlay-style-two"></div>
                        <div class="post-date">
                            <h3><span>Dec</span><br> 29</h3>
                        </div>
                    </div>
                    <div class="text-holder">
                        <h3 class="blog-title"><a href="blog-single.html">A Decline in Solar Growth: Root Cause Analysis</a></h3>
                        <div class="meta-box">
                            <ul class="meta-info">
                                <li><a href="#"><b>Rubin Santro</b></a></li>
                                <li><a href="#">Environment</a></li>
                            </ul> 
                            <div class="author-icon">
                                <span class="icon-user"></span>    
                            </div>  
                        </div> 
                        <div class="text">
                            <p>Frequently occur that pleasures have to be all repudiated & annoyances accepted...</p>
                            <a class="btn-two" href="#">Read More<span class="icon-null"></span></a>
                        </div>  
                    </div>
                </div>
            </div>
            <!--End single blog post-->
            <!--Start single blog post-->
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-blog-post wow fadeInLeft" data-wow-delay="200ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <img src="css/images/blog/v1-5.jpg" alt="Awesome Image">
                        <div class="overlay-style-two"></div>
                        <div class="post-date">
                            <h3><span>Jan</span><br> 31</h3>
                        </div>
                    </div>
                    <div class="text-holder">
                        <h3 class="blog-title"><a href="blog-single.html">Rising Oil Price Should Re-Focus US On Solar Energy Growth</a></h3>
                        <div class="meta-box">
                            <ul class="meta-info">
                                <li><a href="#"><b>Rubin Santro</b></a></li>
                                <li><a href="#">Environment</a></li>
                            </ul> 
                            <div class="author-icon">
                                <span class="icon-user"></span>    
                            </div>  
                        </div> 
                        <div class="text">
                            <p>Same as saying through shrinking from toil & pain these cases perfectly simple...</p>
                            <a class="btn-two" href="#">Read More<span class="icon-null"></span></a>
                        </div>  
                    </div>
                </div>
            </div>
            <!--End single blog post-->
            <!--Start single blog post-->
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-blog-post wow fadeInLeft" data-wow-delay="400ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <img src="css/images/blog/v1-6.jpg" alt="Awesome Image">
                        <div class="overlay-style-two"></div>
                        <div class="post-date">
                            <h3><span>Jan</span><br> 06</h3>
                        </div>
                    </div>
                    <div class="text-holder">
                        <h3 class="blog-title"><a href="blog-single.html">Tax and Duties Are Acting As Road Block for Power for All!</a></h3>
                        <div class="meta-box">
                            <ul class="meta-info">
                                <li><a href="#"><b>Richard Antony</b></a></li>
                                <li><a href="#">Solar System</a></li>
                            </ul> 
                            <div class="author-icon">
                                <span class="icon-user"></span>    
                            </div>  
                        </div> 
                        <div class="text">
                            <p>Our power of choice is untrammelled and do when nothing prevents all our being...</p>
                            <a class="btn-two" href="#">Read More<span class="icon-null"></span></a>
                        </div>  
                    </div>
                </div>
            </div>
            <!--End single blog post-->
            
            <!--Start single blog post-->
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-blog-post wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <img src="css/images/blog/v1-7.jpg" alt="Awesome Image">
                        <div class="overlay-style-two"></div>
                        <div class="post-date">
                            <h3><span>Jan</span><br> 06</h3>
                        </div>
                    </div>
                    <div class="text-holder">
                        <h3 class="blog-title"><a href="blog-single.html">Tax and Duties Are Acting As Road Block for Power for All!</a></h3>
                        <div class="meta-box">
                            <ul class="meta-info">
                                <li><a href="#"><b>Richard Antony</b></a></li>
                                <li><a href="#">Solar System</a></li>
                            </ul> 
                            <div class="author-icon">
                                <span class="icon-user"></span>    
                            </div>  
                        </div> 
                        <div class="text">
                            <p>Our power of choice is untrammelled and do when nothing prevents all our being...</p>
                            <a class="btn-two" href="#">Read More<span class="icon-null"></span></a>
                        </div>  
                    </div>
                </div>
            </div>
            <!--End single blog post-->
            <!--Start single blog post-->
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-blog-post wow fadeInLeft" data-wow-delay="200ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <img src="css/images/blog/v1-8.jpg" alt="Awesome Image">
                        <div class="overlay-style-two"></div>
                        <div class="post-date">
                            <h3><span>Dec</span><br> 29</h3>
                        </div>
                    </div>
                    <div class="text-holder">
                        <h3 class="blog-title"><a href="blog-single.html">A Decline in Solar Growth: Root Cause Analysis</a></h3>
                        <div class="meta-box">
                            <ul class="meta-info">
                                <li><a href="#"><b>Rubin Santro</b></a></li>
                                <li><a href="#">Environment</a></li>
                            </ul> 
                            <div class="author-icon">
                                <span class="icon-user"></span>    
                            </div>  
                        </div> 
                        <div class="text">
                            <p>Frequently occur that pleasures have to be all repudiated & annoyances accepted...</p>
                            <a class="btn-two" href="#">Read More<span class="icon-null"></span></a>
                        </div>  
                    </div>
                </div>
            </div>
            <!--End single blog post-->
            <!--Start single blog post-->
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-blog-post wow fadeInLeft" data-wow-delay="400ms" data-wow-duration="1500ms">
                    <div class="img-holder">
                        <img src="css/images/blog/v1-9.jpg" alt="Awesome Image">
                        <div class="overlay-style-two"></div>
                        <div class="post-date">
                            <h3><span>Jan</span><br> 31</h3>
                        </div>
                    </div>
                    <div class="text-holder">
                        <h3 class="blog-title"><a href="blog-single.html">Rising Oil Price Should Re-Focus US On Solar Energy Growth</a></h3>
                        <div class="meta-box">
                            <ul class="meta-info">
                                <li><a href="#"><b>Rubin Santro</b></a></li>
                                <li><a href="#">Environment</a></li>
                            </ul> 
                            <div class="author-icon">
                                <span class="icon-user"></span>    
                            </div>  
                        </div> 
                        <div class="text">
                            <p>Same as saying through shrinking from toil & pain these cases perfectly simple...</p>
                            <a class="btn-two" href="#">Read More<span class="icon-null"></span></a>
                        </div>  
                    </div>
                </div>
            </div>
            <!--End single blog post-->
        </div>
    </div>
</section>
<!--End blog area-->

<!--Start footer area-->  
<?php site_footer(); ?>