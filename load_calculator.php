<?php
error_reporting(0);
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/online_ups_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
  <style>
    .container-outer img{
      margin-top:-55px;
      height:100px;
    }
  </style>
  <div class="page-wrapper">
    <!-- Header Here -->
    <?php bottom_menu(); ?>
    <?php site_top_menu(); ?>
    <?php slide_slider(); ?>




    <script>
     $(document).ready(function() {

      $('.faq_question').click(function() {

        if ($(this).parent().is('.open')){
          $(this).closest('.faq').find('.faq_answer_container').animate({'height':'0'},200);
          $(this).closest('.faq').removeClass('open');

        }else{
          var newHeight =$(this).closest('.faq').find('.faq_answer').height() +'px';
          $(this).closest('.faq').find('.faq_answer_container').animate({'height':newHeight},200);
          $(this).closest('.faq').addClass('open');
        }

      });

    });
  </script>


  <style>

    p{
      padding: 0 !important;
      margin: 0 !important;
    }

    .faq_question {
      margin: 0px;
      padding: 0px 0px 5px 0px;
      display: inline-block;
      cursor: pointer;
      font-weight: bold;
    }

    .faq_answer_container {
      height: 0px;
      overflow: hidden;
      padding: 0px;
    }

  </style>






  <section>
    <div class="shopping_cart">
     <div class="container2" style="padding-left: 20px;padding-right: 20px;">


      <div class="row">
        <div class="col-md-12 text-center">
          <p style="font-weight: 600;">Ever wondered about the optimal UPS Batteries capacity needed?</p>
          <p>Our easy to use "Power Consumption Calculator" will help you to know the exact total load requirements for your property!</p>
          <p style="margin-top: 20px !important;">Invest wisely with this simple three step process</p>
        </div>
      </div>

      <div style="padding-left: 100px;padding-right: 100px;">
        <div class="row" style="margin-top: 40px;">
          <div class="col-md-4 text-center">
            <p style="font-weight: 500;font-size: 20px">1) Define your load requirement </p>
          </div>
          <div class="col-md-4 text-center">
            <p style="font-weight: 500;font-size: 20px">2) Define your load backup requirement</p>
          </div>
          <div class="col-md-4 text-center">
            <p style="font-weight: 500;font-size: 20px">3) Calculate</p>
          </div>
        </div>
      </div>

      <div class="row" style="margin-top: 20px;">
        <div class="col-md-12" style="padding: 10px 5px 10px 5px;background-color: #e5e5e5;">
          <p style="font-weight: 500;font-size: 20px">1) Define your load requirement</p>
        </div>
      </div>

      <p style="font-size: 18px;margin-top: 20px !important;">Select your Appliances</p>

      <div class="row" style="margin-top: 20px;">
        <div class="col-md-6" style="padding: 10px 5px 10px 10px;background-color: #e5e5e5;border-right: 2px solid white;">
          <div class="row">
            <div class="col-md-6">
              <p>Appliances</p>
            </div>
            <div class="col-md-3">
              <p>Wattage</p>
            </div>
            <div class="col-md-3">
              <p>Quantity</p>
            </div>
          </div>
        </div>

        <div class="col-md-6" style="padding: 10px 5px 10px 10px;background-color: #e5e5e5;border-left:2px solid white;">
          <div class="row">
            <div class="col-md-6">
              <p>Appliances</p>
            </div>
            <div class="col-md-3">
              <p>Wattage</p>
            </div>
            <div class="col-md-3">
              <p>Quantity</p>
            </div>
          </div>
        </div>
      </div>


<!-----------1------------->
      <div class="row" style="margin-top: 20px;">
        <div class="col-md-6 faq" style="padding: 10px 5px 4px 10px;background-color: #4770c2;border-right: 2px solid white;">

          <div class="row">
            <div class="col-md-12 faq_question" style="padding-left: 20px;">
              <div class="row">
               <div class="col-md-6">
                <p style="color: white;">+ Fans And Coolers</p>
              </div>
              <div class="col-md-3">
                <input type="number" name="" value="0" style="max-width: 60px;" disabled>
              </div>
              <div class="col-md-3">
                <input type="number" name="" value="0" style="max-width: 60px;" disabled>
              </div>
            </div>
          </div>
          <div class="col-md-12 faq_answer_container" style="padding-left: 10px;padding-right:14px;">
            <div class="faq_answer" style="background-color: white;">
              <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Ceiling Fan</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  75
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Table Fan</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  50
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Room Cooler</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  250
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </div>
      </div>

              <div class="col-md-6 faq" style="padding: 10px 5px 4px 10px;background-color: #4770c2;border-right: 2px solid white;">

          <div class="row">
            <div class="col-md-12 faq_question" style="padding-left: 20px;">
              <div class="row">
               <div class="col-md-6">
                <p style="color: white;">+ Laptops and Computers</p>
              </div>
              <div class="col-md-3">
                <input type="number" name="" value="0" style="max-width: 60px;" disabled>
              </div>
              <div class="col-md-3">
                <input type="number" name="" value="0" style="max-width: 60px;" disabled>
              </div>
            </div>
          </div>
          <div class="col-md-12 faq_answer_container" style="padding-left: 10px;padding-right:14px;">
            <div class="faq_answer" style="background-color: white;">
              <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Laptop</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  100
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Desktop computer</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  200
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Laser Printer (Small)</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  200
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </div>
      </div>

    </div>
<!---------------------->

<!----------2-------------->
      <div class="row" style="margin-top: 20px;">
        <div class="col-md-6 faq" style="padding: 10px 5px 4px 10px;background-color: #4770c2;border-right: 2px solid white;">

          <div class="row">
            <div class="col-md-12 faq_question" style="padding-left: 20px;">
              <div class="row">
               <div class="col-md-6">
                <p style="color: white;">+ Lights</p>
              </div>
              <div class="col-md-3">
                <input type="number" name="" value="0" style="max-width: 60px;" disabled>
              </div>
              <div class="col-md-3">
                <input type="number" name="" value="0" style="max-width: 60px;" disabled>
              </div>
            </div>
          </div>
          <div class="col-md-12 faq_answer_container" style="padding-left: 10px;padding-right:14px;">
            <div class="faq_answer" style="background-color: white;">
              <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Tubelight</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  40
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Tubelight</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  20
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>CFL Heavy</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  30
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                          <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>CFL Light</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  15
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                          <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>LED Bulb</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  9
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                          <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>LED Bulb</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  5
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                          <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Light Bulb (Incandescent)</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  40
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                          <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Light Bulb (Incandescent)</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  60
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                          <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Light Bulb (Incandescent)</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  100
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </div>
      </div>

              <div class="col-md-6 faq" style="padding: 10px 5px 4px 10px;background-color: #4770c2;border-right: 2px solid white;">

          <div class="row">
            <div class="col-md-12 faq_question" style="padding-left: 20px;">
              <div class="row">
               <div class="col-md-6">
                <p style="color: white;">+ Home Appliances</p>
              </div>
              <div class="col-md-3">
                <input type="number" name="" value="0" style="max-width: 60px;" disabled>
              </div>
              <div class="col-md-3">
                <input type="number" name="" value="0" style="max-width: 60px;" disabled>
              </div>
            </div>
          </div>
          <div class="col-md-12 faq_answer_container" style="padding-left: 10px;padding-right:14px;">
            <div class="faq_answer" style="background-color: white;">
              <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Juicer Mixer Grinder</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  800
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Toaster</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  800
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Refrigerator (upto 200L)</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  300
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                         <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Refrigerator (upto 500L)</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  500
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                         <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Microwave Oven</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  1400
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                         <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Vacuum Cleaner</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  1400
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                         <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Washing Machine</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  1000
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                         <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Geyser / Water Heater</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  2200
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                         <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Room Heater</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  2200
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </div>
      </div>

    </div>
<!---------------------->


<!----------3-------------->
      <div class="row" style="margin-top: 20px;">
        <div class="col-md-6 faq" style="padding: 10px 5px 4px 10px;background-color: #4770c2;border-right: 2px solid white;">

          <div class="row">
            <div class="col-md-12 faq_question" style="padding-left: 20px;">
              <div class="row">
               <div class="col-md-6">
                <p style="color: white;">+ TV & other entertainment</p>
              </div>
              <div class="col-md-3">
                <input type="number" name="" value="0" style="max-width: 60px;" disabled>
              </div>
              <div class="col-md-3">
                <input type="number" name="" value="0" style="max-width: 60px;" disabled>
              </div>
            </div>
          </div>
          <div class="col-md-12 faq_answer_container" style="padding-left: 10px;padding-right:14px;">
            <div class="faq_answer" style="background-color: white;">
              <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Television LED (upto 40")</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  60
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Television CRT (upto 21")</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  100
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Television Plasma</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                250
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                          <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Set Top Box (DTH)</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  50
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                          <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Music System</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  300
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                          <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Gaming Console</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  200
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </div>
      </div>

              <div class="col-md-6 faq" style="padding: 10px 5px 4px 10px;background-color: #4770c2;border-right: 2px solid white;">

          <div class="row">
            <div class="col-md-12 faq_question" style="padding-left: 20px;">
              <div class="row">
               <div class="col-md-6">
                <p style="color: white;">+ ACs</p>
              </div>
              <div class="col-md-3">
                <input type="number" name="" value="0" style="max-width: 60px;" disabled>
              </div>
              <div class="col-md-3">
                <input type="number" name="" value="0" style="max-width: 60px;" disabled>
              </div>
            </div>
          </div>
          <div class="col-md-12 faq_answer_container" style="padding-left: 10px;padding-right:14px;">
            <div class="faq_answer" style="background-color: white;">
              <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Air Conditioner (1 Ton, 3 star)</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  1200
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Air Conditioner (1.5 Ton, 3 star)</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  1700
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Air Conditioner (2 Ton, 3 star)</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  2300
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                         <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Air Conditioner (1 Ton, Inverter)</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  1100
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                         <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Air Conditioner (1.5 Ton, Inverter)</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  1600
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                         <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Air Conditioner (2 Ton, Inverter)</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  2100
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </div>
      </div>

    </div>
<!---------------------->

<!----------4-------------->
      <div class="row" style="margin-top: 20px;">
        <div class="col-md-6 faq" style="padding: 10px 5px 4px 10px;background-color: #4770c2;border-right: 2px solid white;">

          <div class="row">
            <div class="col-md-12 faq_question" style="padding-left: 20px;">
              <div class="row">
               <div class="col-md-6">
                <p style="color: white;">+ Others</p>
              </div>
              <div class="col-md-3">
                <input type="number" name="" value="0" style="max-width: 60px;" disabled>
              </div>
              <div class="col-md-3">
                <input type="number" name="" value="0" style="max-width: 60px;" disabled>
              </div>
            </div>
          </div>
          <div class="col-md-12 faq_answer_container" style="padding-left: 10px;padding-right:14px;">
            <div class="faq_answer" style="background-color: white;">
              <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Photo Copier</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  2000
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Office Printer/Scanner</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  2000
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Petrol Filling Machine</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                1500
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                          <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Projector</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  600
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                                          <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Surveillance System</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  100
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </div>
      </div>

              <div class="col-md-6 faq" style="padding: 10px 5px 4px 10px;background-color: #4770c2;border-right: 2px solid white;">

          <div class="row">
            <div class="col-md-12 faq_question" style="padding-left: 20px;">
              <div class="row">
               <div class="col-md-6">
                <p style="color: white;">+ Motors</p>
              </div>
              <div class="col-md-3">
                <input type="number" name="" value="0" style="max-width: 60px;" disabled>
              </div>
              <div class="col-md-3">
                <input type="number" name="" value="0" style="max-width: 60px;" disabled>
              </div>
            </div>
          </div>
          <div class="col-md-12 faq_answer_container" style="padding-left: 10px;padding-right:14px;">
            <div class="faq_answer" style="background-color: white;">
              <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Water Pump (0.5 HP)</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  400
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
                            <div class="row">
                <div class="col-md-6" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  <p>Water Pump (1 HP)</p>
                </div>
                <div class="col-md-3" style="padding-left: 40px;padding-top: 10px;padding-bottom: 10px;">
                  800
                </div>
                <div class="col-md-3" style="padding-top: 4px;">
                  <div class="btn-group">
                    <button class="btn btn-primary">-</button>
                    <button style="width: 50px;">00</button>
                    <button class="btn btn-primary">+</button>
                  </div>
                </div>
              </div>
            </div>
          </div>


        </div>
      </div>

    </div>
<!---------------------->


      <div class="row" style="margin-top: 60px;">
        <div class="col-md-12" style="padding: 10px 5px 10px 5px;background-color: #e5e5e5;">
          <p style="font-weight: 500;font-size: 20px">2) Define your Backup requirement</p>
        </div>
      </div>

      <div class="row" style="margin-top: 40px;">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-4">
              <p>Total Load(watt)</p>
            </div>
            <div class="col-md-8">
              <input type="" name="" placeholder="0" class="form-control" disabled>
            </div>
          </div>
        </div>
        <div class="col-md-6">
            <div class="row">
            <div class="col-md-4">
              <p>Average Running Load</p>
            </div>
            <div class="col-md-8">
              <select name="load" id="load" class="form-control">
                <option value="20">20%</option>
                <option value="50">50%</option>
                <option value="75">75%</option>
                <option value="100">100%</option>
              </select>
            </div>
          </div>
        </div>
      </div>

            <div class="row" style="margin-top: 40px;">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-4">
              <p>Backup Requirement (hrs)</p>
            </div>
            <div class="col-md-8">
              <input type="" name="" placeholder="0" class="form-control">
            </div>
          </div>
        </div>
        <div class="col-md-6">
            <div class="row">
            <div class="col-md-4">
              <p>Average Running Load</p>
            </div>
            <div class="col-md-8">
            <input type="" name="" placeholder="Home UPS(C20)" class="form-control" disabled>
            </div>
          </div>
        </div>
      </div>

            <div class="row" style="margin-top: 60px;">
        <div class="col-md-12" style="padding: 10px 5px 10px 5px;background-color: #e5e5e5;">
          <p style="font-weight: 500;font-size: 20px">3) Calculate</p>
        </div>
      </div>

      <div class="row" style="margin-top: 40px;">
        <div class="col-md-5">
          <div class="btn-group">
            <button class="btn btn-primary" style="margin-right: 20px;">CALCULATE</button>
            <button class="btn btn-primary">RESET</button>
          </div>
        </div>
        <div class="col-md-7"></div>
      </div>

      <div class="row" style="margin-top: 60px;">
        <div class="col-md-12" style="padding: 60px 20px 60px 20px;background-color: #f0f0f0;">
          
          <p style="font-weight: 500;">Ever Wondered About The Optimal UPS Batteries Capacity Needed?</p>
          <p>Our easy to use "Power Consumption Calculator" will help you to know the exact total load requirements for your property to help you buy the best battery for inverter. The UPS battery price depends on the power required to run home appliances during a power outage.</p>
          <p style="margin-top: 30px;">Invest wisely with this simple three-step process:</p>
          <p style="font-weight: 500;">1. Define your load requirement</p>
          <p>Start by analyzing the load requirement before you buy the best battery for inverter. Calculate it by adding all the power ratings of all appliances, watts used, and the quantity to comprehend the need for the right UPS and inverter battery for homeuseor office.</p>
          <p style="font-weight: 500;">2. Define your backup requirement</p>
          <p>The total load or peak load would be displayed from above step, fill in average running load and the backup requirement in hours to define which inverter battery for your homesuits your needs.Once you have defined your backup requirements, you can go ahead and check ainverter connection with battery that fulfills your backup needs adequately.</p>
          <p style="font-weight: 500;">3. Calculate</p>
          <p>Last, calculate the capacity of the UPS inverter battery for your home or office space. The Load calculator displays the ideal Inverter battery combo for your home or commercial establishments. Luminous has a comprehensive range of inverter and batteries to fulfill your power backup requirement. Check theinverter and battery price list before choosing a large or small inverter battery according to your specific needs.


</p>
       
        </div>
      </div>

  </div>
</div>

</section>


<!----------------Adding/subracting quantity from cart------------------->
<script type="text/javascript">

 function addfunc(id)
 { 
   window.location.href='add_quantity_GET.php?id='+id;
 }


 function subtractfunc(id)
 { 
   window.location.href='subtract_quantity_GET.php?id='+id;
 }

</script>


<!-----------------Deleting product from cart-------------------->
<script type="text/javascript">
  function deletefunc(id)
  { 
    var r = confirm("Remove Product From cart?");
    if (r == true) {


     var dataString = 'id='+ id;


     $.ajax({
      type: "POST",
      url: "remove_cart_product.php",
      data: dataString,
      cache: false,
      success:function(data){
        if(data == 1){

          swal("", "Product Removed From The Cart.", "info");

          setTimeout(function(){
           window.location.reload();
         }, 3000);



        }
        else{


        }

      }
    });

   }
 }

</script>








<?php site_footer(); ?>
