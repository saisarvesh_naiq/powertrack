
<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/site_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
    <style>
        .container-outer img{
            margin-top:-55px;
            height:100px;
        }
    </style>
    <div class="page-wrapper">
      <!-- Header Here -->
      <?php bottom_menu(); ?>
      <?php site_top_menu(); ?>
      <?php slide_slider(); ?>  
      <!--End breadcrumb area-->

      <!--Start Checkout area-->

<?php
if (!isset($_SESSION['user_id'])) {

    echo "<script>
    alert('Please login before checking out!');
    window.location.href='login.php';
    </script>";

}
else{
require 'connect.php';
$stmt = $conn->prepare('SELECT * FROM cust_order_data WHERE user_id="'.$_SESSION['user_id'].'" AND status="1"');
$stmt->execute(); 
$stmt->setFetchMode(PDO::FETCH_ASSOC);
if($stmt->rowCount()>0){

}
else
{
 echo "<script>
    alert('There are no products in the cart!Please add some.');
    window.location.href='cart.php';
    </script>";
}

}


?>


<?php

require 'connect.php';
$stmt2 = $conn->prepare('SELECT * FROM user_sub_details WHERE user_id="'.$_SESSION['user_id'].'" AND status="1"');
$stmt2->execute(); 
$stmt2->setFetchMode(PDO::FETCH_ASSOC);
if($stmt2->rowCount()>0){
   foreach (($stmt2->fetchAll()) as $key => $row3) {


echo '


      <section class="checkout-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="exisitng-customer">
                        <h5>Exisitng Customer?<a href="#">Click here to login</a></h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="coupon">
                        <h5>Have a Coupon?  <a href="#">Click here to enter your code</a></h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="form billing-info">
                        <div class="shop-title-box">
                            <h3>Shiping Details</h3>


                        </div>
                        <form method="post" action="">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="field-label">Country*</div>
                                    <div class="field-input">
                                        <input type="text" value="'.$row3['country'].'" name="country" placeholder="" id="b_country">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="field-label">First Name*</div>
                                    <div class="field-input">
                                        <input type="text" value="'.$row3['firstname'].'" name="fname" placeholder="" id="b_firstname">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="field-label">Last Name *</div>
                                    <div class="field-input">
                                        <input type="text" value="'.$row3['lastname'].'" name="lname" placeholder="" id="b_lastname">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="field-label">Address *</div>
                                    <div class="field-input">
                                        <input type="text" value="'.$row3['address'].'" name="address" placeholder="" id="b_address">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="field-label">Town / City *</div>
                                    <div class="field-input">
                                        <input type="text" value="'.$row3['town'].'" name="town-city" placeholder="" id="b_town">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="field-label">Contact Info *</div>
                                    <div class="field-input">
                                        <input type="text" value="'.$row3['email_id'].'" name="email" placeholder="Email Address" id="b_email">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="field-label">Phone Number *</div>
                                    <div class="field-input">
                                        <input type="number" value="'.$row3['contact_number'].'" class="form-control" name="phone" placeholder="Phone Number" id="b_contact">
                                    </div>
                                </div>';
            }
}

 ?>

                        </div>    
                    </form>
                </div>    
            </div>
<!--             <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                <div class="form shipping-info">
                    <div class="shop-title-box">
                        <h3>Shipping to a Different Address<input type="checkbox"></h3>
                    </div>
                    <form method="post" action="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="field-label">Country*</div>
                                <div class="field-input">
                                    <input type="text" name="country" placeholder="" id="s_country">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="field-label">First Name*</div>
                                <div class="field-input">
                                    <input type="text" name="fname" placeholder="" id="s_firstname">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="field-label">Last Name *</div>
                                <div class="field-input">
                                    <input type="text" name="lname" placeholder="" id="s_lastname">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="field-label">Address *</div>
                                <div class="field-input">
                                    <input type="text" name="address" placeholder="" id="s_address">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="field-label">Town / City *</div>
                                <div class="field-input">
                                    <input type="text" name="town-city" placeholder="" id="s_town">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="field-label">Other Notes</div>
                                <div class="field-input">
                                    <textarea name="other-notes" placeholder="Special notes about your order..." id="order_note"></textarea>
                                </div>
                            </div>
                            
                        </div>    
                    </form>
                </div>    
            </div>  --> 
        </div>
    </div>
    <div class="bottom">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                    <div class="table">
                        <div class="shop-title-box">
                            <h3>Order Summary</h3>
                        </div>
                        <table class="cart-table">
                            <thead class="cart-header">
                                <tr>
                                    <th class="product-column">Products</th>
                                    <th>&nbsp;</th>
                                    <th>Quantity</th>
                                    <th class="price">Total</th>
                                </tr>    
                            </thead>
                            <tbody>

             <?php
        require 'connect.php';
        $sql= $conn->prepare("SELECT * FROM cust_order_data WHERE user_id='".$_SESSION["user_id"]."' AND status='1'");
        $sql->execute();
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        if($sql->rowCount()>0){
          foreach (($sql->fetchAll()) as $key => $row) {

        $sql2= $conn->prepare("SELECT * FROM products_db WHERE prod_id='".$row["prod_id"]."' AND status='1'");
        $sql2->execute();
        $sql2->setFetchMode(PDO::FETCH_ASSOC);
        if($sql2->rowCount()>0){
          foreach (($sql2->fetchAll()) as $key => $row2) {

            
            echo '

                                <tr>
                                    <td colspan="2" class="product-column">
                                        <div class="column-box">
                                            <div class="prod-thumb">
                                                <a href="#"><img src="admin/products/'.$row2['pro_image_1'].'" alt=""></a>
                                            </div>
                                            <div class="product-title">
                                                <h3>'.$row2['prod_name'].'</h3>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="qty">
                                        <input class="quantity-spinner" type="text" value="'.$row['prod_qty'].'" name="quantity" disabled>
                                    </td>
                                    <td class="price">&#8377; '.$row['prod_total_price'].'</td>
                                </tr> ';

                            }
                        }

                    }
                }
                        ?>


                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                    <div class="cart-total">
                        <div class="shop-title-box">
                            <h3>Cart Totals</h3>
                        </div>
                        <ul class="cart-total-table">
                            <li class="clearfix">
                            
                            <?php 
    require 'connect.php';
    $data2 = $conn->query("SELECT SUM(prod_total_price) as Total2 FROM cust_order_data WHERE user_id='".$_SESSION["user_id"]."' AND status='1'")->fetchAll();
    foreach ($data2 as $row4) { 



      ?>


                                <span class="col col-title">Cart Subtotal</span>
                                <span class="col">&#8377; <?php echo $row4['Total2']; ?></span>    
                            </li>
                            <li class="clearfix">
                                <span class="col col-title">Shipping and Handling</span>
                                <span class="col">&#8377; 400.00</span>    
                            </li>
                            <li class="clearfix">
                                <span class="col col-title">Order Total</span>
                                <span class="col">&#8377; <?php echo $row4['Total2']; ?></span>    
                            </li>      
                        </ul>

                        <?php 
                    }
                    ?>

                        <div class="payment-options">
                            <div class="option-block">
                              
                             <select name="category" id="payment_method" class="form-control" style="background-color: #fafafa;"> 
                      <option value="" style="background-color: #fafafa;"> Select Payment Method</option>
                      <option value="COD" style="background-color: #fafafa;"> Cash On Delivery</option>
                      <option value="Paytm" style="background-color: #fafafa;"> Paytm</option>
                      <option value="net banking" style="background-color: #fafafa;"> Bank Transfer</option>
                            
                            </select>


                            </div>
                            <div class="option-block">
                                <div class="radio-block">

                                </div>
                            </div>
                            <div class="placeorder-button text-left">
                                <button class="btn-three" type="submit" id="place_order_btn">Place Order<span class="icon-null"></span></button>
                            </div>   
                        </div>          
                    </div>    
                </div>
            </div>
        </div>    
    </div>
</section>         






  <script>
    
   $("#place_order_btn").click(function(){

  var b_country=$("#b_country").val();
  var b_firstname=$("#b_firstname").val();
  var b_lastname=$("#b_lastname").val();
  var b_address=$("#b_address").val();
  var b_town=$("#b_town").val();
  var b_email=$("#b_email").val();
  var b_contact=$("#b_contact").val();
  var payment_method=$("#payment_method").val();
  

  var datastr='b_country='+b_country+'&b_firstname='+b_firstname+'&b_lastname='+b_lastname+'&b_address='+b_address+'&b_town='+b_town+'&b_email='+b_email+'&b_contact='+b_contact+'&payment_method='+payment_method;

  if (b_country=='') {
   alert("Enter Country Name");

 }
 else if(b_firstname==''){
  alert("Enter your Name");

}

else if (b_lastname=='') {
 alert("Enter your Last Name")
}

else if (b_address=='') {
 alert("Please provide shipping address")
}

else if (b_town=='') {
 alert("Enter your town");

}

else if (b_email=='') {
 alert("Enter email");

}

else if (b_contact=='') {
 alert("Please provide contact number");

}

else if (payment_method=='') {
 alert("Please choose a payment method");

}



else{

 $.ajax({


  type: "POST",
  url: "checkout_GET.php",
  data: datastr,
  cache: false,
  success: function(res){
    if(res==1)
    {
      alert("Your Order was Placed Successfully!");
      window.location.href="add_product.php";

    }
    else{

     alert("Product ordered Successfully");
     window.location.href="index.php";


   }

 }
}); 
}

});
  </script>







<!--End Checkout area-->

<!--Start footer area-->  
<?php site_footer(); ?>