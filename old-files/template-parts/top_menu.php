<?php function site_top_menu() { ?>
      <!-- quickview-modal -->
      <div id="quickview-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content"></div>
         </div>
      </div>
      <!-- quickview-modal / end --><!-- mobilemenu -->
      <div class="mobilemenu">
         <div class="mobilemenu__backdrop"></div>
         <div class="mobilemenu__body">
            <div class="mobilemenu__header">
               <div class="mobilemenu__title">Menu</div>
               <button type="button" class="mobilemenu__close">
					<i class="fa fa-times" aria-hidden="true"></i>
               </button>
            </div>
            <div class="mobilemenu__content">
               <ul class="mobile-links mobile-links--level--0" data-collapse data-collapse-opened-class="mobile-links__item--open">
                  <li class="mobile-links__item" data-collapse-item>
                     <div class="mobile-links__item-title">
                        <a href="index.html" class="mobile-links__item-link">Home</a> 
                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
							<!-- button toggele -->
                        </button>
                     </div>
                     <div class="mobile-links__item-sub-links" data-collapse-content>
                        <ul class="mobile-links mobile-links--level--1">
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="index.html" class="mobile-links__item-link">Home 1</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="index-2.html" class="mobile-links__item-link">Home 2</a></div>
                           </li>
                        </ul>
                     </div>
                  </li>
                <!--  <li class="mobile-links__item" data-collapse-item>
                     <div class="mobile-links__item-title">
                        <a href="#" class="mobile-links__item-link">Online Ups</a> 
                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                          <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </button>
                     </div>
                     <div class="mobile-links__item-sub-links" data-collapse-content>
                        <ul class="mobile-links mobile-links--level--1">
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title">
                                 <a href="#" class="mobile-links__item-link">Power Tools</a> 
                                 <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                 </button>
                              </div>
                              <div class="mobile-links__item-sub-links" data-collapse-content>
                                 <ul class="mobile-links mobile-links--level--2">
                                    <li class="mobile-links__item" data-collapse-item>
                                       <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">Engravers</a></div>
                                    </li>
                                    <li class="mobile-links__item" data-collapse-item>
                                       <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">Wrenches</a></div>
                                    </li>
                                    <li class="mobile-links__item" data-collapse-item>
                                       <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">Wall Chaser</a></div>
                                    </li>
                                    <li class="mobile-links__item" data-collapse-item>
                                       <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">Pneumatic Tools</a></div>
                                    </li>
                                 </ul>
                              </div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title">
                                 <a href="#" class="mobile-links__item-link">Machine Tools</a> 
                                 <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                 </button>
                              </div>
                              <div class="mobile-links__item-sub-links" data-collapse-content>
                                 <ul class="mobile-links mobile-links--level--2">
                                    <li class="mobile-links__item" data-collapse-item>
                                       <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">Thread Cutting</a></div>
                                    </li>
                                    <li class="mobile-links__item" data-collapse-item>
                                       <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">Chip Blowers</a></div>
                                    </li>
                                    <li class="mobile-links__item" data-collapse-item>
                                       <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">Sharpening Machines</a></div>
                                    </li>
                                    <li class="mobile-links__item" data-collapse-item>
                                       <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">Pipe Cutters</a></div>
                                    </li>
                                    <li class="mobile-links__item" data-collapse-item>
                                       <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">Slotting machines</a></div>
                                    </li>
                                    <li class="mobile-links__item" data-collapse-item>
                                       <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">Lathes</a></div>
                                    </li>
                                 </ul>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </li>
                  <li class="mobile-links__item" data-collapse-item>
                     <div class="mobile-links__item-title">
                        <a href="shop-grid-3-columns-sidebar.html" class="mobile-links__item-link">Shop</a> 
                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                           <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </button>
                     </div>
                     <div class="mobile-links__item-sub-links" data-collapse-content>
                        <ul class="mobile-links mobile-links--level--1">
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title">
                                 <a href="shop-grid-3-columns-sidebar.html" class="mobile-links__item-link">Shop Grid</a> 
                                 <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                 </button>
                              </div>
                              <div class="mobile-links__item-sub-links" data-collapse-content>
                                 <ul class="mobile-links mobile-links--level--2">
                                    <li class="mobile-links__item" data-collapse-item>
                                       <div class="mobile-links__item-title"><a href="shop-grid-3-columns-sidebar.html" class="mobile-links__item-link">3 Columns Sidebar</a></div>
                                    </li>
                                    <li class="mobile-links__item" data-collapse-item>
                                       <div class="mobile-links__item-title"><a href="shop-grid-4-columns-full.html" class="mobile-links__item-link">4 Columns Full</a></div>
                                    </li>
                                    <li class="mobile-links__item" data-collapse-item>
                                       <div class="mobile-links__item-title"><a href="shop-grid-5-columns-full.html" class="mobile-links__item-link">5 Columns Full</a></div>
                                    </li>
                                 </ul>
                              </div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="shop-list.html" class="mobile-links__item-link">Shop List</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="shop-right-sidebar.html" class="mobile-links__item-link">Shop Right Sidebar</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title">
                                 <a href="product.html" class="mobile-links__item-link">Product</a> 
                                 <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                                   <i class="fa fa-angle-down" aria-hidden="true"></i>
                                 </button>
                              </div>
                              <div class="mobile-links__item-sub-links" data-collapse-content>
                                 <ul class="mobile-links mobile-links--level--2">
                                    <li class="mobile-links__item" data-collapse-item>
                                       <div class="mobile-links__item-title"><a href="product.html" class="mobile-links__item-link">Product</a></div>
                                    </li>
                                    <li class="mobile-links__item" data-collapse-item>
                                       <div class="mobile-links__item-title"><a href="product-alt.html" class="mobile-links__item-link">Product Alt</a></div>
                                    </li>
                                    <li class="mobile-links__item" data-collapse-item>
                                       <div class="mobile-links__item-title"><a href="product-sidebar.html" class="mobile-links__item-link">Product Sidebar</a></div>
                                    </li>
                                 </ul>
                              </div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="cart.html" class="mobile-links__item-link">Cart</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="checkout.html" class="mobile-links__item-link">Checkout</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="wishlist.html" class="mobile-links__item-link">Wishlist</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="compare.html" class="mobile-links__item-link">Compare</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="account.html" class="mobile-links__item-link">My Account</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="track-order.html" class="mobile-links__item-link">Track Order</a></div>
                           </li>
                        </ul>
                     </div>
                  </li>
                  <li class="mobile-links__item" data-collapse-item>
                     <div class="mobile-links__item-title">
                        <a href="blog-classic.html" class="mobile-links__item-link">Blog</a> 
                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                          <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </button>
                     </div>
                     <div class="mobile-links__item-sub-links" data-collapse-content>
                        <ul class="mobile-links mobile-links--level--1">
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="blog-classic.html" class="mobile-links__item-link">Blog Classic</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="blog-grid.html" class="mobile-links__item-link">Blog Grid</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="blog-list.html" class="mobile-links__item-link">Blog List</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="blog-left-sidebar.html" class="mobile-links__item-link">Blog Left Sidebar</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="post.html" class="mobile-links__item-link">Post Page</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="post-without-sidebar.html" class="mobile-links__item-link">Post Without Sidebar</a></div>
                           </li>
                        </ul>
                     </div>
                  </li>-->
                  <li class="mobile-links__item" data-collapse-item>
                     <div class="mobile-links__item-title">
                        <a href="#" class="mobile-links__item-link">Online UPs</a> 
                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                           <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </button>
                     </div>
                     <div class="mobile-links__item-sub-links" data-collapse-content>
                        <ul class="mobile-links mobile-links--level--1">
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="about-us.html" class="mobile-links__item-link">Product 1</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="contact-us.html" class="mobile-links__item-link">Product 2</a></div>
                           </li>
                          
                        </ul>
                     </div>
                  </li>
				              <li class="mobile-links__item" data-collapse-item>
                     <div class="mobile-links__item-title">
                        <a href="#" class="mobile-links__item-link">Luminuous Solar Division</a> 
                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                           <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </button>
                     </div>
                     <div class="mobile-links__item-sub-links" data-collapse-content>
                        <ul class="mobile-links mobile-links--level--1">
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="about-us.html" class="mobile-links__item-link">Product 1</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="contact-us.html" class="mobile-links__item-link">Product 2</a></div>
                           </li>
                          
                        </ul>
                     </div>
                  </li>
				  <li class="mobile-links__item" data-collapse-item>
                     <div class="mobile-links__item-title">
                        <a href="#" class="mobile-links__item-link">Stabilizers</a> 
                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                           <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </button>
                     </div>
                     <div class="mobile-links__item-sub-links" data-collapse-content>
                        <ul class="mobile-links mobile-links--level--1">
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="about-us.html" class="mobile-links__item-link">Product 1</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="contact-us.html" class="mobile-links__item-link">Product 2</a></div>
                           </li>
                          
                        </ul>
                     </div>
                  </li>
				  <li class="mobile-links__item" data-collapse-item>
                     <div class="mobile-links__item-title">
                        <a href="#" class="mobile-links__item-link">Inverters & Ups</a> 
                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                           <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </button>
                     </div>
                     <div class="mobile-links__item-sub-links" data-collapse-content>
                        <ul class="mobile-links mobile-links--level--1">
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="about-us.html" class="mobile-links__item-link">Product 1</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="contact-us.html" class="mobile-links__item-link">Product 2</a></div>
                           </li>
                          
                        </ul>
                     </div>
                  </li>
				  <li class="mobile-links__item" data-collapse-item>
                     <div class="mobile-links__item-title">
                        <a href="#" class="mobile-links__item-link">Genset Division</a> 
                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                           <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </button>
                     </div>
                     <div class="mobile-links__item-sub-links" data-collapse-content>
                        <ul class="mobile-links mobile-links--level--1">
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="about-us.html" class="mobile-links__item-link">Product 1</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="contact-us.html" class="mobile-links__item-link">Product 2</a></div>
                           </li>
                          
                        </ul>
                     </div>
                  </li>
				   <li class="mobile-links__item" data-collapse-item>
                     <div class="mobile-links__item-title">
                        <a href="#" class="mobile-links__item-link">Power Transformers</a> 
                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                           <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </button>
                     </div>
                     <div class="mobile-links__item-sub-links" data-collapse-content>
                        <ul class="mobile-links mobile-links--level--1">
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="about-us.html" class="mobile-links__item-link">Product 1</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="contact-us.html" class="mobile-links__item-link">Product 2</a></div>
                           </li>
                          
                        </ul>
                     </div>
                  </li>
				  <li class="mobile-links__item" data-collapse-item>
                     <div class="mobile-links__item-title">
                        <a href="#" class="mobile-links__item-link">Batteries</a> 
                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                           <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </button>
                     </div>
                     <div class="mobile-links__item-sub-links" data-collapse-content>
                        <ul class="mobile-links mobile-links--level--1">
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="about-us.html" class="mobile-links__item-link">Product 1</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="contact-us.html" class="mobile-links__item-link">Product 2</a></div>
                           </li>
                          
                        </ul>
                     </div>
                  </li>
                  <li class="mobile-links__item" data-collapse-item>
                     <div class="mobile-links__item-title">
                        <a data-collapse-trigger class="mobile-links__item-link">Currency</a> 
                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                          <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </button>
                     </div>
                     <div class="mobile-links__item-sub-links" data-collapse-content>
                        <ul class="mobile-links mobile-links--level--1">
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">€ Euro</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">£ Pound Sterling</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">$ US Dollar</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">₽ Russian Ruble</a></div>
                           </li>
                        </ul>
                     </div>
                  </li>
                  <li class="mobile-links__item" data-collapse-item>
                     <div class="mobile-links__item-title">
                        <a data-collapse-trigger class="mobile-links__item-link">Language</a> 
                        <button class="mobile-links__item-toggle" type="button" data-collapse-trigger>
                           <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </button>
                     </div>
                     <div class="mobile-links__item-sub-links" data-collapse-content>
                        <ul class="mobile-links mobile-links--level--1">
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">English</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">French</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">German</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">Russian</a></div>
                           </li>
                           <li class="mobile-links__item" data-collapse-item>
                              <div class="mobile-links__item-title"><a href="#" class="mobile-links__item-link">Italian</a></div>
                           </li>
                        </ul>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
      </div>



<?php } ?>