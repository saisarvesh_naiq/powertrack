<?php function bottom_menu() { ?>
<style>
.mobile-header__menu-button .fa-bars{
color:#fff;}
.mobile-header__menu-button .mobile-header__logo{
	color:#fff;
}
</style>
         <header class="site__header d-lg-none">
            <div class="mobile-header mobile-header--sticky mobile-header--stuck">
               <div class="mobile-header__panel">
                  <div class="container">
                     <div class="mobile-header__body">
                        <button class="mobile-header__menu-button">
                           <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                        <a class="mobile-header__logo" href="index.html">
							POWERTRAC
                        </a>
                        <div class="mobile-header__search">
                           <form class="mobile-header__search-form" action="#">
                              <input class="mobile-header__search-input" name="search" placeholder="Search over 10,000 products" aria-label="Site search" type="text" autocomplete="off"> 
                              <button class="mobile-header__search-button mobile-header__search-button--submit" type="submit">
                                <i class="fa fa-search" aria-hidden="true"></i>
                              </button>
                              <button class="mobile-header__search-button mobile-header__search-button--close" type="button">
                                <i class="fa fa-times" aria-hidden="true"></i>
                              </button>
                              <div class="mobile-header__search-body"></div>
                           </form>
                        </div>
                        <div class="mobile-header__indicators">
                           <div class="indicator indicator--mobile-search indicator--mobile d-sm-none">
                              <button class="indicator__button">
                                 <span class="indicator__area">
                                  <i class="fa fa-search" aria-hidden="true"></i>
                                 </span>
                              </button>
                           </div>
                           <div class="indicator indicator--mobile d-sm-flex d-none">
                              <a href="wishlist.html" class="indicator__button">
                                 <span class="indicator__area">
                                   <svg width="20px" height="20px">
                                          <use xlink:href="assets/images/sprite.svg#heart-20"></use>
                                       </svg>
                                    <span class="indicator__value">0</span>
                                 </span>
                              </a>
                           </div>
                           <div class="indicator indicator--mobile">
                              <a href="cart.html" class="indicator__button">
                                 <span class="indicator__area">
                                   <svg width="20px" height="20px">
                                          <use xlink:href="assets/images/sprite.svg#cart-20"></use>
                                       </svg>
                                    <span class="indicator__value">3</span>
                                 </span>
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </header>
         <!-- mobile site__header / end --><!-- desktop site__header -->
         <header class="site__header d-lg-block d-none">
            <div class="site-header">
               <!-- .topbar -->
               <div class="site-header__topbar topbar">
                  <div class="topbar__container container">
                     <div class="topbar__row">
                        <div class="topbar__item topbar__item--link"><a class="topbar-link" href="about-us.html">About Us</a></div>
                        <div class="topbar__item topbar__item--link"><a class="topbar-link" href="contact-us.html">Contacts</a></div>
                        <div class="topbar__item topbar__item--link"><a class="topbar-link" href="#">Store Location</a></div>
                        <div class="topbar__item topbar__item--link"><a class="topbar-link" href="track-order.html">Track Order</a></div>
                        <div class="topbar__item topbar__item--link"><a class="topbar-link" href="blog-classic.html">Blog</a></div>
                        <div class="topbar__spring"></div>
                        <div class="topbar__item">
                           <div class="topbar-dropdown">
                              <button class="topbar-dropdown__btn" type="button">
                                 My Account 
                                 <i class="fa fa-angle-down" aria-hidden="true"></i>
                              </button>
                              <div class="topbar-dropdown__body">
                                 <!-- .menu -->
                                 <ul class="menu menu--layout--topbar">
                                    <li><a href="account.html">Login</a></li>
                                    <li><a href="account.html">Register</a></li>
                                    <li><a href="#">Orders</a></li>
                                    <li><a href="#">Addresses</a></li>
                                 </ul>
                                 <!-- .menu / end -->
                              </div>
                           </div>
                        </div>
                        <div class="topbar__item">
                           <div class="topbar-dropdown">
                              <button class="topbar-dropdown__btn" type="button">
                                 Currency: <span class="topbar__item-value">INR</span> 
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                              </button>
                              <div class="topbar-dropdown__body">
                                 <!-- .menu -->
                                 <ul class="menu menu--layout--topbar">
                                    <li><a href="#">€ Euro</a></li>
                                    <li><a href="#">£ Pound Sterling</a></li>
                                    <li><a href="#">$  US Dollar</a></li>
                                    <li><a href="#">₽ Russian Ruble</a></li>
                                 </ul>
                                 <!-- .menu / end -->
                              </div>
                           </div>
                        </div>
                        <div class="topbar__item">
                           <div class="topbar-dropdown">
                              <button class="topbar-dropdown__btn" type="button">
                                 Language: <span class="topbar__item-value">EN</span> 
                                 <i class="fa fa-angle-down" aria-hidden="true"></i>
                              </button>
                              <div class="topbar-dropdown__body">
                                 <!-- .menu -->
                                 <ul class="menu menu--layout--topbar menu--with-icons">
                                    <li>
                                       <a href="#">
                                          <div class="menu__icon"></div>
                                          English
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                          <div class="menu__icon"></div>
                                          French
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                          <div class="menu__icon"></div>
                                          German
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                          <div class="menu__icon"></div>
                                          Russian
                                       </a>
                                    </li>
                                    <li>
                                       <a href="#">
                                          <div class="menu__icon"></div>
                                          Italian
                                       </a>
                                    </li>
                                 </ul>
                                 <!-- .menu / end -->
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- .topbar / end -->
               <div class="site-header__nav-panel">
                  <div class="nav-panel">
                     <div class="nav-panel__container container">
                        <div class="nav-panel__row">
                           <div class="nav-panel__logo">
                              <a href="index.html">
							  POWERTRAC
							  <!--<img src="assets/images/logo.png" alt="Powertrack">-->
                              </a>
                           </div>
                           <!-- .nav-links -->
                           <div class="nav-panel__nav-links nav-links">
                              <ul class="nav-links__list">
                                 <li class="nav-links__item nav-links__item--with-submenu">
                                    <a href="shop-grid-4-columns-sidebar.html">
                                       <span>
                                          Online Ups 
                                          &nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i>
                                       </span>
                                    </a>
                                    <div class="nav-links__menu">
                                       <!-- .menu -->
                                       <ul class="menu menu--layout--classic">
                                          <li><a href="shop-list.html">Shop List</a></li>
                                          <li><a href="shop-right-sidebar.html">Shop Right Sidebar</a></li>
                                          <li>
                                             <a href="product.html">
                                                Product 
                                                <svg class="menu__arrow" width="6px" height="9px">
                                                   <use xlink:href="assets/images/sprite.svg#arrow-rounded-right-6x9"></use>
                                                </svg>
                                             </a>
                                             <div class="menu__submenu">
                                                <!-- .menu -->
                                                <ul class="menu menu--layout--classic">
                                                   <li><a href="product.html">Product</a></li>
                                                   <li><a href="product-alt.html">Product Alt</a></li>
                                                   <li><a href="product-sidebar.html">Product Sidebar</a></li>
                                                </ul>
                                                <!-- .menu / end -->
                                             </div>
                                          </li>
                                       </ul>
                                       <!-- .menu / end -->
                                    </div>
                                 </li>
                                <li class="nav-links__item nav-links__item--with-submenu">
                                    <a href="shop-grid-4-columns-sidebar.html">
                                       <span>
                                         Luminuous Solar Division
                                          &nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i>
                                       </span>
                                    </a>
                                    <div class="nav-links__menu">
                                       <!-- .menu -->
                                       <ul class="menu menu--layout--classic">
                                          <li><a href="shop-list.html">Shop List</a></li>
                                          <li><a href="shop-right-sidebar.html">Shop Right Sidebar</a></li>
                                          <li>
                                             <a href="product.html">
                                                Product 
                                                <svg class="menu__arrow" width="6px" height="9px">
                                                   <use xlink:href="assets/images/sprite.svg#arrow-rounded-right-6x9"></use>
                                                </svg>
                                             </a>
                                             <div class="menu__submenu">
                                                <!-- .menu -->
                                                <ul class="menu menu--layout--classic">
                                                   <li><a href="product.html">Product</a></li>
                                                   <li><a href="product-alt.html">Product Alt</a></li>
                                                   <li><a href="product-sidebar.html">Product Sidebar</a></li>
                                                </ul>
                                                <!-- .menu / end -->
                                             </div>
                                          </li>
                                       </ul>
                                       <!-- .menu / end -->
                                    </div>
                                 </li>
								  <li class="nav-links__item nav-links__item--with-submenu">
                                    <a href="shop-grid-4-columns-sidebar.html">
                                       <span>
                                         Stabilizers
                                          &nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i>
                                       </span>
                                    </a>
                                    <div class="nav-links__menu">
                                       <!-- .menu -->
                                       <ul class="menu menu--layout--classic">
                                          <li><a href="shop-list.html">Shop List</a></li>
                                          <li><a href="shop-right-sidebar.html">Shop Right Sidebar</a></li>
                                          <li>
                                             <a href="product.html">
                                                Product 
                                                <svg class="menu__arrow" width="6px" height="9px">
                                                   <use xlink:href="assets/images/sprite.svg#arrow-rounded-right-6x9"></use>
                                                </svg>
                                             </a>
                                             <div class="menu__submenu">
                                                <!-- .menu -->
                                                <ul class="menu menu--layout--classic">
                                                   <li><a href="product.html">Product</a></li>
                                                   <li><a href="product-alt.html">Product Alt</a></li>
                                                   <li><a href="product-sidebar.html">Product Sidebar</a></li>
                                                </ul>
                                                <!-- .menu / end -->
                                             </div>
                                          </li>
                                       </ul>
                                       <!-- .menu / end -->
                                    </div>
                                 </li> 
								 <li class="nav-links__item nav-links__item--with-submenu">
                                    <a href="shop-grid-2-columns-sidebar.html">
                                       <span>
                                         Inverters & Ups
                                          
                                       </span>
                                    </a>
                                    <div class="nav-links__menu">
                                       <!-- .menu -->
                                       <ul class="menu menu--layout--classic">
                                          <li><a href="shop-list.html">Shop List</a></li>
                                          <li><a href="shop-right-sidebar.html">Shop Right Sidebar</a></li>
                                          <li>
                                             <a href="product.html">
                                                Product 
                                                <svg class="menu__arrow" width="6px" height="9px">
                                                   <use xlink:href="assets/images/sprite.svg#arrow-rounded-right-6x9"></use>
                                                </svg>
                                             </a>
                                             <div class="menu__submenu">
                                                <!-- .menu -->
                                                <ul class="menu menu--layout--classic">
                                                   <li><a href="product.html">Product</a></li>
                                                   <li><a href="product-alt.html">Product Alt</a></li>
                                                   <li><a href="product-sidebar.html">Product Sidebar</a></li>
                                                </ul>
                                                <!-- .menu / end -->
                                             </div>
                                          </li>
                                       </ul>
                                       <!-- .menu / end -->
                                    </div>
                                 </li>
								<li class="nav-links__item nav-links__item--with-submenu">
                                    <a href="#">
                                       <span>
                                          More
                                        &nbsp; <i class="fa fa-angle-down" aria-hidden="true"></i>
                                       </span>
                                    </a>
                                    <div class="nav-links__megamenu nav-links__megamenu--size--nl">
                                       <div class="megamenu">
                                          <div class="row">
                                             <div class="col-6">
                                                <ul class="megamenu__links megamenu__links--level--0">
                                                   <li class="megamenu__item megamenu__item--with-submenu">
                                                      <a href="#">Genset Division</a>
                                                      <ul class="megamenu__links megamenu__links--level--1">
                                                         <li class="megamenu__item"><a href="#">Product 1</a></li>
                                                         <li class="megamenu__item"><a href="#">Product 2</a></li>
                                                         <li class="megamenu__item"><a href="#">Product 3</a></li>
                                                         <li class="megamenu__item"><a href="#">Product 4</a></li>
                                                      </ul>
                                                   </li>
                                                   <li class="megamenu__item megamenu__item--with-submenu">
                                                      <a href="#">Power Transformers</a>
                                                      <ul class="megamenu__links megamenu__links--level--1">
                                                        <li class="megamenu__item"><a href="#">Product 1</a></li>
                                                         <li class="megamenu__item"><a href="#">Product 2</a></li>
                                                         <li class="megamenu__item"><a href="#">Product 3</a></li>
                                                         <li class="megamenu__item"><a href="#">Product 4</a></li>
                                                      </ul>
                                                   </li>
                                                </ul>
                                             </div>
                                             <div class="col-6">
                                                <ul class="megamenu__links megamenu__links--level--0">
                                                   <li class="megamenu__item megamenu__item--with-submenu">
                                                      <a href="#">Batteries</a>
                                                      <ul class="megamenu__links megamenu__links--level--1">
                                                         <li class="megamenu__item"><a href="#">Product 1</a></li>
                                                         <li class="megamenu__item"><a href="#">Product 2</a></li>
                                                         <li class="megamenu__item"><a href="#">Product 3</a></li>
                                                         <li class="megamenu__item"><a href="#">Product 4</a></li>
                                                      </ul>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                       
                                    </div>
                                 </li>
                           <!-- .nav-links / end -->
                           <div class="nav-panel__indicators">
                              <div class="indicator indicator--trigger--click">
                                 <button type="button" class="indicator__button">
                                    <span class="indicator__area">
                                       <svg class="indicator__icon" width="20px" height="20px">
                                          <use xlink:href="assets/images/sprite.svg#search-20"></use>
                                       </svg>
                                       <svg class="indicator__icon indicator__icon--open" width="20px" height="20px">
                                          <use xlink:href="assets/images/sprite.svg#cross-20"></use>
                                       </svg>
                                    </span>
                                 </button>
                                 <div class="indicator__dropdown">
                                    <div class="drop-search">
                                       <form action="#" class="drop-search__form">
                                          <input class="drop-search__input" name="search" placeholder="Search over 10,000 products" aria-label="Site search" type="text" autocomplete="off"> 
                                          <button class="drop-search__button drop-search__button--submit" type="submit">
                                             <svg width="20px" height="20px">
                                                <use xlink:href="assets/images/sprite.svg#search-20"></use>
                                             </svg>
                                          </button>
                                       </form>
                                    </div>
                                 </div>
                              </div>
                              <div class="indicator">
                                 <a href="wishlist.html" class="indicator__button">
                                    <span class="indicator__area">
                                       <svg width="20px" height="20px">
                                          <use xlink:href="assets/images/sprite.svg#heart-20"></use>
                                       </svg>
                                       <span class="indicator__value">0</span>
                                    </span>
                                 </a>
                              </div>
                              <div class="indicator indicator--trigger--click">
                                 <a href="cart.html" class="indicator__button">
                                    <span class="indicator__area">
                                       <svg width="20px" height="20px">
                                          <use xlink:href="assets/images/sprite.svg#cart-20"></use>
                                       </svg>
                                       <span class="indicator__value">3</span>
                                    </span>
                                 </a>
                                 <div class="indicator__dropdown">
                                    <!-- .dropcart -->
                                    <div class="dropcart">
                                       <div class="dropcart__products-list">
                                          <div class="dropcart__product">
                                             <div class="dropcart__product-image"><a href="product.html"><img src="assets/images/products/product-1.jpg" alt=""></a></div>
                                             <div class="dropcart__product-info">
                                                <div class="dropcart__product-name"><a href="product.html">Electric Planer Brandix KL370090G 300 Watts</a></div>
                                                <ul class="dropcart__product-options">
                                                   <li>Color: Yellow</li>
                                                   <li>Material: Aluminium</li>
                                                </ul>
                                                <div class="dropcart__product-meta"><span class="dropcart__product-quantity">2</span> x <span class="dropcart__product-price">&#8377; 699.00</span></div>
                                             </div>
                                             <button type="button" class="dropcart__product-remove btn btn-light btn-sm btn-svg-icon">
                                                <svg width="10px" height="10px">
                                                   <use xlink:href="assets/images/sprite.svg#cross-10"></use>
                                                </svg>
                                             </button>
                                          </div>
                                          <div class="dropcart__product">
                                             <div class="dropcart__product-image"><a href="product.html"><img src="assets/images/products/product-2.jpg" alt=""></a></div>
                                             <div class="dropcart__product-info">
                                                <div class="dropcart__product-name"><a href="product.html">Undefined Tool IRadix DPS3000SY 2700 watts</a></div>
                                                <div class="dropcart__product-meta"><span class="dropcart__product-quantity">1</span> x <span class="dropcart__product-price">&#8377; 849.00</span></div>
                                             </div>
                                             <button type="button" class="dropcart__product-remove btn btn-light btn-sm btn-svg-icon">
                                                <svg width="10px" height="10px">
                                                   <use xlink:href="assets/images/sprite.svg#cross-10"></use>
                                                </svg>
                                             </button>
                                          </div>
                                          <div class="dropcart__product">
                                             <div class="dropcart__product-image"><a href="product.html"><img src="assets/images/products/product-5.jpg" alt=""></a></div>
                                             <div class="dropcart__product-info">
                                                <div class="dropcart__product-name"><a href="product.html">Brandix Router Power Tool 2017ERXPK</a></div>
                                                <ul class="dropcart__product-options">
                                                   <li>Color: True Red</li>
                                                </ul>
                                                <div class="dropcart__product-meta"><span class="dropcart__product-quantity">3</span> x <span class="dropcart__product-price">&#8377; 1,210.00</span></div>
                                             </div>
                                             <button type="button" class="dropcart__product-remove btn btn-light btn-sm btn-svg-icon">
                                                <svg width="10px" height="10px">
                                                   <use xlink:href="assets/images/sprite.svg#cross-10"></use>
                                                </svg>
                                             </button>
                                          </div>
                                       </div>
                                       <div class="dropcart__totals">
                                          <table>
                                             <tr>
                                                <th>Subtotal</th>
                                                <td>&#8377; 5,877.00</td>
                                             </tr>
                                             <tr>
                                                <th>Shipping</th>
                                                <td>&#8377; 25.00</td>
                                             </tr>
                                             <tr>
                                                <th>Tax</th>
                                                <td>&#8377; 0.00</td>
                                             </tr>
                                             <tr>
                                                <th>Total</th>
                                                <td>&#8377; 5,902.00</td>
                                             </tr>
                                          </table>
                                       </div>
                                       <div class="dropcart__buttons"><a class="btn btn-secondary" href="cart.html">View Cart</a> <a class="btn btn-primary" href="checkout.html">Checkout</a></div>
                                    </div>
                                    <!-- .dropcart / end -->
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </header>



<?php } ?>