<?php function site_slider() { ?>

            <div class="block-slideshow block-slideshow--layout--full block">
               <div class="container">
                  <div class="row">
                     <div class="col-12">
                        <div class="block-slideshow__body">
                           <div class="owl-carousel">
                              <a class="block-slideshow__slide" href="#">
                                 <div class="block-slideshow__slide-image block-slideshow__slide-image--desktop" style="background-image: url('assets/images/slides/slide-3-full.jpg')"></div>
                                 <div class="block-slideshow__slide-image block-slideshow__slide-image--mobile" style="background-image: url('assets/images/slides/slide-3-mobile.jpg')"></div>
                                 <div class="block-slideshow__slide-content">
                                    <div class="block-slideshow__slide-title">Power Your<br>Home with Us</div>
                                    <div class="block-slideshow__slide-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Etiam pharetra laoreet dui quis molestie.</div>
                                    <div class="block-slideshow__slide-button"><span class="btn btn-primary btn-lg">Shop Now</span></div>
                                 </div>
                              </a>
                              <a class="block-slideshow__slide" href="#">
                                 <div class="block-slideshow__slide-image block-slideshow__slide-image--desktop" style="background-image: url('assets/images/slides/slide-3-full.jpg')"></div>
                                 <div class="block-slideshow__slide-image block-slideshow__slide-image--mobile" style="background-image: url('assets/images/slides/slide-3-mobile.jpg')"></div>
                                 <div class="block-slideshow__slide-content">
                                    <div class="block-slideshow__slide-title">Empower Your<br>Business</div>
                                    <div class="block-slideshow__slide-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Etiam pharetra laoreet dui quis molestie.</div>
                                    <div class="block-slideshow__slide-button"><span class="btn btn-primary btn-lg">Shop Now</span></div>
                                 </div>
                              </a>
                              <a class="block-slideshow__slide" href="#">
                                 <div class="block-slideshow__slide-image block-slideshow__slide-image--desktop" style="background-image: url('assets/images/slides/slide-3-full.jpg')"></div>
                                 <div class="block-slideshow__slide-image block-slideshow__slide-image--mobile" style="background-image: url('assets/images/slides/slide-3-mobile.jpg')"></div>
                                 <div class="block-slideshow__slide-content">
                                    <div class="block-slideshow__slide-title">Zoom<br>Ahead</div>
                                    <div class="block-slideshow__slide-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Etiam pharetra laoreet dui quis molestie.</div>
                                    <div class="block-slideshow__slide-button"><span class="btn btn-primary btn-lg">Shop Now</span></div>
                                 </div>
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
			
<?php } ?>