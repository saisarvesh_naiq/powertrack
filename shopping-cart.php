<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/site_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
<style>
.container-outer img{
margin-top:-55px;
height:100px;
}
</style>
    <div class="page-wrapper">
		<!-- Header Here -->
		<?php bottom_menu(); ?>
		<?php site_top_menu(); ?>
		<?php slide_slider(); ?>                
<!--End mainmenu area-->    

<!--Start breadcrumb area-->     
<section class="breadcrumb-area" style="background-image: url(css/images/resources/breadcrumb-bg.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title float-left">
                       <h1>Shopping Cart</h1>
                    </div>
                    <div class="breadcrumb-menu float-right">
                        <ul class="clearfix">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Shopping Cart</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>
<!--End breadcrumb area-->

<!--Start cart area-->
<section class="cart-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="table-outer">
                    <table class="cart-table">
                        <thead class="cart-header">
                            <tr>
                                <th class="prod-column">Products</th>
                                <th>&nbsp;</th>
                                <th>Quantity</th>
                                <th class="availability">Availability</th>
                                <th class="price">Price</th>
                                <th>Total</th>
                                <th>Remove</th>
                            </tr>    
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2" class="prod-column">
                                    <div class="column-box">
                                        <div class="prod-thumb">
                                            <a href="#"><img src="css/images/shop/product-thumb-1.jpg" alt=""></a>
                                        </div>
                                        <div class="title">
                                            <h3 class="prod-title">Product 1</h3>
                                        </div>    
                                    </div>
                                </td>
                                <td class="qty">
                                    <input class="quantity-spinner" type="text" value="1" name="quantity">
                                </td>
                                <td class="unit-price">
                                    <div class="available-info">
                                        <span class="icon fa fa-check thm-bg-clr"></span>Item(s)<br>Avilable Now
                                    </div>
                                </td>
                                <td class="price">&#8377; 2900.00</td>
                                <td class="sub-total">&#8377; 2900.00</td>
                                <td>
                                    <div class="remove">
                                        <div class="checkbox">
                                            <label>
                                                <input name="remove" type="checkbox">
                                                <span>Remove</span>
                                            </label>
                                        </div>   
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="prod-column">
                                    <div class="column-box">
                                        <div class="prod-thumb">
                                            <a href="#"><img src="css/images/shop/product-thumb-2.jpg" alt=""></a>
                                        </div>
                                        <div class="title">
                                            <h3 class="prod-title">Product 2</h3>
                                        </div>    
                                    </div>
                                </td>
                                <td class="qty">
                                    <input class="quantity-spinner" type="text" value="2" name="quantity">
                                </td>
                                <td class="unit-price">
                                    <div class="available-info">
                                        <span class="icon fa fa-check thm-bg-clr"></span>Item(s)<br>Avilable Now
                                    </div>
                                </td>
                                <td class="price">&#8377; 2900.00</td>
                                <td class="sub-total">&#8377; 2900.00</td>
                                <td>
                                    <div class="remove">
                                        <div class="checkbox">
                                            <label>
                                                <input name="remove" type="checkbox">
                                                <span>Remove</span>
                                            </label>
                                        </div>   
                                    </div>
                                </td>
                            </tr>
                        </tbody>    
                    </table>
                </div>
            </div>     
        </div>
        <div class="row cart-middle">
            <div class="col-xl-6 col-lg-9 col-md-8 col-sm-12">
                <div class="apply-coupon">
                    <input type="text" name="coupon-code" value="" placeholder="Enter Coupon Code...">
                    <div class="apply-coupon-button">
                        <button class="btn-three" type="button">Apply Coupon<span class="icon-null"></span></button>
                    </div>        
                </div>
            </div>
            <div class="col-xl-6 col-lg-3 col-md-4 col-sm-12">
                <div class="update-cart pull-right">
                    <button class="btn-three" type="button">Update Cart<span class="icon-null"></span></button>
                </div>
            </div>
        </div>
        <div class="row cart-bottom">
            <div class="col-xl-6 col-lg-5 col-md-12 col-sm-12">
                <div class="calculate-shipping">
                    <div class="row">
                        <div class="col-lg-6">
                            <input class="mar-bottom" type="text" placeholder="State / Country" required>
                        </div>
                        <div class="col-lg-6">
                            <input class="zip-code" type="text" placeholder="Zip Code" required>
                        </div>
                    </div>
				    <button class="btn-three" type="submit">update Totals<span class="icon-null"></span></button> 
                </div>
            </div>
            <!--Start cart total -->
            <div class="col-xl-6 col-lg-7 col-md-12 col-sm-12">
                <div class="cart-total">
                    <div class="shop-title-box">
                        <h3>Cart Totals</h3>
                    </div>
                    <ul class="cart-total-table">
                        <li class="clearfix">
                            <span class="col col-title">Cart Subtotal</span>
                            <span class="col">&#8377; 5000.00</span>    
                        </li>
                        <li class="clearfix">
                            <span class="col col-title">Shipping and Handling</span>
                            <span class="col">&#8377; 400.00</span>    
                        </li>
                        <li class="clearfix">
                            <span class="col col-title">Order Total</span>
                            <span class="col">&#8377; 5400.00</span>    
                        </li>      
                    </ul>
                    <button class="btn-three checkout-btn">Proceed to Checkout<span class="icon-null"></span></button> 
                </div>    
            </div>
            <!--End cart total -->
        </div>
    </div>
</section>         
<!--End cart area-->

<!--Start footer area-->  
<?php site_footer(); ?>
