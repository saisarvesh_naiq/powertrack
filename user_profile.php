<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/online_ups_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
  <style>
    .container-outer img{
      margin-top:-55px;
      height:100px;
    }
  </style>
  <div class="page-wrapper">
    <!-- Header Here -->
    <?php bottom_menu(); ?>
    <?php site_top_menu(); ?>
    <?php slide_slider(); ?>


        <?php
        require 'connect.php';
        $sql= $conn->prepare("SELECT * FROM user_sub_details WHERE user_id='".$_SESSION["user_id"]."' AND status='1'");
        $sql->execute();
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        if($sql->rowCount()>0){
          foreach (($sql->fetchAll()) as $key => $row) {


            
          echo '
        



    <section>

      <div class="user_profile">
         <div class="container" style="padding-left: 20px;padding-right: 20px;box-shadow: 0px 0px 39px -8px rgba(165,162,162,1);border-radius: 8px;">
            
            <div class="row">
              <div class="col-md-6 general_information">
                <h3>General Information</h3>
                <div class="row">
                  <div class="col-md-12">
                    <img src="'.$row['profile_pic'].'" class="profile_img" id="img1"> 
                    <input type="hidden" name="image1" id="image1" disabled>
                    <button type="button" class="btn btn-primary form-control" style="margin-top:20px;"><input type="file" id="blog_pic" name="file" /></button> 
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <input type="text" value="'.$row['firstname'].'" name="first_name" class="form-control profile_input" placeholder="FIRST NAME" id="firstname">
                  </div>
                  <div class="col-md-6">
                     <input type="text" value="'.$row['lastname'].'" name="last_name" class="form-control profile_input" placeholder="LAST NAME" id="lastname">
                  </div>
                </div>

                 <input type="text" value="'.$row['company'].'" name="company" class="form-control profile_input" placeholder="Company" id="company">
                
              </div>

              <div class="col-md-6 contact_details">
                <h3>Shipping Details</h3>
                 <textarea placeholder="Address" class="form-control address_textarea" id="address">'.$row['address'].'</textarea>

                 <div class="row">
                   <div class="col-md-6">
                     <input type="number" value="'.$row['pincode'].'" name="zip_code" class="form-control profile_input2" placeholder="Pin code" id="pincode">
                   </div>
                   <div class="col-md-6">
                     <input type="text" value="'.$row['town'].'" name="Town" class="form-control profile_input2" placeholder="Town" id="town">
                   </div>
                 </div>

                 <select name="Country" class="form-control country_select" id="country">
                  <option value="India" selected>India</option>
                  <option value="USA">USA</option>
                  <option value="Gernamy">Germany</option>
                  <option value="Australia">Australia</option>
                </select>


                    <input type="number" value="'.$row['contact_number'].'" name="Contact Number" class="form-control profile_input2" placeholder="Contact Number" id="contact_number">

                <input type="text" name="email" value="'.$row['email_id'].'" class="form-control profile_input2" placeholder="Your Email" id="email_id">

                <div class="row">
                  <div class="col-md-6">
                    <button class="btn btn-primary profile_btn" id="save_changes">UPDATE</button>
                  </div>
                  <div class="col-md-6 text-right">
                      <a href="index.php"><button class="btn btn-primary profile_btn">Continue Shopping ></button></a>
                  </div>
                </div>

                

              </div>
            </div>

         </div>
      </div>

    </section>';

  }
}
?>

    <script>
      $("#save_changes").click(function(){

    var profile_pic=$("#image1").val();
    var firstname=$("#firstname").val();
    var lastname=$("#lastname").val();
    var company=$("#company").val();
    var address=$("#address").val();
    var pincode=$("#pincode").val();
    var town=$("#town").val();
    var country=$("#country").val();
    var contact_number=$("#contact_number").val();
    var email_id=$("#email_id").val();
    
      

    var datastr='profile_pic='+profile_pic+'&firstname='+firstname+'&lastname='+lastname+'&company='+company+'&address='+address+'&pincode='+pincode+'&town='+town+'&country='+country+'&contact_number='+contact_number+'&email_id='+email_id;

    $.ajax({


    type: "POST",
    url: "edit_profile_GET.php",
    data: datastr,
    cache: false,
    success: function(res){
    if(res==1)
    {
      swal("Profile Edited Successfully");

    }
    else{


    swal("Error Occured While Updating Profile...");


    }

    }
    }); 
    

    });
    
  
  </script>




    <script>
    $('#blog_pic').change(function(){

      var fd = new FormData();
      var files = $('#blog_pic')[0].files[0];
      fd.append('file',files);

      $.ajax({
        url: 'upload_blog.php',
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        success: function(response){
          if(response != 0){
            $("#img1").attr("src",response); 
            $('#image1').val(response);
                    $(".preview img").show(); // Display image element
                }else{
                  alert('file not uploaded');
                }
            },
        });
    });

  </script>
































    <?php site_footer(); ?>
