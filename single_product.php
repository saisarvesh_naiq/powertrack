<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/online_ups_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@500&display=swap" rel="stylesheet">
<body>
  <style>
    .container-outer img{
      margin-top:-55px;
      height:100px;
    }
  </style>
  <div class="page-wrapper">
    <!-- Header Here -->
    <?php bottom_menu(); ?>
    <?php site_top_menu(); ?>
    <?php slide_slider(); ?>






    <section>

      <div class="user_profile">
       <div class="container" style="padding-left: 20px;padding-right: 20px;border:1px solid #257870;border-radius: 8px;">

          <?php
     require 'connect.php';
      $prod_id=$_GET['prod_id'];
      $sql= $conn->prepare("SELECT * FROM products_db WHERE prod_id='".$prod_id."'");
      $sql->execute();
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      if($sql->rowCount()>0){
        foreach (($sql->fetchAll()) as $key => $row) {

          $sql2= $conn->prepare("SELECT * FROM pro_main_category WHERE cat_id='".$row['main_category']."'");
          $sql2->execute();
          $sql2->setFetchMode(PDO::FETCH_ASSOC);
        if($sql2->rowCount()>0){
        foreach (($sql2->fetchAll()) as $key => $row2) {

    echo '

        <div class="row">
          <div class="col-md-6 text-center product_display"  style="background-color: #e2ebf0 !important;">

           
           <input type="number" name="" id="prod_id" style="display:none;" value="'.$row['prod_id'].'">
           <img src="admin/products/'.$row['pro_image_1'].'" class="img-fluid single_product_img">


         </div>
         <div class="col-md-6 single_product_details">

           <h3>'.$row['prod_name'].'</h3>
           <p>AVAILABILITY: <span style="color: red;margin-top: 16px;"><u>'.$row['pro_qty'].' LEFT IN STOCK</u></span></p>
           <hr/>

           <p><strong>Its not currently available in all locations. Check availability.</strong></p>

           <p><strong>Category : '.$row2['cat_name'].'</strong></p>

           <p>'.$row['prod_details'].'</p>


           <input type="number" class="form-control" id="prod_qty" placeholder="Enter product quantity">


          <div class="row" style="margin-top: 60px;">
                  <div class="col-md-6 single_product_price">
                    <h2>&#8377; '.$row['prod_price'].'</h2>
                  </div>
                  <div class="col-md-6 text-right">
                    <button class="btn btn-primary add_cart_btn" id="add_to_cart">ADD TO CART</button>
                  </div>
                </div>
                <hr/>
        </div>
      </div>';

    }

  }
}
}
  ?>


    </div>
  </div>

</section>


<style>
  .view_recent_div{
    max-width: 350px;
    max-height: 350px !important;
    height: 350px !important;
    padding: 20px 20px 20px 20px;
    border:1px solid #c3cfe2;
    border-radius: 8px;
  }

  .view_recent_div:hover{

    box-shadow: 0px 0px 18px -1px rgba(195,207,226,1);

  }

  .view_recent_div img{

    max-height: 200px;
    width: auto;
    object-fit: contain;
  }

  .buy_btn{
    
    background-color: #051f2e;
    color: #ffffff;
    max-width: 130px;
    width: 140px;
    height: 40px;

    margin-top: 30px;

  }

</style>



<section>

 <div class="container">
   <div class="row">
     <div class="col-md-12 text-center">
      <h3 style="font-weight: 700;font-size: 40px !important;">View Similar Products</h3>
      <hr/>
      <div class="owl-carousel owl-theme" style="margin-top: 40px;margin-bottom: 40px;">


        <?php
        require 'connect.php';
        $prod_id=$_GET['prod_id'];
        $sql= $conn->prepare("SELECT * FROM products_db WHERE prod_id='".$prod_id."' AND status='1'");
        $sql->execute();
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        if($sql->rowCount()>0){
          foreach (($sql->fetchAll()) as $key => $row) {

            $sql2= $conn->prepare("SELECT * FROM products_db WHERE main_category='".$row['main_category']."' AND status='1' LIMIT 8");
            $sql2->execute();
            $sql2->setFetchMode(PDO::FETCH_ASSOC);
            if($sql2->rowCount()>0){
              foreach (($sql2->fetchAll()) as $key => $row2) {
               
               echo '             <div class="item text-center view_recent_div">
                  <img src="admin/products/'.$row2['pro_image_1'].'">
                  <h5>'.$row2['prod_name'].'</h5>
                  <h6>&#8377; '.$row2['prod_price'].'</h6>  

                   <a href="single_product.php?prod_id='.$row2['prod_id'].'"><button class="buy_btn">BUY NOW</button></a>
                </div>
           ';
               
              }
            }
          }
        }
        ?>








              </div>

       </div>
     </div>
   </div>

</section>




<script>
  

   $("#add_to_cart").click(function(){

  var prod_id=$("#prod_id").val();
  var prod_qty=$("#prod_qty").val();

  var datastr='prod_id='+prod_id+'&prod_qty='+prod_qty;

  if (prod_qty=="0") {
   swal("Product Quantity cannot be zero");

 }

   else if (prod_qty=="") {
   swal("Please enter product quantity!");

 }



else{

 $.ajax({


  type: "POST",
  url: "shop_single_GET.php",
  data: datastr,
  cache: false,
  success: function(res){
    if(res==1)
    {
      swal("Product Has Been added to your cart!", "", "success");
      // swal("Product Added to cart Successfully!");
                setTimeout(function(){
           window.location.href ="cart.php";
         }, 3000);

    }
    else if(res==2)
    {
       swal("", "Sorry! This product is currently not in stock!", "info");

    }
      else if(res==3)
    {
       swal("Product Quantity exceeds our stock limit!", "Please reduce product quantity.", "warning");

    }
    else{
     swal("", "Please login to add product into the cart!", "error");

                setTimeout(function(){
           window.location.href ="login.php";
         }, 2000);


   }

 }
}); 
}

});


</script>



<script>
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
})
</script>





















<?php site_footer(); ?>
