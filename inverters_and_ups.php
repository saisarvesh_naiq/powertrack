<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/inverters_and_ups_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
  <style>
    .container-outer img{
      margin-top:-55px;
      height:100px;
    }

    .how_to_div{
      max-width: 230px;
      margin: 0 auto;
      padding: 30px 20px 30px 20px;
      border-radius: 8px;
      border:1px solid #c3cfe2;
    }

    .how_to_div:hover{
      border:2px solid #f96a01;
    }

  </style>
  <div class="page-wrapper">
    <!-- Header Here -->
    <?php bottom_menu(); ?>
    <?php site_top_menu(); ?>
    <?php slide_slider(); ?>


    <div style="margin-top: 170px;">
    	<?php inverters_and_ups_slider(); ?>
    </div>


    <div class="container">


      <div class="product_intro">
       <div class="row">
        <div class="col-md-12 text-center" >
          <h1>Inverters & UPS</h1>
          <p>An inverter is an electrical device that converts direct current (DC) to alternating current (AC); the converted AC can be at any required voltage and frequency with the use of appropriate transformers, switching, and control circuits. The electrical inverter is a high-power electronic oscillator.
          </p>
          <p>
           Luminous offers a world class range of inverters and long backup UPS which have seen the light of the day only after going through extensive research and development worth million of dollars. Our undiluted focus on the dynamic power requirements of the customer has driven us to continuously innovate and come out with technologies which have become a benchmark for the whole industry and resulted in products offering great value to the consumer, no doubt we are the leaders in the domestic market and strive to be a prominent international player in the segment.
         </p>
         <p>Inverters / Long Back up UPS are used in a wide range of applications, from household appliances to large electric utility high-voltage direct current applications that transport bulk power. Inverters / Long Back up UPS are commonly used to supply AC power from DC sources such as batteries.

         </p>
       </div>
     </div>
   </div>


   <div class="how_to" style="padding:20px 10px 80px 10px; ">
     <div class="row">
       <div class="col-md-12 text-center">
         <h1>How to Buy Home UPS & Inverter?</h1>
         <p style="margin-top: 30px;">Need help in choosing the perfect Inverter & UPS for your home? Look no further than Luminous, India's no.1 Home Inverters & UPS company!<br>This section will guide you on everything to do with Inverters & UPS.</p>
       </div>
     </div>

     <div class="row">
      <div class="col-md-3 text-center how_to_div" style="margin-top: 10px;">
        <a href="https://www.youtube.com/watch?v=AT64BSo5y9M&feature=emb_logo">
          <img src="assets/images/icon/inverter_video_hover.png">
        </a>
        <a href="https://www.youtube.com/watch?v=AT64BSo5y9M&feature=emb_logo" style="color: inherit;">
          <p style="margin: 0 !important;padding:0 !important;font-weight: 700;margin-top: 10px !important;">Which Home UPS &<br>
          Inverter to choose?</p>
        </a>  
      </div>
      <div class="col-md-3 text-center how_to_div" style="margin-top: 10px;">
       <a href="load_calculator.php">
        <img src="assets/images/icon/load_calc.png">
      </a>
      <a href="load_calculator.php" style="color: inherit;">
       <p style="margin: 0 !important;padding:0 !important;font-weight: 700;margin-top: 10px !important;">Load<br>Calculator</p>
     </a>
   </div>
   <div class="col-md-3 text-center how_to_div" style="margin-top: 10px;">
    <a href="#">
      <img src="assets/images/icon/catalogue.png">
    </a>
    <a href="" style="color: inherit;">
      <p style="margin: 0 !important;padding:0 !important;font-weight: 700;margin-top: 10px !important;">Download<br>Catalogue</p>
    </a>        
  </div>
  <div class="col-md-3 text-center how_to_div" style="margin-top: 10px;">
    <a href="#">
      <img src="assets/images/icon/buying_guide.png">
    </a>
    <a href="" style="color: inherit;">
      <p style="margin: 0 !important;padding:0 !important;font-weight: 700;margin-top: 10px !important;">Buying<br>Guide</p>
    </a>
  </div>
</div>
</div>


<!--        <div class="product_filter">
           <div class="row">
               <div class="col-md-12 text-center">
                   <div class="row">
                       <div class="col-md-4">
                           <div class="row">
                            <div class="col-md-1"></div>
                               <div class="col-md-3">
                                   <img src="assets/images/products/load_requirenment.png">
                               </div>
                               <div class="col-md-8">
                                   <p>Select By</p>
                                   <h1>Load Requirenment</h1>
                               </div>
                           </div>

                           <div class="row">
                             <div class="col-md-1"></div>
                               <div class="col-md-3"></div>
                               <div class="col-md-8">
                                    <p style="margin-top: 20px;"><a href=""> 2000 VA to 4000 VA</a></p>
                           <p><a href=""> Above 4000 VA</a></p>


                               </div>
                           </div>                    
                       </div>

                    <div class="col-md-4">
                           <div class="row">
                            <div class="col-md-1"></div>
                               <div class="col-md-3">
                                   <img src="assets/images/products/technology.png">
                               </div>
                               <div class="col-md-8">
                                   <p>Select By</p>
                                   <h1>Technology</h1>
                               </div>
                           </div>

                           <div class="row">
                               <div class="col-md-4"></div>
                               <div class="col-md-8">
                                    <p style="margin-top: 20px;"><a href=""> Sine Wave</a></p>
                           <p><a href=""> Square Wave</a></p>
                               </div>
                           </div>                    
                       </div>

                    <div class="col-md-4">
                           <div class="row">
                            <div class="col-md-1"></div>
                               <div class="col-md-3">
                                   <img src="assets/images/products/technology.png">
                               </div>
                               <div class="col-md-8">
                                   <p>Select By</p>
                                   <h1>Series</h1>
                               </div>
                           </div>

                           <div class="row">
                               <div class="col-md-4"></div>
                               <div class="col-md-8">
                                    <p style="margin-top: 20px;"><a href=""> Pro UPS</a></p>
                           <p><a href=""> Power X</a></p>
                           <p><a href="">  Cruze</a></p>
                           <p><a href=""> Icruze</a></p>

                               </div>
                           </div>                    
                       </div>
               



                   </div>
                   <button class="view_all_btn">View all products</button>
               </div>
           </div>
         </div> -->


         <?php
         require 'connect.php';
         $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='4' AND status='1'");
         $sql->execute();
         $sql->setFetchMode(PDO::FETCH_ASSOC);
         if($sql->rowCount()>0){
          foreach (($sql->fetchAll()) as $key => $row) {

           echo '
           <div class="product_details">
           <div class="container">

           <div class="row">
           <div class="col-md-6 text-center" style="padding-left:50px;padding-right:50px;">
           <img src="admin/products/'.$row['pro_image_1'].'">
           </div>
           <div class="col-md-6 product_details_content">
           <h1>'.$row['prod_name'].'</h1>
           <p>'.$row['prod_details'].'</p>

           <div class="row" style="margin-top: 60px">
           <div class="col-md-2"></div>
           <div class="col-md-4 text-center">
           <a href="single_product.php?prod_id='.$row['prod_id'].'"><button class="brochure_btn" >View Product</button></a>
           </div>
           <div class="col-md-4 text-center">
           <button class="quote_button">Get A Quote</button>
           </div>
           <div class="col-md-2"></div>
           </div>

           </div>
           </div>


           <div class="product_features">
           <div class="row">
           <div class="col-md-2"></div>

           <div class="col-md-8">
           <div class="row">
           <div class="col-md-3 text-center feature_divs">
           <img src="assets/images/products/highly_reliable.png">
           </div>
           <div class="col-md-3 text-center feature_divs">
           <img src="assets/images/products/selectable_charging.png">
           </div>
           <div class="col-md-3 text-center feature_divs">
           <img src="assets/images/products/cold_start.png">
           </div>
           <div class="col-md-3 text-center feature_divs">
           <img src="assets/images/products/AVR.png">
           </div>
           </div>
           </div>

           <div class="col-md-2"></div>
           </div>
           </div>

           </div>
           </div>
           ';

         }
       }
       ?>


     </div>




     <?php site_footer(); ?>
