<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/site_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
<style>
.container-outer img{
margin-top:-55px;
height:100px;
}
</style>
    <div class="page-wrapper">
		<!-- Header Here -->
		<?php bottom_menu(); ?>
		<?php site_top_menu(); ?>
		<?php slide_slider(); ?>                 
<!--End mainmenu area-->    

<!--Start breadcrumb area-->     
<section class="breadcrumb-area" style="background-image: url(css/images/resources/breadcrumb-bg.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title float-left">
                       <h1>My Account</h1>
                    </div>
                    <div class="breadcrumb-menu float-right">
                        <ul class="clearfix">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Account</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>
<!--End breadcrumb area-->

<!--Start login register area-->
<section class="login-register-area">
    <div class="container">
        <div class="row">
		<div class="col-xl-6 col-lg-12 col-md-12 col-sm-12">
           <div class="testimonial-carousel owl-carousel owl-theme">
                        <!--Start Single Testimonial Item-->
						
                        <div class="col-md-12">
                            <div class="">
                                <img src="a1.jpg" alt="Awesome Image">
                            </div>
						</div>
						<div class="col-md-12">
                            <div class="">
                                <img src="a3.jpg" alt="Awesome Image">
                            </div>
						</div>
						<div class="col-md-12">
                            <div class="">
                                <img src="a2.jpg" alt="Awesome Image">
                            </div>
						</div>
			</div></div>
            <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12">
                <div class="form">
                    <div class="shop-title-box">
                        <h3>Login Now</h3>
                    </div>
                    <div class="row">
                        <form action="#">
                            <div class="col-xl-12">
                                <div class="input-field">
                                    <input type="text" name="email" placeholder="Enter Mail id *">
                                    <div class="icon-holder">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                    </div>
                                </div>    
                            </div>
                            <div class="col-xl-12">
                                <div class="input-field">
                                    <input type="text" name="password" placeholder="Enter Password">
                                    <div class="icon-holder">
                                        <i class="fa fa-unlock-alt" aria-hidden="true"></i>
                                    </div>
                                </div>    
                            </div>
                            <div class="col-xl-12">
                                <div class="row">
                                    <div class="col-xl-6 col-lg-6 col-sm-12">
                                        <button class="btn-three" type="submit">Login Now<span class="icon-null"></span></button>
                                        <div class="remember-text">
                                            <div class="checkbox">
                                                <label>
                                                    <input name="remember" type="checkbox">
                                                    <span>Remember Me</span>
                                                </label>
                                            </div>  
							            </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-sm-12">
                                        <ul class="social-icon">
                                            <li class="login-with">Or login with</li>
                                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter twitter" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google-plus gplus" aria-hidden="true"></i></a></li>
                                        </ul>
                                        <a class="forgot-password" href="#">Forgot Password?</a>
                                    </div>
                                </div>   
                            </div> 
                        </form>    
                    </div>
                </div>    
            </div> 
        </div>
    </div>
</section>      
<!--End login register area-->
<!--Start footer area-->  
<?php site_footer(); ?>