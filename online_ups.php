<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/online_ups_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
<style>
.container-outer img{
margin-top:-55px;
height:100px;
}
</style>
    <div class="page-wrapper">
		<!-- Header Here -->
		<?php bottom_menu(); ?>
		<?php site_top_menu(); ?>
		<?php slide_slider(); ?>


    <div style="margin-top: 170px;">
    	<?php online_ups_slider(); ?>
    </div>
				

	<div class="container">
	   

	   <div class="product_intro">
	   	<div class="row">
	   		<div class="col-md-12 text-center">
	   				   	<h1>Uninterruptible Power Supplies (UPS)</h1>
	   				   	<p>Luminous UPS range is loaded with state of art features that deliver continuous power supply and protect computer and peripherals from power surges, drop in line voltage, brownouts, blackouts and other critical hazards. They also protect mission critical applications and equipment from downtime, data loss, corruption and power outages that hamper productivity and cripple equipment. Our UPS range are equipped with Automatic Voltage regulator, Smart Charge and Intelligent Continuous Charging Technology ensuring longer battery life & faster charging.</p>
	   		</div>
	   	</div>
	   </div>

   
        <?php
     require 'connect.php';
      $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='1' AND status='1'");
      $sql->execute();
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      if($sql->rowCount()>0){
        foreach (($sql->fetchAll()) as $key => $row) {

            echo ' <div class="product_details">
            <div class="container">
                
             <div class="row">
                <div class="col-md-6 text-center" style="padding-left:50px;padding-right:50px;">
                    <img src="admin/products/'.$row['pro_image_1'].'">
                </div>
                <div class="col-md-6 product_details_content">
                    <h1>'.$row['prod_name'].'</h1>
                    <p>'.$row['prod_details'].'</p>

                    <div class="row" style="margin-top: 60px">
                        <div class="col-md-2"></div>
                        <div class="col-md-4 text-center">
                        <a href="single_product.php?prod_id='.$row['prod_id'].'"><button class="brochure_btn" >View Product</button></a>
                        </div>
                        <div class="col-md-4 text-center">
                            <button class="quote_button">Get A Quote</button>
                        </div>
                        <div class="col-md-2"></div>
                    </div>

                </div>
             </div>

            
            <div class="product_features">
             <div class="row">
                <div class="col-md-2"></div>
                 
                 <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-3 text-center feature_divs">
                            <img src="assets/images/products/highly_reliable.png">
                        </div>
                        <div class="col-md-3 text-center feature_divs">
                            <img src="assets/images/products/selectable_charging.png">
                        </div>
                        <div class="col-md-3 text-center feature_divs">
                            <img src="assets/images/products/cold_start.png">
                        </div>
                        <div class="col-md-3 text-center feature_divs">
                            <img src="assets/images/products/AVR.png">
                        </div>
                    </div>
                 </div>

                <div class="col-md-2"></div>
             </div>
             </div>

            </div>
        </div>
';
        }
    }
            ?>



       


       <!--  <div class="product_details">
        	<div class="container">
        		
             <div class="row">
             	<div class="col-md-6">
             		<img src="assets/images/products/liebert-s600.jpg">
             	</div>
             	<div class="col-md-6 product_details_content">
             		<h1>Liebert S600</h1>
             		<p>The Liebert S600 is a fully-digital, highly reliable, double conversion UPS solution that delivers clean and consistent power. This highly efficient solution is ideal for various deployments, whether it's IT racks, network closets, automation control systems, and precision instruments to small-sized control rooms among other edge applications.</p>

                    <div class="row" style="margin-top: 60px">
                    	<div class="col-md-2"></div>
                    	<div class="col-md-4 text-center">
                    		<button class="brochure_btn">View Brochure</button>
                    	</div>
                    	<div class="col-md-4 text-center">
                    		<button class="quote_button">Get A Quote</button>
                    	</div>
                    	<div class="col-md-2"></div>
                    </div>

             	</div>
             </div>

            
            <div class="product_features">
             <div class="row">
             	<div class="col-md-2"></div>
                 
                 <div class="col-md-8">
                 	<div class="row">
                 		<div class="col-md-3 text-center feature_divs">
                 			<img src="assets/images/products/highly_reliable.png">
                 		</div>
                 		<div class="col-md-3 text-center feature_divs">
                 			<img src="assets/images/products/selectable_charging.png">
                 		</div>
                 		<div class="col-md-3 text-center feature_divs">
                 			<img src="assets/images/products/cold_start.png">
                 		</div>
                 		<div class="col-md-3 text-center feature_divs">
                 			<img src="assets/images/products/AVR.png">
                 		</div>
                 	</div>
                 </div>

             	<div class="col-md-2"></div>
             </div>
             </div>

        	</div>
        </div>

 -->



      
        

	</div>































<?php site_footer(); ?>
