<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/site_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
<style>
.container-outer img{
margin-top:-55px;
height:100px;
}
.registration{

	padding-top: 220px;
}
 @media (max-width: 991.98px) {
      .registration {

        padding:20px 20px 20px 20px;

        } 
      }
</style>
    <div class="page-wrapper">
		<!-- Header Here -->
		<?php bottom_menu(); ?>
		<?php site_top_menu(); ?>
		<?php slide_slider(); ?>
        <!-- End Hidden Bar -->   
		


<section>
	<div class="registration">
		<div class="container" style="background-color: #eff7fa !important;border-radius: 8px;border:1px solid #c3cfe2">
			<div class="row">
				<div class="col-md-12 registration_content">
					<h3>Registration Info</h3>

					<input type="text" name="full_name" class="form-control" placeholder="Full Name" id="user_name">
					<input type="text" name="email" class="form-control" placeholder="Email" id="user_email">
					<input type="password" name="password" class="form-control" placeholder="Password" id="user_password">
					<input type="password" name="fill_name" class="form-control" placeholder="Confirm Password" id="user_password1">

					<div class="row" style="margin-top: 30px;">
						<div class="col-md-4"></div>
						<div class="col-md-4">
							<button type="button" class="btn btn-primary form-control registration_btn" id="register_btn">Register</button>
							<hr/>
							<p style="text-align: center;margin-top: 20px;">or <a href="login.php">Login</a></p>
						</div>
						<div class="col-md-4"></div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>



<script>


$("#register_btn").click(function(){

  var user_name=$("#user_name").val();
  var user_email=$("#user_email").val();
  var user_password=$("#user_password").val();
  var user_password1=$("#user_password1").val();

  var datastr='user_name='+user_name+'&user_email='+user_email+'&user_password='+user_password+'&user_password1='+user_password1;


  if (user_name=='') {
   alert("Please fill full name");
 }

 else if (user_email=='') {
  alert("Please provide an email");
}
else if (user_password=='') {
  alert("Please confirm your password");
}
else if (user_password!==user_password1){
  swal("Passwords do not match");
}

else{

 $.ajax({


  type: "POST",
  url: "register_GET.php",
  data: datastr,
  cache: false,
  success: function(res){
    if(res==1)
    {
      	swal({
				  title: "Registration Success!",
				  text: "Redirecting You to Login page!",
				  type: "success",
				  confirmButtonText: "OK"
				},
				function(isConfirm){
				  if (isConfirm) {
					window.location.href = "login.php";
				  }
				});
    }
    else if(res==2){
    	swal("Sign Up Failed!", "Email Id Already Exist", "error");
    }
    else{


     swal("Something went wrong..Please try again later!");


   }

 }
}); 
}






});





	

</script>
















        
<?php site_footer(); ?>