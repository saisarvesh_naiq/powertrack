-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2020 at 05:42 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `powertrack`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_powertrac`
--

CREATE TABLE `admin_powertrac` (
  `admin_id` int(11) NOT NULL,
  `admin_email` varchar(200) NOT NULL,
  `admin_password` varchar(300) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_powertrac`
--

INSERT INTO `admin_powertrac` (`admin_id`, `admin_email`, `admin_password`, `status`) VALUES
(1, 'saisarvesh@gmail.com', 'sarvesh', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cust_order_data`
--

CREATE TABLE `cust_order_data` (
  `cust_order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `prod_qty` int(11) NOT NULL,
  `prod_price` float NOT NULL,
  `prod_total_price` float NOT NULL,
  `csid` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cust_order_data`
--

INSERT INTO `cust_order_data` (`cust_order_id`, `user_id`, `user_email`, `prod_id`, `prod_qty`, `prod_price`, `prod_total_price`, `csid`, `status`) VALUES
(24, 5, 'sarvesh@gmail.com', 1, 5, 20000, 60000, '', 0),
(25, 5, 'sarvesh@gmail.com', 2, 3, 1500, 6000, '', 0),
(26, 5, 'sarvesh@gmail.com', 1, 3, 20000, 60000, '', 0),
(27, 5, 'sarvesh@gmail.com', 5, 1, 6000, 6000, '', 0),
(28, 5, 'sarvesh@gmail.com', 5, 1, 6000, 6000, '', 0),
(29, 5, 'sarvesh@gmail.com', 1, 2, 20000, 20000, '', 0),
(30, 5, 'sarvesh@gmail.com', 2, 1, 1500, 1500, '', 0),
(31, 5, 'sarvesh@gmail.com', 4, 1, 2000, 2000, '', 0),
(32, 5, 'sarvesh@gmail.com', 1, 2, 20000, 40000, '', 0),
(33, 5, 'sarvesh@gmail.com', 3, 5, 30000, 150000, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_data`
--

CREATE TABLE `order_data` (
  `order_id` int(11) NOT NULL,
  `tracking_id` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_email` varchar(500) NOT NULL,
  `b_country` varchar(200) NOT NULL,
  `b_firstname` varchar(200) NOT NULL,
  `b_lastname` varchar(200) NOT NULL,
  `b_address` varchar(700) NOT NULL,
  `b_town` varchar(400) NOT NULL,
  `b_email` varchar(500) NOT NULL,
  `b_contact` varchar(20) NOT NULL,
  `s_country` varchar(200) NOT NULL,
  `s_firstname` varchar(100) NOT NULL,
  `s_lastname` varchar(100) NOT NULL,
  `s_address` varchar(500) NOT NULL,
  `s_town` varchar(100) NOT NULL,
  `payment_method` varchar(200) NOT NULL,
  `order_date` datetime NOT NULL,
  `total_amount` float NOT NULL,
  `order_note` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_data`
--

INSERT INTO `order_data` (`order_id`, `tracking_id`, `user_id`, `order_email`, `b_country`, `b_firstname`, `b_lastname`, `b_address`, `b_town`, `b_email`, `b_contact`, `s_country`, `s_firstname`, `s_lastname`, `s_address`, `s_town`, `payment_method`, `order_date`, `total_amount`, `order_note`, `status`) VALUES
(10, 'PWTRK1273c0d2b15', 5, '', 'India', 'saisarvesh', 'naik', 'curchorem,goa', 'curchorem', 'sarvesh@gmail.com', '8007023761', '', '', '', '', '', 'COD', '2020-10-14 20:28:37', 190000, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products_db`
--

CREATE TABLE `products_db` (
  `prod_id` int(11) NOT NULL,
  `prod_c_id` int(11) NOT NULL,
  `prod_name` varchar(600) NOT NULL,
  `prod_code` varchar(300) NOT NULL,
  `prod_details` varchar(10000) NOT NULL,
  `main_category` int(11) NOT NULL,
  `sub_category` int(11) NOT NULL,
  `prod_parent_id` int(11) NOT NULL,
  `prod_price` float NOT NULL,
  `is_discount` int(2) NOT NULL DEFAULT 0,
  `discount_price` float NOT NULL,
  `discount_perc` float NOT NULL,
  `pro_image_1` varchar(600) NOT NULL,
  `pro_image_2` varchar(600) NOT NULL,
  `pro_image_3` varchar(600) NOT NULL,
  `pro_image_4` varchar(600) NOT NULL,
  `pro_brand` int(11) NOT NULL,
  `pro_qty` int(11) NOT NULL,
  `pro_qty_order_per_user` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '0-deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products_db`
--

INSERT INTO `products_db` (`prod_id`, `prod_c_id`, `prod_name`, `prod_code`, `prod_details`, `main_category`, `sub_category`, `prod_parent_id`, `prod_price`, `is_discount`, `discount_price`, `discount_perc`, `pro_image_1`, `pro_image_2`, `pro_image_3`, `pro_image_4`, `pro_brand`, `pro_qty`, `pro_qty_order_per_user`, `status`) VALUES
(1, 123123, 'Liebert ITA2 UPS', '', 'In today\'s dynamic world, it is not enough for enterprises to have basic power protection. With digital trends constantly emerging and transforming the way you do business, business continuity is all the more vital. You simply cannot afford downtime in your critical system or waste time recovering these systems after a disruption. What you need is a robust, high-speed, reliable UPS system, which offers perennial, round-the-clock protection to diverse application needs.\r\n\r\n', 1, 1, 0, 20000, 0, 0, 0, '../assets/img/products/liebert-ita2-ups.jpg', '', '', '', 0, 18, 20, 1),
(2, 3243242, 'Liebert eXM 80kW - 200kW', '', 'Small and medium sized businesses need UPS solutions that deliver lower first costs and ongoing operational savings, high reliability, and enable speed and flexibility in a dynamic IT environment. Vertiv’s commitment is to deliver the most technologically advance product for our customer’s mission critical applications, is once again reiterated by our next generation UPS series Liebert eXMTM.', 1, 2, 0, 1500, 0, 0, 0, '../assets/img/products/liebert-exm.jpg', '', '', '', 0, 15, 15, 1),
(3, 234535, 'Liebert S600', '', 'The Liebert S600 is a fully-digital, highly reliable, double conversion UPS solution that delivers clean and consistent power. This highly efficient solution is ideal for various deployments, whether it\'s IT racks, network closets, automation control systems, and precision instruments to small-sized control rooms among other edge applications.', 1, 3, 0, 30000, 0, 0, 0, '../assets/img/products/liebert-s600.jpg', '', '', '', 0, 5, 10, 1),
(4, 4534543, 'NXG', '', 'NXG Range is our hybrid UPS range and can charge from Grid and Solar. NXG Inverter uses Solar on Priority, and in absence of Solar it will charge batteries from Grid\r\n\r\n', 2, 4, 0, 2000, 0, 0, 0, '../assets/img/products/NXG.png', '', '', '', 0, 50, 50, 1),
(5, 345345, 'NXT PCU', '', 'With more than 99.9% efficiency, the NXT PCUs are hybrid products with inbuilt MPPT charge controller, specially designed for solar applications.', 2, 5, 0, 6000, 0, 0, 0, '../assets/img/products/nxt_pcu.png', '', '', '', 0, 12, 12, 1),
(6, 5466, 'NXi Grid Tie', '', 'NXi inverters convert DC power generated from Solar panels into AC power which can be used for running loads and excess can be exported to the grid, maximizing your savings.', 2, 6, 0, 15000, 0, 0, 0, '../assets/img/products/nxi_grid.png', '', '', '', 0, 30, 30, 1),
(7, 54654, 'Cruze Solar', '', 'Looking for economical solar solutions? The Luminous Cruze Solar inverter run heavy loads with extreme ease and efficiency!', 2, 7, 0, 14000, 0, 0, 0, '../assets/img/products/liebert-exm.jpg', '', '', '', 0, 16, 16, 1),
(8, 2342342, '3 Phase Air Cooled Servo Controlled Voltage Stabilizers', '', 'Provides perfectly stable output even under severe conditions of unbalanced voltage conditions. Ideal to protect the electrical and electronic equipments from high and low voltage.<br>\r\n1) Single Phasing Prevention<br>\r\n2) Output Electronic Over Load Protection<br>\r\n3) Phase Reversal Protection<br>\r\n4) Input Short Circuit Protection with MCB<br>\r\n5) Input Over Load Protection with MCB<br>\r\n6) Output Low Voltage Protection', 3, 8, 0, 17000, 0, 0, 0, '../assets/img/products/3_phase_stabilizer.jpg', '', '', '', 0, 45, 45, 1),
(9, 23553, 'Servo Controlled Voltage Stabilizers - 3 Phase Oil Cooled', '', 'Provides perfectly stable output even under severe conditions of unbalanced voltage conditions. Ideal to protect the electrical and electronic equipments from high and low voltage.<br>\r\n1) Single Phasing Prevention<br>\r\n2) Output Electronic Over Load Protection<br>\r\n3) Phase Reversal Protection<br>\r\n4) Input Short Circuit Protection with MCB<br>\r\n5) Input Over Load Protection with MCB<br>\r\n6) Output Low Voltage Protection<br>\r\n7) Output High Voltage Protection', 3, 9, 0, 12500, 0, 0, 0, '../assets/img/products/servo_controlled_voltage.jpg', '', '', '', 0, 10, 10, 1),
(10, 3456546, '3 Phase Resendial Servvo Controlled Voltage Stabalizers', '', 'Provides perfectly stable output even under severe conditions of unbalanced voltage conditions. Ideal to protect the electrical and electronic equipments from high and low voltage.<br>\r\n1) Output voltage set ( 210-245V)<br>\r\n2) Output Low Voltage Cutoff<br>\r\n3) Input High on LCD Display<br>\r\n4) Sensitivity / Regulation<br>\r\n5) Over Load Cutoff<br>\r\n6) CT full scale<br>\r\n7) Output High Voltage Cutoff<br>\r\n8) Input Low on LCD Display', 3, 10, 0, 17000, 0, 0, 0, '../assets/img/products/3_phase_residential_servo.jpg', '', '', '', 0, 20, 20, 1),
(11, 45647, 'Linear Automatic Servo Stabilizers', '', 'Servo Voltage Stabilizers come in a wide range of models. The standard 3-phase models are suitable for balanced & unbalanced supply/loads, which conform to the following<br>\r\n1) Cement Plants<br>\r\n2) Air Ports<br>\r\n3) Rolling Plants<br>\r\n4) Flour Mills<br>\r\n5) Engineering Units<br>\r\n6) Cell Phone<br> \r\n7) Networks', 3, 11, 0, 24000, 0, 0, 0, '../assets/img/products/linear_automatic_servo.jpg', '', '', '', 0, 12, 12, 1),
(12, 45667, 'Static Voltage Stabilizer', '', 'With Very High Speed of Correction provides perfectly stable output even under severe conditions of unbalanced voltage conditions. Ideal to protect the electrical and electronic equipments from high and low voltage.', 3, 12, 0, 30000, 0, 0, 0, '../assets/img/products/static_voltage_stabilizer.jpg', '', '', '', 0, 18, 18, 1),
(13, 657678, 'Regalia', '', '<strong>A Revolution in Power Back-up Systems</strong><br><br>\r\n             		Ever thought a UPS could be smart? That\'s what we call the Regalia, India\'s first wall mounted smart power back-up system! Using Li-Ion batteries that have a lifecycle of upto 10 years and require no-maintenance at all, we aren\'t kidding. To top it off, it has a touch screen to show the back-up time and battery charging time in colourful display!', 4, 13, 0, 5000, 0, 0, 0, '../assets/img/products/regalia.png', '', '', '', 0, 14, 14, 1),
(14, 547768, 'Zelio', '', 'When Control Matters The Most</strong><br><br>\r\n             		With more than 99.9% efficiency, the NXT PCUs are hybrid products with inbuilt MPPT charge controller, specially designed for solar applications.', 4, 14, 0, 19000, 0, 0, 0, '../assets/img/products/zelio_inverter.png', '', '', '', 0, 28, 28, 1),
(30, 0, 'Diesel Generators', '', 'Known for the ruggedness, reliability of Ashok Leyland make engines, the company takes pride in having powered over 150,000 plus diesel generators over last ten years. This incredible confidence has made Ashok Leyland to enhance the offering to wider range of engines from 5 kVA to 2250 kVA. Ashok Leyland engines are aesthetically designed, highly fuel efficient, environment-friendly and are low on operating costs.<br>\r\n1) Reliable, Robust & Rugged engines designed to deliver continuously in arduous environment<br>\r\n2) Better Block Loading Capability<br>\r\n3) Compact engine - Engineered for Optimized Power Solutions<br>\r\n4) Low Ownership Cost<br>\r\n5) Low Oil & Fuel consumption<br>\r\n6) Continuous duty Power rating<br>\r\n7) Minimal vibrations and lower noise levels<br>\r\n8) Easy Serviceability & Repairability<br>\r\n9) Standardized Design for complete range<br>\r\n10) Wide after-sales & parts support', 5, 0, 0, 12344, 0, 0, 0, '../assets/img/products/13102020142226diesel_generators.jpg', '', '', '', 0, 12, 12, 1),
(31, 0, 'Distrbution & Power Transformers', '', 'Subjected to rigorous and strict quality controls at every stage of manufacturing, Designed to meet latest national international standards like BIS, IS, IEC, ANSI, BS, DIN, etc., Wide range of Transformers duly tested at Central Power Research Institute, Bangalore , Bhopal Quality assurance programme available<br>\n1) Tube Mills <br>\n2) Telecom Centers <br>\n3) Rice Mills<br>\n4) Rolling Plants<br>\n5) Educational Institutions<br>\n6) Cold Storage\n', 7, 0, 0, 34500, 0, 0, 0, '../assets/img/products/13102020143933Distribution-Transformer.jpg', '', '', '', 0, 21, 21, 1),
(32, 0, 'Industrial Transformers', '', 'It is designed for 30-35 degree C above ambient. Suitable for any kind of ambient temperature conditions.<br>\r\nCore is constructed from Low Loss of CRGO M-4 Grade confirming to latest standards & is fitted & clamped with special built in-house frames to reduce the magnetic noise & to make the structure rigid & robust.<br>\r\nUse paper covered electrolytic grade copper strip for winding. Cooling ducts are provided to keep the hot spot temperature as low as possible.<br>\r\nTanks are made of M.S. Sheets with adequate bracing & stiffeners are given a coat of Epoxy Primer & Epoxy paint for better life. Both light grey & Siemens grey (RAL 7032) are available in colour. ', 7, 0, 0, 60000, 0, 0, 0, '../assets/img/products/13102020144216Industrial-Transformers.jpg', '', '', '', 0, 8, 8, 1),
(33, 0, 'DRY TYPE TRANSFORMER', '', 'Dry type transformers are suitable for indoor installations in explosive atmospheric areas and populated locations such as malls, multiplex etc. These dry type transformers have F or H insulation class with or without enclosure as per the site requirement.\r\nDry transformers are very compact in sizes and hence suitable for basement installations in high rise buildings etc. Dry transformers are available with low losses giving savings over long service periods. Periodical vacuum cleaning of winding surface is the only maintenance. The capacity range of these iron core transformer is up to 2000 KVA with primary Voltage up to 11 KV class & secondary Voltage between 433 to 6600 Volts.\r\n\r\n', 7, 0, 0, 3434, 0, 0, 0, '../assets/img/products/13102020144648dry-transformer.jpg', '', '', '', 0, 34, 34, 1),
(34, 0, 'H.T. Transformer', '', 'Even after the installation of standard off circuit distribution transformer, the voltage fluctuation on the LT side persists due to limited range of voltage correction of the transformer. To control the voltage fluctuations, it is ideal to install Servo Controlled Automatic Voltage Controller either on LT side or HT side.<br>\r\nSERVOMAX make Transformers with built-in rolling contact type AVR suitable for indoor/outdoor installation, copper wound HT Two-in-one system is a revolutionary landmark in the industry when it comes to voltage regulation and stabilization. Even after the installation of standard distribution transformer the problem of low/high voltage on the LT side persists, resulting in improper operation of the electrical equipments, premature failure & production loss of a plant.\r\n\r\n', 7, 0, 0, 4321, 0, 0, 0, '../assets/img/products/13102020145034H.T_transformer.jpg', '', '', '', 0, 32, 32, 1),
(35, 0, 'H.T. Automatic Voltage Regulator', '', 'Even after the installation of standard off circuit distribution transformer, the voltage fluctuation on the LT side persists due to limited range of voltage correction of the transformer. To control the voltage fluctuations, it is ideal to install Servo Controlled Automatic Voltage Controller either on LT side or HT side.<br>\r\nSERVOMAX make Transformers with built-in rolling contact type AVR suitable for indoor/outdoor installation, copper wound HT Two-in-one system is a revolutionary landmark in the industry when it comes to voltage regulation and stabilization. Even after the installation of standard distribution transformer the problem of low/high voltage on the LT side persists, resulting in improper operation of the electrical equipments, premature failure & production loss of a plant.', 7, 0, 0, 8065, 0, 0, 0, '../assets/img/products/13102020145311H.T_automatic_voltage_stabilizer.jpg', '', '', '', 0, 25, 25, 1),
(36, 0, 'Furnace Transformers', '', 'Multiple Shielding Techniques employed to reduce the inter winding capacitance to below 0.005 picofarad and increases DC Isolation to over 1000 Megaohms.<br>\r\n\r\nTechnically any transformer that has no direct current path between its primary & secondary windings provides isolation from any kind of leakage in Current, Spikes/Surges produced in the system. Provision of electrostatic shield in between primary & secondary winding depends upon its construction, specification & performance characteristics.<br>\r\n\r\nIsolation / Ultra Isolation Transformer are being used to protect Electronic & Sophisticated equipments, CNC Machines from the excessive line voltage transients, spikes / surges produced in the system. Electrostatic shielding helps in grounding these spikes & not allowing to pass on to the secondary side of the transformer.\r\n\r\n', 7, 0, 0, 99878, 0, 0, 0, '../assets/img/products/13102020145503Furnace-Transformers.jpg', '', '', '', 0, 77, 77, 1),
(37, 0, 'VRLA - NEPST ', '', 'This valve regulated lead-acid battery range has a design life of 20 years at 27°C under ideal float condition. The capacity of these batteries ranges from 200Ah to 6000 Ah at 27°C down to 1.75 VPC. The operating temperature range is -5°C to 40°C.</p>\r\n           <ul>\r\n            <li style=\"list-style-type:circle;\"><p> These batteries give 1800 cycles at 50% DOD.</p></li>\r\n            <li style=\"list-style-type:circle;margin-top: -30px;\"><p>  Excellent recovery from deep discharge.</p></li>\r\n            <li style=\"list-style-type:circle;margin-top: -30px;\"><p> Antimony free alloy is used and hence self-discharge is less than 0.5% per week of C10 capacity at 27°C.</p></li>\r\n            <li style=\"list-style-type:circle;margin-top: -30px;\"><p> Antimony free alloy is used and hence self-discharge is less than 0.5% per week of C10 capacity at 27°C.</p></li>\r\n            <li style=\"list-style-type:circle;margin-top: -30px;\"><p>  No water topping up required ever resulting in saving of hundreds of liters of distilled water and manpower required for topping up throughout life of battery.</p></li>\r\n            <li style=\"list-style-type:circle;margin-top: -30px;\"><p> The gas recombination technology cycle effectively nullifies generation of gas during normal use resulting in no emission of corrosive fumes under normal operative conditions and hence no elaborate air exhaust system is required.</p></li>\r\n            <li style=\"list-style-type:circle;margin-top: -30px;\"><p>  Specifications conforming to GR No. TEC/GR/TX/BAT-001/04.JUN 2011. JIS: C 8704-2:1999 IEC: 60896 - 21 & 22 ANSI: T1 330(US specification for Telecom battery) RDSO: IRS S93-96 with latest amendment.</p></li>\r\n            <li style=\"list-style-type:circle;margin-top: -30px;\"><p>  NEPST Range: Capacity Range 200Ah to 400 Ah @ 27°C down to 1.75 VPC - NMST Range: Capacity Range 600 Ah to 6000 Ah @ 27°C down to 1.75 VPC.</p></li>\r\n          </ul>\r\n', 6, 0, 0, 8033, 0, 0, 0, '../assets/img/products/14102020171207VRLA-NEPST.jpg', '', '', '', 0, 21, 21, 1),
(38, 0, 'GEL TUBULAR - GTB RANGE', '', 'Exide offers GTB range in Tubular Gel VRLA Battery in the Telecom sector. The GTB Range of Capacity ranging from 200 Ah to 600 Ah @ 35°C down to 1.75 VPC.</p>\r\n\r\n       <ul>\r\n        <li style=\"list-style-type:circle;\"><p>The cycle life at 35 degree Celsius is 3000 cycles at 50% DOD.</p></li>\r\n        <li style=\"list-style-type:circle;margin-top: -10px;\"><p>The performance of GTB batteries conforms to IEC 61427, IEC 60896-21/22 & TEC Spec No. TEC/GR/TX/BAT-003/02 MAR 2011.</p></li>\r\n        <li style=\"list-style-type:circle;margin-top: -10px;\"><p>Rugged and reliable for cyclic application, ideal for frequent discharge-charge cycle.</p></li>\r\n        <li style=\"list-style-type:circle;margin-top: -10px;\"><p>Very low self-discharge.</p></li>\r\n        <li style=\"list-style-type:circle;margin-top: -10px;\"><p>No water topping up ever.</p></li>\r\n        <li style=\"list-style-type:circle;margin-top: -10px;\"><p>Low electrical resistance, high charging efficiency.</p></li>\r\n        <li style=\"list-style-type:circle;margin-top: -10px;\"><p>Resistant to separator damage.</p></li>\r\n        <li style=\"list-style-type:circle;margin-top: -10px;\"><p>No acid stratification, low gassing due to internal gas recombination, good heat dissipation characteristics resulting longer cycle life.</p></li>\r\n        <li style=\"list-style-type:circle;margin-top: -10px;\"><p>Excellent energy saving feature and quick recharge. Low foot print, cells are housed in stackable MS modules (8V).</p></li>\r\n        <li style=\"list-style-type:circle;margin-top: -10px;\"><p> Explosion proof, self-resealing, pressure regulating and can be safely used in high ambient temperature zone.</p></li>\r\n\r\n      </ul>\r\n', 6, 0, 0, 3123, 0, 0, 0, '../assets/img/products/14102020171941GEL-TUBULAR.jpg', '', '', '', 0, 123, 123, 1),
(39, 0, 'TRACTION & MOTIVE POWER', '', '<strong>TRACTION & MOTIVE POWER</strong></p>\r\n     <p style=\"margin-top: -30px;\">Over the years, Exide batteries have become the driving force in principal economic sectors globally. In many critical sectors batteries are essential to start electrically operated equipment and keep them running in absence of main power. Forklift Trucks in Material Handling applications, Mining Locomotives, Golf Cart, Electric Vehicles, Naval applications - all depend on Exide to fulfil their needs for start-up and motive power. Exide Traction Batteries are supplied to one of the world\'s leading OEM Electric Truck Manufacturer in Germany, the country which is Exide\'s main Traction market after India.\r\n     </p>\r\n     <p><strong>FLOODED HSP CLASSIC RANGE AND FLOODED GEN-X RANGE</strong></p>\r\n     <p style=\"margin-top: -30px;\">Flooded HSP Classic Range<br>\r\n     The very popular and ever dependent HSP range has been powering various makes of forklifts for more than two decades. The HSP range is available in BS and DIN range from 58Ah to 841 Ah in BS and 120Ah to 1400Ah in DIN. Available in both British and Continental standards, the HSP range can fit into all the makes of forklifts.', 6, 0, 0, 321, 0, 0, 0, '../assets/img/products/14102020172115traction_and_motive.jpg', '', '', '', 0, 324, 324, 1),
(40, 0, 'Flooded Gen-X Range', '', 'The Exide Gen-X range comes with 15% additional capacity in the same dimensions of the HSP range. The higher diameter of the gauntlet tubes increases the annular volume resulting in increased power. The advance formula of Red lead and Grey oxide used in the positive active material ensures increased power and long life. The Exide Gen-X range of batteries is at the highest technology level and has very high efficiency.', 6, 0, 0, 4322, 0, 0, 0, '../assets/img/products/14102020172308flooded_genx.jpg', '', '', '', 0, 21, 21, 1),
(41, 0, 'Amaron (Batteries)', '', 'Amara Raja Batteries Limited (ARBL), in collaboration with Johnson Controls Inc., USA, is the largest manufacturer of Standby Valve Regulated Lead Acid (VRLA) batteries in the Indian Ocean Rim region. We offer the discerning Indian customer an extensive range of India\'s most powerful zero-maintenance batteries. Our fully integrated world-class manufacturing facility is situated in the holy city of Tirupati, in Andhra Pradesh, India. Amaron batteries incorporate the latest technological advances in the field of captive power and are on par with batteries manufactured and marketed anywhere in the world.\r\n\r\n', 8, 0, 0, 3422, 0, 0, 0, '../assets/img/products/14102020173456amron1.png', '', '', '', 0, 12, 12, 1),
(42, 0, 'Powerzone (Batteries)', '', 'Powerzone from Amara Raja Batteries Limited is a retail initiative catering to rural and semi-urban India’s captive power needs with formatted retail outlets spread across the country. The Powerzone Retail Stores is a One Stop shop for all Power Needs providing global quality at local prices. The solutions on offer include Automotive Batteries, Two wheeler batteries, inverters/home UPS & inverter batteries. The product range is ever increasing as the needs of the rural and semi-urban customer too are increasing. Amara Raja Batteries Limited (ARBL), the largest manufacturer of Standby Valve Regulated Lead Acid (VRLA) batteries in the Indian Ocean', 8, 0, 0, 45345, 0, 0, 0, '../assets/img/products/14102020173536powerzone.png', '', '', '', 0, 51, 51, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pro_main_category`
--

CREATE TABLE `pro_main_category` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(200) NOT NULL,
  `cat_status` int(10) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pro_main_category`
--

INSERT INTO `pro_main_category` (`cat_id`, `cat_name`, `cat_status`) VALUES
(1, 'ONLINE UPS', 1),
(2, 'LUMINOUS SOLAR DIVISION', 1),
(3, 'STABILIZERS', 1),
(4, 'INVERTERS & UPS', 1),
(5, 'GENSET DIVISION', 1),
(6, 'BATTERIES', 1),
(7, 'POWER TRANSFORMERS', 1),
(8, 'AUTOMOTIVE BATTERIES', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pro_sub_category`
--

CREATE TABLE `pro_sub_category` (
  `sub_id` int(11) NOT NULL,
  `cat_name` varchar(500) NOT NULL,
  `main_cat_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pro_sub_category`
--

INSERT INTO `pro_sub_category` (`sub_id`, `cat_name`, `main_cat_id`, `status`) VALUES
(1, 'Liebert ITA2 UPS', 1, 1),
(2, 'Liebert eXM 80kW - 200kW', 1, 1),
(3, 'Liebert S600', 1, 1),
(4, 'NXG', 2, 1),
(5, 'NXT PCU', 2, 1),
(6, 'NXi Grid Tie', 2, 1),
(7, 'Cruze Solar', 2, 1),
(8, '3 Phase Air Cooled Servo Controlled Voltage Stabilizers', 3, 1),
(9, 'Servo Controlled Voltage Stabilizers - 3 Phase Oil Cooled', 3, 1),
(10, '3 Phase Resendial Servvo Controlled Voltage Stabalizers', 3, 1),
(11, 'Linear Automatic Servo Stabilizers', 3, 1),
(12, 'Static Voltage Stabilizer', 3, 1),
(13, 'Regalia', 4, 1),
(14, 'Zelio', 4, 1),
(15, 'Cruze', 4, 1),
(16, 'Eco Volt & Electra Sine', 4, 1),
(17, 'Eco Watt & Electra', 4, 1),
(18, 'Shakti Charge & Rhino Charge', 4, 1),
(19, 'Eco Watt Rapid', 4, 1),
(20, 'VRLA - NEPST & NMST RANGE', 6, 1),
(21, 'GEL TUBULAR - GTB RANGE', 6, 1),
(22, 'TRACTION & MOTIVE POWER', 6, 1),
(23, 'Flooded Gen-X Range', 6, 1),
(24, 'NDP & HDP TUBULAR RANGE', 6, 1),
(25, 'TBS / OPZS RANGE', 6, 1),
(26, 'Flooded 6EL Range', 6, 1),
(27, 'Flooded 6EL plus Range', 6, 1),
(28, 'Flooded 6EL Master Range', 6, 1),
(29, 'VRLA Exide Powersafe plus Range', 6, 1),
(30, 'Advance VRLA Exide Powersafe NXT Range', 6, 1),
(31, 'CHLORIDE SAFEPOWER & CHLORIDE DRAGON BATTERY', 6, 1),
(32, 'Distrbution & Power Transformers', 7, 1),
(33, 'Industrial Transformers', 7, 1),
(34, 'DRY TYPE TRANSFORMER', 7, 1),
(35, 'H.T. Transformer', 7, 1),
(36, 'H.T. Automatic Voltage Regulator', 7, 1),
(37, 'Furnace Transformers', 7, 1),
(46, 'new product', 1, 1),
(47, 'Diesel Generators', 5, 1),
(48, 'Diesel Generators', 5, 1),
(49, 'Diesel Generators', 5, 1),
(50, 'Distrbution ', 7, 1),
(51, 'Industrial Transformers', 7, 1),
(52, 'DRY TYPE TRANSFORMER', 7, 1),
(53, 'H.T. Transformer', 7, 1),
(54, 'H.T. Automatic Voltage Regulator', 7, 1),
(55, 'Furnace Transformers', 7, 1),
(56, 'VRLA - NEPST ', 6, 1),
(57, 'GEL TUBULAR - GTB RANGE', 6, 1),
(58, 'TRACTION ', 6, 1),
(59, 'Flooded Gen-X Range', 6, 1),
(60, 'Amaron (Batteries)', 8, 1),
(61, 'Powerzone (Batteries)', 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_db`
--

CREATE TABLE `user_db` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `hash_code` varchar(255) NOT NULL,
  `cart_id` varchar(255) NOT NULL,
  `join_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_db`
--

INSERT INTO `user_db` (`user_id`, `user_name`, `user_email`, `user_password`, `hash_code`, `cart_id`, `join_date`, `status`) VALUES
(2, 'saisarvesh naik', 'saisarvesh.naik@gmail.com', 'nopass', '2bac648ed005856ff8af0a2f9cd3ed198fc85b87', '', '2020-10-07 07:07:25', 1),
(4, 'asdasdasd', 'light@gmail.com', 'nopass', 'c59c4d5d9d47321adc85114104f8a44c53e4bf99', '', '2020-10-07 08:13:16', 1),
(5, 'naruto', 'sarvesh@gmail.com', 'hahahaha', '97d6a64d6072553c7d9c86e48bff568b2f1522b6', '', '2020-10-09 06:00:48', 1),
(6, 'sausuke', 'sasuke@gmail.com', 'uchiha', '9339ce01dc91268b1f6db350e96c0f490f0e33cb', '', '2020-10-09 06:27:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_details`
--

CREATE TABLE `user_sub_details` (
  `user_sub_details_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `profile_pic` varchar(1000) NOT NULL,
  `email_id` varchar(500) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `company` varchar(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `pincode` int(11) DEFAULT NULL,
  `town` varchar(300) NOT NULL,
  `country` varchar(100) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_sub_details`
--

INSERT INTO `user_sub_details` (`user_sub_details_id`, `user_id`, `profile_pic`, `email_id`, `firstname`, `lastname`, `company`, `address`, `pincode`, `town`, `country`, `contact_number`, `status`) VALUES
(4, 5, 'assets/images/profile_pictures/12102020105140Bootstrapgrids.jpg', 'sarvesh@gmail.com', 'saisarvesh', 'naik', '85tech', 'curchorem,goa', 123456, 'curchorem', 'India', '8007023761', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `wishlist_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `prod_qty` int(11) NOT NULL,
  `prod_price` float NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`wishlist_id`, `user_id`, `prod_id`, `prod_qty`, `prod_price`, `status`) VALUES
(3, 5, 11, 1, 24000, 0),
(4, 5, 6, 1, 15000, 0),
(5, 5, 12, 1, 30000, 0),
(6, 5, 5, 1, 6000, 0),
(7, 5, 1, 1, 20000, 0),
(8, 5, 2, 1, 1500, 0),
(9, 5, 4, 1, 2000, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_powertrac`
--
ALTER TABLE `admin_powertrac`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `cust_order_data`
--
ALTER TABLE `cust_order_data`
  ADD PRIMARY KEY (`cust_order_id`);

--
-- Indexes for table `order_data`
--
ALTER TABLE `order_data`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `products_db`
--
ALTER TABLE `products_db`
  ADD PRIMARY KEY (`prod_id`);

--
-- Indexes for table `pro_main_category`
--
ALTER TABLE `pro_main_category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `pro_sub_category`
--
ALTER TABLE `pro_sub_category`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `user_db`
--
ALTER TABLE `user_db`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_sub_details`
--
ALTER TABLE `user_sub_details`
  ADD PRIMARY KEY (`user_sub_details_id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`wishlist_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_powertrac`
--
ALTER TABLE `admin_powertrac`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cust_order_data`
--
ALTER TABLE `cust_order_data`
  MODIFY `cust_order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `order_data`
--
ALTER TABLE `order_data`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `products_db`
--
ALTER TABLE `products_db`
  MODIFY `prod_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `pro_main_category`
--
ALTER TABLE `pro_main_category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pro_sub_category`
--
ALTER TABLE `pro_sub_category`
  MODIFY `sub_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `user_db`
--
ALTER TABLE `user_db`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_sub_details`
--
ALTER TABLE `user_sub_details`
  MODIFY `user_sub_details_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `wishlist_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
