<?php
error_reporting(0);
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/online_ups_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
  <style>
    .container-outer img{
      margin-top:-55px;
      height:100px;
    }
  </style>
  <div class="page-wrapper">
    <!-- Header Here -->
    <?php bottom_menu(); ?>
    <?php site_top_menu(); ?>
    <?php slide_slider(); ?>






    <section>

      <div class="shopping_cart">
       <div class="container2">
         <div class="row">
           <div class="col-md-8 shopping_content">

            <div class="row">
              <div class="col-md-6">
                <h2>Shopping Cart</h2>
              </div>
              <div class="col-md-6 text-right">

               <?php 

               require 'connect.php';
               $data = $conn->query("SELECT count(cust_order_id) as Total1 FROM cust_order_data WHERE user_id='".$_SESSION["user_id"]."' AND status='1'")->fetchAll();
               foreach ($data as $row3) {  
                ?>

                <h3 class="cart_item_quantity"><?php echo $row3['Total1']; ?> Items</h3>
              </div>
            </div>


          <?php } ?>

          <hr/>


          <div class="row">
            <div class="col-md-5">
              <p><strong>PRODUCT DETAILS</strong></p>
            </div>
            <div class="col-md-3 text-center">
              <p><strong>QUANTITY</strong></p>
            </div>
            <div class="col-md-2 text-center">
             <p><strong>PRICE</strong></p>                
           </div>
           <div class="col-md-2 text-center">
            <p><strong>TOTAL</strong></p>
          </div>
        </div>


        <?php
        require 'connect.php';
        $sql= $conn->prepare("SELECT * FROM cust_order_data WHERE user_id='".$_SESSION["user_id"]."' AND status='1'");
        $sql->execute();
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        if($sql->rowCount()>0){
          foreach (($sql->fetchAll()) as $key => $row) {


           $sql2= $conn->prepare("SELECT * FROM products_db WHERE prod_id='".$row["prod_id"]."'");
           $sql2->execute();
           $sql2->setFetchMode(PDO::FETCH_ASSOC);
           if($sql2->rowCount()>0){
            foreach (($sql2->fetchAll()) as $key => $row2) {


              echo '

              <div class="row">
              <div class="col-md-5">
              <div class="row" style="margin-top: 20px;">
              <div class="col-md-5 text-center cart_img_div">
              <img src="admin/products/'.$row2['pro_image_1'].'">
              </div>
              <div class="col-md-7 cart_product_desc">
              <h5>'.$row2['prod_name'].'</h5>
              <button class="btn btn-primary remove_btn" onclick=\'deletefunc("' .$row["cust_order_id"]. '")\' >REMOVE</button>
              </div>
              </div>

              </div>
              <div class="col-md-3 text-center ">
              <div class="btn-group text-center add_subtract_div">
              <button class="btn btn-primary product_add" onclick=\'subtractfunc("' .$row["cust_order_id"]. '")\' ><strong>-</strong></button>
              <button class="btn btn-primary product_quantity">'.$row['prod_qty'].'</button>
              <button class="btn btn-primary product_subtract" onclick=\'addfunc("' .$row["cust_order_id"]. '")\' ><strong>+</strong></button>
              </div>
              </div>
              <div class="col-md-2 text-center">
              <div class="row">
              <div class="col-md-12 cart_product_price_div">
              <p>&#8377; '.$row['prod_price'].'</p>
              </div>
              </div>
              </div>

              <div class="col-md-2 text-center">
              <div class="row">
              <div class="col-md-12 cart_product_total_price_div">
              <p>&#8377; '.$row['prod_total_price'].'</p>
              </div>
              </div>
              </div>
              </div>
              <hr style="margin:0;padding:0;"/>
              ';





            }
          }
        }
      }
      else{

        echo'<h3 style="margin-top:40px;">There are no products in your cart!</h3>';
      }
      ?>

    </div>


    <?php 
    require 'connect.php';
    $data2 = $conn->query("SELECT SUM(prod_total_price) as Total2 FROM cust_order_data WHERE user_id='".$_SESSION["user_id"]."' AND status='1'")->fetchAll();
    foreach ($data2 as $row4) { 



      ?>



      <div class="col-md-4 order_summary">
       <h2>Order Summary</h2>
       <hr/>
       <div class="row">
         <div class="col-md-6">
           <p><strong>ITEMS <?php echo $row3['Total1']; ?></strong></p>
         </div>
         <div class="col-md-6">
           <p style="text-align: right;"><strong>&#8377; <?php echo $row4['Total2']; ?></strong></p>
         </div>
       </div>

   <!--     <p style="margin-top: 20px;"><strong>SHIPPING</strong></p>

       <select name="payment_method" id="shipping_dropdown" class="form-control">
        <option value="standard">Standard Delivery - &#8377; 200</option>
        <option value="cod">COD</option>
        <option value="etc">etc</option>
        <option value="etc">etc</option>
      </select> -->


     <!--  <p style="margin-top: 20px;"><strong>PROMO CODE</strong></p>
      <input type="text" name="promo_code" class="form-control promo_code" placeholder="Enter your code">

      <button class="btn btn-primary apply_btn" >APPLY</button> -->

      <hr style="margin-top: 30px;margin-bottom: 30px;" />

      <div class="row">
       <div class="col-md-6">
         <p><strong>TOTAL COST</strong></p>
       </div>
       <div class="col-md-6 text-right">
         <p><strong>&#8377;<?php echo $row4['Total2']; ?></strong></p>
       </div>
     </div>
     
   <?php } ?>

     <a href="checkout.php"><button class="btn btn-primary form-control checkout_btn" id="checkout_btn">CHECKOUT</button></a>


 </div>
</div> 
</div>
</div>

</section>


<!----------------Adding/subracting quantity from cart------------------->
<script type="text/javascript">

   function addfunc(id)
 { 
   window.location.href='add_quantity_GET.php?id='+id;
 }


    function subtractfunc(id)
 { 
   window.location.href='subtract_quantity_GET.php?id='+id;
 }

</script>


<!-----------------Deleting product from cart-------------------->
<script type="text/javascript">
  function deletefunc(id)
  { 
    var r = confirm("Remove Product From cart?");
    if (r == true) {


     var dataString = 'id='+ id;


     $.ajax({
      type: "POST",
      url: "remove_cart_product.php",
      data: dataString,
      cache: false,
      success:function(data){
        if(data == 1){

          swal("", "Product Removed From The Cart.", "info");

          setTimeout(function(){
           window.location.reload();
         }, 3000);



        }
        else{


        }

      }
    });

   }
 }

</script>








<?php site_footer(); ?>
