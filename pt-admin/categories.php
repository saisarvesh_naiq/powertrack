<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/main_header.php");
include_once("template-parts/top_bar.php");
include_once("includes/db_include.inc.php");
include_once("includes/function.inc.php");
if(isset($_SESSION['ADMIN_LOGIN']) && $_SESSION['ADMIN_LOGIN']!=''){

}else{
	header('location:index.php');
	die();
}
if(isset($_GET['type']) && $_GET['type']!=''){
	$type=get_safe_value($con,$_GET['type']);
	if($type=='status'){
		$operation=get_safe_value($con,$_GET['operation']);
		$id=get_safe_value($con,$_GET['id']);
		if($operation=='active'){
			$status='1';
		}else{
			$status='0';
		}
		$update_status_sql="UPDATE `pt_24_categories` SET `cat_status`='$status' WHERE `cat_id`='$id'";
		mysqli_query($con,$update_status_sql);
	}
	
	if($type=='delete'){
		$id=get_safe_value($con,$_GET['id']);
		$delete_sql="delete from `pt_24_categories` where cat_id='$id'";
		mysqli_query($con,$delete_sql);
	}
}
site_header(); ?>
<body class="cbp-spmenu-push">
<style>
.btn a{
	color:#fff;
	font-weight:900;
}
.btn a:hover{
	color:#fff;
	font-weight:900;
}
</style>
	<div class="main-content">
		<!-- Navigation -->
		<?php bottom_menu(); ?>
		<!-- header-starts -->
		<?php top_bar(); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<div class="tables">
					<div class="bs-example widget-shadow" data-example-id="bordered-table"> 
						<h4>Bordered Basic Table:</h4>
						<table class="table table-bordered"> 
						<thead> 
						<tr> 
						<th>Sr.No</th> 
						<th>Category Name</th> 
						<th>Status</th> 
						<th>Edit</th> 
						<th>Delete</th></tr> 
						</thead><tbody> 
						<?php 
						$sql="SELECT `cat_id`, `cat_name`, `cat_status` FROM `pt_24_categories` order by `cat_name` asc";
					    $res=mysqli_query($con,$sql);
						$i=1;
						while($row=mysqli_fetch_assoc($res)){
						?>
						<tr><th scope="row"><?php echo $i++;?></th><td><?php echo $row['cat_name']?></td><td><?php if($row['cat_status']==1){ ?>
								<span class="btn btn-success"><a href="?type=status&operation=deactive&id=<?php echo $row['cat_id']?>">Active</a></span>&nbsp;
							<?php	}else{ ?>
								<span class="btn btn-warning"><a href="?type=status&operation=active&id=<?php echo $row['cat_id']?>">Deactive</a></span>&nbsp;
							<?php	} ?></td><td><span class="btn btn-info"><a href="update_category.php?id=<?php echo $row['cat_id']?>">Edit</a></span></td> <td><span class="btn btn-danger"><a href="?type=delete&id=<?php echo $row['cat_id']?>" onclick="return confirm('Are you sure you want to delete this Category?');">Delete</a></span></td></tr>  <?php } ?></tbody> </table>
					</div>
					<div class="bs-example widget-shadow" data-example-id="bordered-table"> 
						<h4>Bordered Basic Table:</h4>
						<table class="table table-bordered"> <thead> <tr> <th>#</th> <th>First Name</th> <th>Last Name</th> <th>Username</th> </tr> </thead> <tbody> <tr> <th scope="row">1</th> <td>Mark</td> <td>Otto</td> <td>@mdo</td> </tr> <tr> <th scope="row">2</th> <td>Jacob</td> <td>Thornton</td> <td>@fat</td> </tr> <tr> <th scope="row">3</th> <td>Larry</td> <td>the Bird</td> <td>@twitter</td> </tr> </tbody> </table>
					</div>
				</div>
			</div>
		</div>
<?php echo site_footer(); ?>
</body>
</html>