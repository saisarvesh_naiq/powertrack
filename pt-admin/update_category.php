<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/main_header.php");
include_once("template-parts/top_bar.php");
include_once("includes/db_include.inc.php");
include_once("includes/function.inc.php");
if(isset($_SESSION['ADMIN_LOGIN']) && $_SESSION['ADMIN_LOGIN']!=''){

}else{
	header('location:index.php');
	die();
}
if(isset($_POST['p_update'])){
	$cat_id = get_safe_value($con,$_POST['cat_id']);
	$cat_name = get_safe_value($con,$_POST['cat_name']);
	if($cat_id != ''){
		$update_status_sql="UPDATE `pt_24_categories` SET `cat_name`='$cat_name' WHERE `cat_id`='$cat_id'";
		mysqli_query($con,$update_status_sql);
	}	
}
site_header(); ?>
<body class="cbp-spmenu-push">
<style>
.btn a{
	color:#fff;
	font-weight:900;
}
.btn a:hover{
	color:#fff;
	font-weight:900;
}
</style>
	<div class="main-content">
		<!-- Navigation -->
		<?php bottom_menu(); ?>
		<!-- header-starts -->
		<?php top_bar(); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<div class="forms">
					<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
						<div class="form-title">
							<h4>Update Category:</h4>
						</div>
						<div class="form-body">
							<form action="" method="post"> 
							<?php
							$id=get_safe_value($con,$_GET['id']);
							$res=mysqli_query($con,"select * from `pt_24_categories` where `cat_id`='$id'");
							$check=mysqli_num_rows($res);
							if($check>0){
								$row=mysqli_fetch_assoc($res);
								$categories=$row['cat_name'];
							}else{
								header('location:categories.php');
								die();
							}
							?>
							<div class="form-group row"> 
							<div class="col-md-2"> 
							<label for="exampleInputEmail1">Category Name:</label> </div><div class="col-md-10"> 
							<input type="text" name="cat_name" class="form-control" id="exampleInputEmail1" value="<?php echo $categories ?>"> 
							<input type="hidden" name="cat_id" value="<?php echo $_GET['id']; ?>">
							</br>
							<input type="submit" name="p_update" Value="Update" class="btn btn-success" onclick="return confirm('Are you sure you want to Update this Category?');"></div></div></form> 
						</div>
					</div>
				</div>
			</div>
		</div>
<?php echo site_footer(); ?>
</body>
</html>