<?php
include_once("template-parts/header.php");
include_once("includes/db_include.inc.php");
include_once("includes/function.inc.php");
$msg='';
if(isset($_POST["login_p"])){
$email = get_safe_value($con,$_POST["email"]);	
$password = get_safe_value($con,$_POST["password"]);
$securing = password_hash($password, PASSWORD_DEFAULT);
if(!empty($email) && !empty($password)){
$checkavailable = "SELECT * FROM `pt_admin_login` WHERE `user_email` = '$email'"; // Query to cross check Company name with database.
                $result = mysqli_query($con, $checkavailable);
                /* If username or email is taken */
                if ($result->num_rows != 0) {
				    $msg = "Email ALready Exist";		
				}else{
					$sql = "INSERT INTO `pt_admin_login`(`t_id`, `user_email`, `user_pass`) VALUES ('','$email','$securing')";
                    $result = mysqli_query($con, $sql);
					$_SESSION['REG_SUCC']= "REG_SUCC";
                    /* If registration is successful return user to registration.php and promt user success pop-up. */
                    header('Location: index.php');
                    exit();
				}
}
}
site_header(); ?> 
<body class="cbp-spmenu-push">
<style>
.error{
	color:red;
	font-weight:700;
	margin-top:20px;
	text-align:center;
}
</style>
<div class="main-content">
		<!-- main content start-->
		<div id="">
			<div class="main-page login-page ">
				<h2 class="title1">Register</h2>
				<div class="widget-shadow">
					<div class="login-body">
						<form action="" method="post">
							<input type="email" class="user" name="email" placeholder="Enter Your Email" required="">
							<input type="password" name="password" class="lock" placeholder="Password" required="">
							<div class="forgot-grid">
								<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Remember me</label>
								<div class="clearfix"> </div>
							</div>
							<input type="submit" name="login_p" value="Register">
						</form>
						<div class="error"><?php echo $msg ?></div>
					</div>
				</div>
			</div>
		</div>
		<!--footer-->
		<div class="footer">
		   <p>&copy; <?php echo date("Y"); ?> Powertrac Corporation</p></div>
        <!--//footer-->
	</div>
	
	<!-- side nav js -->
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
		
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.js"> </script>
	<!-- //Bootstrap Core JavaScript -->
   
</body>
</html>