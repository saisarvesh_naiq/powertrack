<?php function bottom_menu() { ?>
<style>
.navbar .navbar-collapse .sidebar-menu li a{
	
	font-weight:600;
	color:#fff;
}
.sidebar-left{
	overflow-x: scroll;
}
</style>
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
		<!--left-fixed -navigation-->
	<aside class="sidebar-left">
      <nav class="navbar navbar-inverse">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="power_white.png" height="60px" width="160px"></img></a>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="sidebar-menu">
              <li class="treeview">
                <a href="index.php">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
              </li>
			  <li class="treeview">
                <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Products</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-angle-right"></i> View</a></li>
                  <li><a href="#"><i class="fa fa-angle-right"></i> Insert</a></li>
				  <li><a href="#"><i class="fa fa-angle-right"></i> Update</a></li>
				  <li><a href="#"><i class="fa fa-angle-right"></i> Delete</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="charts.html">
                <i class="fa fa-pie-chart"></i>
                <span>New Orders</span>
                <span class="label label-primary pull-right">0</span>
                </a>
              </li>
			  <li class="treeview">
                <a href="charts.html">
                <i class="fa fa-pie-chart"></i>
                <span>Completed Orders</span>
                <span class="label label-primary pull-right">0</span>
                </a>
              </li>
			  <li class="treeview">
                <a href="#">
                <i class="fa fa-dashboard"></i> <span>Invoice</span>
                </a>
              </li>
			  <li class="treeview">
                <a href="#">
                <i class="fa fa-edit"></i> <span>Users</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-angle-right"></i> Registered Users</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                <i class="fa fa-envelope"></i> <span>Mailbox</span>
                <i class="fa fa-angle-left pull-right"></i><small class="label pull-right label-info1">08</small><span class="label label-primary1 pull-right">02</span></a>
                <ul class="treeview-menu">
                  <li><a href="inbox.html"><i class="fa fa-angle-right"></i>Feedback Mails <small class="label label-primary1 pull-right">02</small></a></li>
                  <li><a href="compose.html"><i class="fa fa-angle-right"></i>Contact Mails <small class="label pull-right label-info1">08</small></a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                <i class="fa fa-folder"></i> <span>Register New User</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-angle-right"></i> Admin User</a></li>
                  <li><a href="#"><i class="fa fa-angle-right"></i> Front-end User</a></li>
                </ul>
              </li>
              <li class="header">Site Setting</li>
               <li class="treeview">
                <a href="#">
                <i class="fa fa-folder"></i> <span>SEO Setting</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-angle-right"></i> Site Info</a></li>
                  <li><a href="#"><i class="fa fa-angle-right"></i> Site Tags</a></li>
                </ul>
              </li>
			  <li class="treeview">
                <a href="#">
                <i class="fa fa-folder"></i> <span>Site info Setting</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-angle-right"></i> Change Email Address</a></li>
                  <li><a href="#"><i class="fa fa-angle-right"></i> Change Contact Info</a></li>
                </ul>
              </li>
			  <li class="header">Blog Setting</li>
			  <li class="treeview">
                <a href="#">
                <i class="fa fa-folder"></i> <span>Blogs</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-angle-right"></i> Write Blog</a></li>
                  <li><a href="#"><i class="fa fa-angle-right"></i> Update Blog</a></li>
				  <li><a href="#"><i class="fa fa-angle-right"></i> Delete Blog</a></li>
                </ul>
              </li>
            </ul>
          </div>
          <!-- /.navbar-collapse -->
      </nav>
    </aside>
	</div>



<?php } ?>