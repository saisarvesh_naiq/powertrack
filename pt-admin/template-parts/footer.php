<?php function site_footer() { ?>
     <div class="footer">
		   <p>&copy; <?php echo date("Y"); ?> Powertrac Corporation</p></div>
		 </div>
		
	<!-- new added graphs chart js-->
	
    <script src="js/Chart.bundle.js"></script>
    <script src="js/utils.js"></script>
	<script src="js/scripts.js"></script>
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.js"> </script>
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>



<?php } ?>