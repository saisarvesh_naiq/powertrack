<?php
include_once("template-parts/header.php");
include_once("includes/connect.php");

// $msg='';
// if(isset($_POST["login_p"])){
// $email = get_safe_value($con,$_POST["email"]);	
// $password = get_safe_value($con,$_POST["password"]);
// if(!empty($email) && !empty($password)){
// $sql = "SELECT * FROM `pt_admin_login` WHERE `user_email` = '$email'";
// $result = mysqli_query($con, $sql);
//             if ($result->num_rows === 1) {
// 				$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
//                 if (password_verify($password, $row['user_pass'])) {      
//                     $_SESSION['ADMIN_LOGIN'] = $email;  
// 					header('location:home.php');
// 					die();					
//                 } else {
//                     $msg="Please enter correct login details";
//                 } 
// 			}else{
//                     $msg="Please Enter Correct Login Details";
//             } 
// 			}else{
// 		$msg = "Please Enter All Fields";	
// 	}
// }



site_header(); ?> 
<body class="cbp-spmenu-push">
<style>
body {
  background-image: url("bg_p.jpg");
  background-repeat: no-repeat;
  background-color: #cccccc;
   background-position: center; /* Center the image */
  background-size: cover; /* Resize the background image to cover the entire container */
  
}
.error{
	color:red;
	font-weight:700;
	margin-top:20px;
	text-align:center;
}
.error_2{
	color:red;
	font-weight:700;
	margin-top:20px;
	margin-bottom:10px;
	text-align:center;
}
</style>
<div class="main-content">
		<!-- main content start-->
		<div id="">
			<div class="main-page login-page ">
				<h2 class="title1">Login</h2>
				<div class="widget-shadow">
					<div class="login-body">
						<form action="" method="post">
							<input type="email" class="user" name="email" placeholder="Enter Your Email" required="" id="admin_email">
							<input type="password" name="password" class="lock" placeholder="Password" required="" id="admin_password">
							<div class="forgot-grid">
								<label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i></i>Remember me</label>
								<div class="clearfix"> </div>
							</div>
							<input type="submit" name="login_p" value="Sign In" id="admin_login_btn">
						</form>
						<div class="error"></div>
					</div>
				</div>
			</div>
		</div>
		<!--footer-->
		<div class="footer">
		   <p>&copy; <?php echo date("Y"); ?> Powertrac Corporation</p></div>
        <!--//footer-->
	</div>


    <script>


$("#admin_login_btn").click(function(){

  var admin_email=$("#admin_email").val();
  var admin_password=$("#admin_password").val();


  var datastr='&admin_email='+admin_email+'&admin_password='+admin_password;


 if (admin_email=='') {
  alert("Please enter email");
}
else if (admin_password=='') {
  alert("Please enter your password");
}

else{

 $.ajax({


  type: "POST",
  url: "admin_login_GET.php",
  data: datastr,
  cache: false,
  success: function(res){
    if(res==1)
    {
      alert("Admin has logged in!");
      window.location.href ="home.php";
    }
    else{


     swal("This account does not exist!");
      

   }

 }
}); 
}

});

</script>















	
	<!-- side nav js -->
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
		
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.js"> </script>
	<!-- //Bootstrap Core JavaScript -->
   
</body>
</html>