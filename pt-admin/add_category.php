<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/main_header.php");
include_once("template-parts/top_bar.php");
include_once("includes/db_include.inc.php");
include_once("includes/function.inc.php");
if(isset($_SESSION['ADMIN_LOGIN']) && $_SESSION['ADMIN_LOGIN']!=''){

}else{
	header('location:index.php');
	die();
}
site_header();  ?>
<body class="cbp-spmenu-push">
<style>
.btn a{
	color:#fff;
	font-weight:900;
}
.btn a:hover{
	color:#fff;
	font-weight:900;
}
</style>
	<div class="main-content">
		<!-- Navigation -->
		<?php bottom_menu(); ?>
		<!-- header-starts -->
		<?php top_bar(); ?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<div class="forms">
					<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
						<div class="form-title">
							<h4>Add Main Category:</h4>
						</div>
						<div class="form-body">
							<form action="" method="post"> 
							<div class="form-group row"> 
							<div class="col-md-2"> 
							<label for="exampleInputEmail1">Category Name:</label> </div><div class="col-md-10"> 
							<input type="text" name="cat_name" class="form-control" id="exampleInputEmail1" placeholder="Enter Name of Main Category"> 
							</br>
							<input type="submit" name="add_main" Value="Add Main Category" class="btn btn-success" onclick="return confirm('Are you sure you want to Update this Category?');"></div></div></form> 
						</div>
					</div>
				</div>
			</div>
			<div class="main-page">
				<div class="forms">
					<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
						<div class="form-title">
							<h4>Add Sub Category:</h4>
						</div>
						<div class="form-body">
							<form action="" method="post"> 
							<div class="form-group row"> 
							<div class="col-md-12">
							<div class="col-md-4">Choose Main Category:</div>
							
							<div class="col-md-8"><select name="main_cat" class="form-control" required><option value="">Please Select Main Category</option>
							  <?php 
							  $sql="SELECT `cat_id`, `cat_name`, `cat_status` FROM `pt_24_categories` order by `cat_name` asc";
					          $res=mysqli_query($con,$sql); 
							  while($row=mysqli_fetch_assoc($res)){
							  ?> <option value="<?php echo $row["cat_id"]; ?>"><?php echo $row["cat_name"]; ?></option><?php } ?></select></div>
							</div><div class="col-md-12"><div class="col-md-4"> 
							</br><label for="exampleInputEmail1">Enter Sub Category:</label> </div><div class="col-md-8"> 
							</br><input type="text" name="sub_cat" class="form-control" id="exampleInputEmail1" placeholder="Enter Sub Category" required> 
							</br>
							<input type="submit" name="add_sub" Value="Add Sub Category" class="btn btn-success"></div></div></div></form> 
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
		
		if(isset($_POST['add_sub'])){
	$main_cat = get_safe_value($con,$_POST['main_cat']);
	$sub_cat = get_safe_value($con,$_POST['sub_cat']);
	if($main_cat != '' || $sub_cat !=''){
		$update_status_sql="INSERT INTO `p_24_sub_categories`(`cat_id`, `main_cat_id`, `sub_cat_name`, `status`) VALUES ('','$main_cat','$sub_cat','1')";
		$succ = mysqli_query($con,$update_status_sql);
		if($succ){ ?>
			<script>swal("Success!", "New Category Added Successfully", "error");</script>
	<?php	}else{?>
		<script>swal("Error!", "Sorry Something Went Wrong", "error");</script><?php
	}
	}	
}

?>
<?php echo site_footer(); ?>
</body>
</html>