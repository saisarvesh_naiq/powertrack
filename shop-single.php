<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/site_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
<style>
.container-outer img{
margin-top:-55px;
height:100px;
}
</style>
    <div class="page-wrapper">
		<!-- Header Here -->
		<?php bottom_menu(); ?>
		<?php site_top_menu(); ?>
		<?php slide_slider(); ?> 

<!--Start breadcrumb area-->     
<section class="breadcrumb-area" style="background-image: url(css/images/resources/breadcrumb-bg.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title float-left">
                       <h1>Single Product</h1>
                    </div>
                    <div class="breadcrumb-menu float-right">
                        <ul class="clearfix">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Single Product</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>
<!--End breadcrumb area-->

<!--Start shop area-->
<section id="shop-area" class="single-shop-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="shop-content">
                    <!--Start single shop content-->
                    <div class="single-shop-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="single-product-image-holder">
                                    <img src="css/images/shop/single-product.jpg" alt="Awesome Image">
                                </div>
                            </div> 
                            <div class="col-lg-6">
                                <div class="content-box">
                                    <span class="price">&#8377;2900.00</span>
                                    <h2>Solar Panel</h2>
                                    <div class="review-box">
                                        <ul>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star"></i></li>
                                            <li><i class="fa fa-star-half"></i></li>
                                        </ul>
                                    </div>
                                    <div class="text">
                                        <p>Luminous red charge 18000tt description luminous tall tubular batteries have unique patented alloy composition, which protects the lead part of battery from corrosion. ... These batteries not only charge faster, they last much longer as well. This reduces electricity consumption, which in turn increases financial savings.</p>
                                    </div>
                                    <div class="location-box">
                                        <p>Check Delivery Option at Your Location:</p>
                                        <form action="#">
                                            <input type="text" placeholder="Pincode">
                                            <button type="submit">Check</button><br/>
                                        </form>
                                        <span>Expected Delivery in 4-10 Days</span>
                                    </div>
                                    <div class="addto-cart-box">
                                        <input class="quantity-spinner" type="text" value="2" name="quantity">
                                        <button class="btn-three addtocart" type="submit">Add to Cart<span class="icon-null"></span></button>
                                    </div>
                                    <div class="share-products-socials">
                                        <h5>Share This Product</h5>
                                        <ul>
                                            <li><a href="#"><i class="fa fa-facebook fb" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter tw" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-pinterest pin" aria-hidden="true"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin lin" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End single shop content-->
                    <!--Start product tab box-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="product-tab-box tabs-box">
                                <ul class="tab-btns tab-buttons clearfix">
                                    <li data-tab="#desc" class="tab-btn"><span>Descriprion</span></li>
                                    <li data-tab="#review" class="tab-btn active-btn"><span>Reviews (2)</span></li>
                                </ul>
                                <div class="tabs-content">
                                    <div class="tab" id="desc">
                                        <div class="product-details-content">
                                            <div class="desc-content-box">
                                                <p>Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences.</p>
                                                <p>Must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and of the great explorer of the truth, the master-builder of human happiness.</p>
                                            </div>
                                        </div>    
                                    </div>
                                    <div class="tab active-tab" id="review">
                                        <div class="review-box-holder">
                                            <div class="row">
                                                <!--Start single review box-->
                                                <div class="col-xl-6">
                                                    <div class="single-review-box">
                                                        <div class="icon-holder">
                                                            <span class="icon-user"></span>
                                                        </div>
                                                        <div class="text-holder">
                                                            <div class="top">
                                                                <div class="name">
                                                                    <h3>Steven Rich <span>– September 17, 2018:</span></h3>
                                                                </div>
                                                                <div class="review-box">
                                                                    <ul>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="text">
                                                                <p>Value for money , I use it from long time and it is very useful and good product.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--End single review box-->
                                                <!--Start single review box-->
                                                <div class="col-xl-6">
                                                    <div class="single-review-box">
                                                        <div class="icon-holder">
                                                            <span class="icon-user"></span>
                                                        </div>
                                                        <div class="text-holder">
                                                            <div class="top">
                                                                <div class="name">
                                                                    <h3>William Cobus <span>– September 17, 2018:</span></h3>
                                                                </div>
                                                                <div class="review-box">
                                                                    <ul>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                        <li><i class="fa fa-star"></i></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="text">
                                                                <p>We denounce with righteous indignation and dislike men who are so beguiled & demoralized.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--End single review box-->
                                            </div>
                                        </div>
                                        <div class="review-form">
                                            <div class="title">
                                                <h3>Add Your Review</h3>
                                                <span>Your email address will not be published. Required fields are marked *</span>
                                            </div>
                                            <div class="add-rating-box">
                                                <div class="add-rating-title">
                                                    <h4>Your Rating</h4>    
                                                </div>
                                                <div class="review-box">
                                                    <ul>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <form id="review-form" action="#">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="input-box">
                                                            <input type="text" name="ffname" placeholder="First Name" required="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="input-box">
                                                            <input type="text" name="flname" placeholder="Last Name" required="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="input-box">
                                                            <input type="email" name="email" placeholder="Email" required="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="input-box">
                                                            <textarea name="review" placeholder="Your Review" required=""></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button class="btn-three" type="submit">submit<span class="icon-null"></span></button>
                                                    </div>
                                                </div>
                                            </form>  
                                        </div>
                                    </div>

                                </div>      
                            </div>
                        </div>
                    </div>
                    <!--End product tab box-->
                    <!--Start related product box-->
                    <div class="related-product">
                        <div class="title">
                            <h3>Recently Viewed</h3>
                        </div>
                        <div class="row">
                            <!--Start single product item-->
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                <div class="single-product-item text-center">
                                    <div class="img-holder">
                                        <img src="images/shop/7.jpg" alt="Awesome Product Image">
                                    </div>
                                    <div class="title-holder text-center">
                                        <div class="static-content">
                                            <h3 class="title text-center"><a href="shop-single.html">Product 1</a></h3>
                                            <div class="rate-review-box">
                                                <div class="rate-box float-left">
                                                    <p>&#8377;2900.00</p>    
                                                </div>
                                                <div class="review-box float-right">
                                                    <ul>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="overlay-content">
                                            <ul>
                                                <li><a href="shop-single.html"><span class="icon-cart"></span></a></li>
                                                <li><a href="#"><span class="icon-heart"></span></a></li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End single product item--> 
                            <!--Start single product item-->
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                <div class="single-product-item text-center">
                                    <div class="img-holder">
                                        <img src="images/shop/1.jpg" alt="Awesome Product Image">
                                    </div>
                                    <div class="title-holder text-center">
                                        <div class="static-content">
                                            <h3 class="title text-center"><a href="shop-single.html">Product 2</a></h3>
                                            <div class="rate-review-box">
                                                <div class="rate-box float-left">
                                                    <p><del>&#8377;4900.00</del> $29.00</p>    
                                                </div>
                                                <div class="review-box float-right">
                                                    <ul>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="overlay-content">
                                            <ul>
                                                <li><a href="shop-single.html"><span class="icon-cart"></span></a></li>
                                                <li><a href="#"><span class="icon-heart"></span></a></li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End single product item-->
                            <!--Start single product item-->
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                                <div class="single-product-item text-center">
                                    <div class="img-holder">
                                        <img src="images/shop/4.jpg" alt="Awesome Product Image">
                                    </div>
                                    <div class="title-holder text-center">
                                        <div class="static-content">
                                            <h3 class="title text-center"><a href="shop-single.html">Product 3</a></h3>
                                            <div class="rate-review-box">
                                                <div class="rate-box float-left">
                                                    <p>&#8377;2900.00</p>    
                                                </div>
                                                <div class="review-box float-right">
                                                    <ul>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="overlay-content">
                                            <ul>
                                                <li><a href="shop-single.html"><span class="icon-cart"></span></a></li>
                                                <li><a href="#"><span class="icon-heart"></span></a></li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--End single product item-->  
                             
                        </div>
                    </div>
                    <!--End related product box-->
                </div> 
            </div>
        
        </div>
    </div>
</section>
<!--End shop area-->

<!--Start footer area-->  
<?php site_footer(); ?>