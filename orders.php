<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/online_ups_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>

<link href="https://fonts.googleapis.com/css2?family=Secular+One&display=swap" rel="stylesheet">
<body>
<style>
.container-outer img{
margin-top:-55px;
height:100px;
}
</style>
    <div class="page-wrapper">
		<!-- Header Here -->
		<?php bottom_menu(); ?>
		<?php site_top_menu(); ?>
		<?php slide_slider(); ?>




	<div class="container"  style="padding-left: 40px;padding-right: 40px;">


   
   <div class="order_placed">
     
     <div class="row">
       <div class="col-md-12 text-center">
          
          <img src="Powertrac-Logo.png">
  
          <h1>Your Order is on its way.</h1>

          <button class="btn btn-primary track_btn"><strong>Track your order</strong></button>
          <p><strong>Please allow 24 hours to track your order.</strong></p>

          <div class="row" style="margin-top: 60px;">
            <div class="col-md-2"></div>
            <div class="col-md-4 text-left order_info">
              <p><strong>SUMMARY:</strong></p>
              <p><strong>Order #:   12345678</strong></p>
              <p><strong>Order Date: October 30 2020</strong></p>
              <p><strong>Order Total: &#8377; 29000</strong></p>
            </div>
            <div class="col-md-4 text-left order_info">
              <p><strong>SHIPPING ADDRESS:</strong></p>
              <p><strong>H.no 12345</strong></p>
              <p><strong>Carriamoddi,Curchorem</strong></p>
              <p><strong>Goa.</strong></p>
            </div>
            <div class="col-md-2"></div>
          </div>


          <div class="row" style="margin-top: 30px;">
            <div class="col-md-2"></div>
            <div class="col-md-4 text-left product_sum">
              <p style="font-size: 20px;"><strong>ITEM SHIPPED</strong></p>
                
                <div class="row" style="margin-top: 30px;">
                  <div class="col-md-3 text-center">
                    <img src="assets/images/products/flooded_genx.jpg">
                  </div>
                  <div class="col-md-9">
                    <p><strong>3 Phase Air Cooled Servo Controlled Voltage Stabilizers</strong></p>
                  </div>
                </div>

            </div>
            <div class="col-md-2 text-center product_sum">
              <p style="font-size: 20px;">QUANTITY</p>
              <p style="margin-top: 40px;"><strong>1</strong></p>
            </div>
            <div class="col-md-2 text-center product_sum">
            <p style="font-size: 20px;">PRICE</p>
            <p style="margin-top: 40px;"><strong>&#8377; 29000</strong></p>
            </div>
             <div class="col-md-2"></div>
          </div>
          <hr/>

          <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-4"></div>
            <div class="col-md-4">
              <div class="row">
                <div class="col-md-8 text-left pr_price">
                  <p style="font-size: 18px;"><strong>Subtotal(1 item):</strong></p>
                  <p style="font-size: 18px;"><strong>Shipping</strong></p>
                  <p style="font-size: 18px;"><strong>Estimated Tax</strong></p>
                  <p style="font-size: 20px;font-weight: 700;"><strong>Order Total</strong></p>
                </div>
                <div class="col-md-4 text-left">
                   <p style="font-size: 18px;"><strong>&#8377; 29000</strong></p>
                  <p style="font-size: 18px;"><strong>&#8377; 200</strong></p>
                  <p style="font-size: 18px;"><strong>&#8377; 100</strong></p>
                  <p style="font-size: 20px;font-weight: 700;"><strong>&#8377; 29300</strong></p>
                </div>
                </div>
              </div>
              <div class="col-md-2"></div>
            </div>





            <div class="row">
              <div class="col-md-2"></div>
              <div class="col-md-8" style="background-color: #008cf6;padding: 24px 112px 24px 112px;">
                <h4 style="color: #ffffff !important;"><strong>Call us at 999-999-999 or reply to this email</strong></h4>
              </div>
                 <div class="col-md-2"></div>
            </div>

            <div class="row">
              <div class="col-md-2"></div>
              <div class="col-md-8">
                <div class="row">
                  <div class="col-md-3 powertrack_service">
                    <img src="assets/images/icon/24-hours.png" class="img-fluid">
                    <p><strong>CUSTOMER<br>SERVICE</strong></p>
                  </div>
                  <div class="col-md-3 powertrack_service">
                     <img src="assets/images/icon/shipped.png" class="img-fluid">
                    <p><strong>FREE SHIPPING<br>ORDERS &#8377; 1000+</strong></p>
                  </div>
                  <div class="col-md-3 powertrack_service">
                     <img src="assets/images/icon/satisfaction.png" class="img-fluid">
                    <p><strong>SATISFACTION<br>GUARANTEED</strong></p>
                  </div>
                  <div class="col-md-3 powertrack_service">
                     <img src="assets/images/icon/exchange.png" class="img-fluid">
                    <p><strong>HASSEL-FREE<br>RETURNS</strong></p>
                  </div>
                </div>
              </div>
              <div class="col-md-2"></div>
            </div>




      
          </div>
       </div>
     </div> 

   </div>

  
      
        

	</div>
</div>






























<?php site_footer(); ?>
