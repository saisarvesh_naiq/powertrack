<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/site_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
    <style>
        .container-outer img{
            margin-top:-55px;
            height:100px;
        }
    </style>
    <div class="page-wrapper">
      <!-- Header Here -->
      <?php bottom_menu(); ?>
      <?php site_top_menu(); ?>
      <?php slide_slider(); ?>
      <!-- End Hidden Bar -->   
      <?php site_slider(); ?>




      <section>
        <div class="container2" style="padding-left: 20px;padding-right: 20px;">
            <div class="row" style="margin-top: 40px;margin-bottom: 40px;">
                <div class="col-md-2 text-center main_categories"><a href="online_ups.php">
                    <img src="assets/images/icon/online_ups.png">
                    <h3>Online UPS</h3></a>
                </div>
                <div class="col-md-2 text-center main_categories"><a href="luminous_solar_division.php">
                    <img src="assets/images/icon/solar.png">
                    <h3>Luminar Solar Division</h3></a></div>
                    <div class="col-md-2 text-center main_categories"><a href="stabilizers.php">
                        <img src="assets/images/icon/stabilizers.png">
                        <h3>Stabilizers</h3></a></div>
                        <div class="col-md-2 text-center main_categories"><a href="inverters_and_ups.php">
                            <img src="assets/images/icon/inverter.png">
                            <h3>Inverters and UPS</h3></a></div>
                            <div class="col-md-2 text-center main_categories"><a href="genset_division.php">
                                <img src="assets/images/icon/online_ups.png">
                                <h3>Genset Division</h3></a></div>
                                <div class="col-md-2 text-center main_categories"><a href="batteries.php">
                                    <img src="assets/images/icon/batteries.png">
                                    <h3>Batteries</h3></a></div>
                                </div>
                            </div>
                        </section>






                        <section class="sec-pad-with-content-margin-30 gray-bg">
                            <div class="container">
                                <div class="sec-title-four text-center">
                                    <h2>Our<span> Solutions</span></h2>
                                    <p>Frequent power failures, fluctuations and power surges not only is it a nuisance but also affects your appliances and business output. We help power you by offering a vast range of superior quality products paired with prompt services which exceed customer expectations and deliver critical business continuity.<br> Take control of your power costs today!</p>
                                </div><!-- /.sec-title-four -->
                                <div class="row">
                                    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                        <div class="single-service-style-one">
                                            <div class="image-block">
                                                <img src="css/images/home-pages/service-1-1.jpg" alt="Awesome Image"/>
                                                <div class="overlay-block"></div><!-- /.overlay-block -->
                                            </div><!-- /.image-block -->
                                            <div class="main-content">
                                                <div class="icon-block">
                                                    <div class="inner-box">
                                                        <i class="energy-icon-sea-dyke"></i>
                                                    </div><!-- /.inner-box -->
                                                </div><!-- /.icon-block -->
                                                <div class="text-block">
                                                  <!--  <span>Power Your Car with Powertrac.</span> -->
                                                   <h3><a href="#">Automobile Batteries</a></h3>
                                               </div><!-- /.text-block -->
                                           </div><!-- /.main-content -->
                                           <div class="hover-content">
                                            <p>Power Your Car with Powertrac Batteries.</p>
                                            <a href="online_ups.php" class="read-more">View Options <i class="energy-icon-right-arrow"></i></a>
                                            <i class="energy-icon-sea-dyke box-icon"></i>
                                        </div><!-- /.hover-content -->
                                    </div><!-- /.single-service-style-one -->
                                </div><!-- /.col-lg-4 -->
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                    <div class="single-service-style-one">
                                        <div class="image-block">
                                            <img src="css/images/home-pages/service-1-2.jpg" alt="Awesome Image"/>
                                            <div class="overlay-block"></div><!-- /.overlay-block -->
                                        </div><!-- /.image-block -->
                                        <div class="main-content">
                                            <div class="icon-block">
                                                <div class="inner-box">
                                                    <i class="energy-icon-verified-text-paper"></i>
                                                </div><!-- /.inner-box -->
                                            </div><!-- /.icon-block -->
                                            <div class="text-block">
                                                <!-- <span>Power Your Car with Powertrac.</span> -->
                                                <h3><a href="inverters_and_ups.php">Invertor and UPS</a></h3>
                                            </div><!-- /.text-block -->
                                        </div><!-- /.main-content -->
                                        <div class="hover-content">
                                           <p>Power Your Car with Powertrac Batteries.</p>
                                           <a href="inverters_and_ups.php" class="read-more">View Options <i class="energy-icon-right-arrow"></i></a>
                                           <i class="energy-icon-verified-text-paper box-icon"></i>
                                       </div><!-- /.hover-content -->
                                   </div><!-- /.single-service-style-one -->
                               </div><!-- /.col-lg-4 -->
                               <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                                <div class="single-service-style-one">
                                    <div class="image-block">
                                        <img src="css/images/home-pages/service-1-3.jpg" alt="Awesome Image"/>
                                        <div class="overlay-block"></div><!-- /.overlay-block -->
                                    </div><!-- /.image-block -->
                                    <div class="main-content">
                                        <div class="icon-block">
                                            <div class="inner-box">
                                                <i class="energy-icon-turbine"></i>
                                            </div><!-- /.inner-box -->
                                        </div><!-- /.icon-block -->
                                        <div class="text-block">
                                         <!-- <span>Power Your Car with Powertrac.</span> -->
                                         <h3><a href="#">Generator Sets</a></h3>
                                     </div><!-- /.text-block -->
                                 </div><!-- /.main-content -->
                                 <div class="hover-content">
                                   <p>Power Your Car with Powertrac Batteries.</p>
                                   <a href="genset_division.php" class="read-more">View Options <i class="energy-icon-right-arrow"></i></a>
                                   <i class="energy-icon-turbine box-icon"></i>
                               </div><!-- /.hover-content -->
                           </div><!-- /.single-service-style-one -->
                       </div><!-- /.col-lg-4 -->
                       <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                        <div class="single-service-style-one">
                            <div class="image-block">
                                <img src="css/images/home-pages/service-1-4.jpg" alt="Awesome Image"/>
                                <div class="overlay-block"></div><!-- /.overlay-block -->
                            </div><!-- /.image-block -->
                            <div class="main-content">
                                <div class="icon-block">
                                    <div class="inner-box">
                                        <i class="energy-icon-quality"></i>
                                    </div><!-- /.inner-box -->
                                </div><!-- /.icon-block -->
                                <div class="text-block">
                                   <!-- <span>Power Your Car with Powertrac.</span> -->
                                   <h3><a href="#">Online Ups Systems</a></h3>
                               </div><!-- /.text-block -->
                           </div><!-- /.main-content -->
                           <div class="hover-content">
                            <p>Power Your Car with Powertrac Batteries.</p>
                            <a href="online_ups.php" class="read-more">View Options <i class="energy-icon-right-arrow"></i></a>
                            <i class="energy-icon-quality box-icon"></i>
                        </div><!-- /.hover-content -->
                    </div><!-- /.single-service-style-one -->
                </div><!-- /.col-lg-4 -->
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                    <div class="single-service-style-one">
                        <div class="image-block">
                            <img src="css/images/home-pages/service-1-5.jpg" alt="Awesome Image"/>
                            <div class="overlay-block"></div><!-- /.overlay-block -->
                        </div><!-- /.image-block -->
                        <div class="main-content">
                            <div class="icon-block">
                                <div class="inner-box">
                                    <i class="energy-icon-road-split"></i>
                                </div><!-- /.inner-box -->
                            </div><!-- /.icon-block -->
                            <div class="text-block">
                             <!-- <span>Power Your Car with Powertrac.</span> -->
                             <h3><a href="#">Voltage stabilizers</a></h3>
                         </div><!-- /.text-block -->
                     </div><!-- /.main-content -->
                     <div class="hover-content">
                        <p>Power Your Car with Powertrac Batteries.</p>
                        <a href="stabilizers.php" class="read-more">View Options <i class="energy-icon-right-arrow"></i></a>
                        <i class="energy-icon-road-split box-icon"></i>
                    </div><!-- /.hover-content -->
                </div><!-- /.single-service-style-one -->
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="single-service-style-one">
                    <div class="image-block">
                        <img src="css/images/home-pages/service-1-6.jpg" alt="Awesome Image"/>
                        <div class="overlay-block"></div><!-- /.overlay-block -->
                    </div><!-- /.image-block -->
                    <div class="main-content">
                        <div class="icon-block">
                            <div class="inner-box">
                                <i class="energy-icon-message"></i>
                            </div><!-- /.inner-box -->
                        </div><!-- /.icon-block -->
                        <div class="text-block">
                          <!--  <span>Power Your Car with Powertrac.</span> -->
                           <h3><a href="#">Power Transformers</a></h3>
                       </div><!-- /.text-block -->
                   </div><!-- /.main-content -->
                   <div class="hover-content">
                    <p>Power Your Car with Powertrac Batteries.</p>
                    <a href="power_transformers.php" class="read-more">View Options <i class="energy-icon-right-arrow"></i></a>
                    <i class="energy-icon-message box-icon"></i>
                </div><!-- /.hover-content -->
            </div><!-- /.single-service-style-one -->
        </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->
</div><!-- /.container -->
</section>


<section class="sec-pad-with-content-margin-30">
    <div class="container">
        <div class="sec-title-four text-center">
            <h2>Our <span>Products</span></h2>
            <p>Our product offering encompasses Electrical, Critical Power Backup, Power Storage, Power Generation Etc. <br> Take control of your power costs today!</p>
        </div><!-- /.sec-title-four -->
        <ul class="project-filter post-filter has-dynamic-filters-counter">
           <li data-filter=".filter-item" class="active"><span class="filter-text">All</span></li>
                    <li data-filter=".ups"><span class="filter-text">Online Ups</span></li><!--
                    --><li data-filter=".ldr"><span class="filter-text">LDR</span></li><!--
                    --><li data-filter=".stb"><span class="filter-text">Stabilizers</span></li><!--
                    --><li data-filter=".inv"><span class="filter-text">Inverters</span></li><!--
                    --><li data-filter=".gd"><span class="filter-text">Genset Divison</span></li><!--
                --><li data-filter=".pt"><span class="filter-text">Power Transformers</span></li>
                <li data-filter=".bat"><span class="filter-text">Batteries</span></li>
            </ul>
            <div class="row filter-layout masonary-layout">

               <?php
               require 'connect.php';
               $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='1' AND status='1'");
               $sql->execute();
               $sql->setFetchMode(PDO::FETCH_ASSOC);
               if($sql->rowCount()>0){
                foreach (($sql->fetchAll()) as $key => $row) {

                 echo '<div class="col-lg-3 col-md-6 col-sm-12 filter-item ups">
                 <div class="single-team-style-one">
                 <div class="image-block">
                 <img src="admin/products/'.$row['pro_image_1'].'" class="home_product_img" alt="awesome image">
                 </div>
                 <div class="text-block">
                 <div class="inner-block">
                 <div class="main-content">
                 <h3>'.$row['prod_name'].'</h3>
                 <span>&#8377;'.$row['prod_price'].'</span>
                 </div>
                 <div class="hover-content">
                 <div class="social">
                 <a href="single_product.php?prod_id='.$row['prod_id'].'"><i class="fa fa-shopping-cart"></i></a>
                 <a href="wishlist_GET.php?prod_id='.$row['prod_id'].'"><i class="fa fa-heart"></i></a>
                 </div>
                 </div>
                 </div>
                 </div>
                 </div>
                 </div>';

             }
         }
         else{

            echo '<h3>No products were found!</h3>';
        }
        ?>


        <?php
        require 'connect.php';
        $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='2' AND status='1'");
        $sql->execute();
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        if($sql->rowCount()>0){
            foreach (($sql->fetchAll()) as $key => $row) {

             echo '<div class="col-lg-3 col-md-6 col-sm-12 filter-item ldr">
             <div class="single-team-style-one">
             <div class="image-block">
             <img src="admin/products/'.$row['pro_image_1'].'" class="home_product_img" alt="awesome image">
             </div>
             <div class="text-block">
             <div class="inner-block">
             <div class="main-content">
             <h3>'.$row['prod_name'].'</h3>
             <span>&#8377;'.$row['prod_price'].'</span>
             </div>
             <div class="hover-content">
             <div class="social">
             <a href="single_product.php?prod_id='.$row['prod_id'].'"><i class="fa fa-shopping-cart"></i></a>
             <a href="wishlist_GET.php?prod_id='.$row['prod_id'].'"><i class="fa fa-heart"></i></a>
             </div>
             </div>
             </div>
             </div>
             </div>
             </div>';

         }
     }
     else{

        echo '<h3>No products were found!</h3>';
    }
    ?>


    <?php
    require 'connect.php';
    $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='3' AND status='1'");
    $sql->execute();
    $sql->setFetchMode(PDO::FETCH_ASSOC);
    if($sql->rowCount()>0){
        foreach (($sql->fetchAll()) as $key => $row) {

         echo '<div class="col-lg-3 col-md-6 col-sm-12 filter-item stb">
         <div class="single-team-style-one">
         <div class="image-block">
         <img src="admin/products/'.$row['pro_image_1'].'" class="home_product_img" alt="awesome image">
         </div>
         <div class="text-block">
         <div class="inner-block">
         <div class="main-content">
         <h3>'.$row['prod_name'].'</h3>
         <span>&#8377;'.$row['prod_price'].'</span>
         </div>
         <div class="hover-content">
         <div class="social">
         <a href="single_product.php?prod_id='.$row['prod_id'].'"><i class="fa fa-shopping-cart"></i></a>
         <a href="wishlist_GET.php?prod_id='.$row['prod_id'].'"><i class="fa fa-heart"></i></a>
         </div>
         </div>
         </div>
         </div>
         </div>
         </div>';

     }
 }
 else{

    echo '<h3>No products were found!</h3>';
}
?>


<?php
require 'connect.php';
$sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='4' AND status='1'");
$sql->execute();
$sql->setFetchMode(PDO::FETCH_ASSOC);
if($sql->rowCount()>0){
    foreach (($sql->fetchAll()) as $key => $row) {

     echo '<div class="col-lg-3 col-md-6 col-sm-12 filter-item inv">
     <div class="single-team-style-one">
     <div class="image-block">
     <img src="admin/products/'.$row['pro_image_1'].'" class="home_product_img" alt="awesome image">
     </div>
     <div class="text-block">
     <div class="inner-block">
     <div class="main-content">
     <h3>'.$row['prod_name'].'</h3>
     <span>&#8377;'.$row['prod_price'].'</span>
     </div>
     <div class="hover-content">
     <div class="social">
     <a href="single_product.php?prod_id='.$row['prod_id'].'"><i class="fa fa-shopping-cart"></i></a>
     <a href="wishlist_GET.php?prod_id='.$row['prod_id'].'"><i class="fa fa-heart"></i></a>
     </div>
     </div>
     </div>
     </div>
     </div>
     </div>';

 }
}
else{

    echo '<h3>No products were found!</h3>';
}
?>


<?php
require 'connect.php';
$sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='5' AND status='1'");
$sql->execute();
$sql->setFetchMode(PDO::FETCH_ASSOC);
if($sql->rowCount()>0){
    foreach (($sql->fetchAll()) as $key => $row) {

     echo '<div class="col-lg-3 col-md-6 col-sm-12 filter-item gd">
     <div class="single-team-style-one">
     <div class="image-block">
     <img src="admin/products/'.$row['pro_image_1'].'" class="home_product_img" alt="awesome image">
     </div>
     <div class="text-block">
     <div class="inner-block">
     <div class="main-content">
     <h3>'.$row['prod_name'].'</h3>
     <span>&#8377;'.$row['prod_price'].'</span>
     </div>
     <div class="hover-content">
     <div class="social">
     <a href="single_product.php?prod_id='.$row['prod_id'].'"><i class="fa fa-shopping-cart"></i></a>
     <a href="wishlist_GET.php?prod_id='.$row['prod_id'].'"><i class="fa fa-heart"></i></a>
     </div>
     </div>
     </div>
     </div>
     </div>
     </div>';

 }
}
else{

    echo '<h3>No products were found!</h3>';
}
?>


<?php
require 'connect.php';
$sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='7' AND status='1'");
$sql->execute();
$sql->setFetchMode(PDO::FETCH_ASSOC);
if($sql->rowCount()>0){
    foreach (($sql->fetchAll()) as $key => $row) {

     echo '<div class="col-lg-3 col-md-6 col-sm-12 filter-item pt">
     <div class="single-team-style-one">
     <div class="image-block">
     <img src="admin/products/'.$row['pro_image_1'].'" class="home_product_img" alt="awesome image">
     </div>
     <div class="text-block">
     <div class="inner-block">
     <div class="main-content">
     <h3>'.$row['prod_name'].'</h3>
     <span>&#8377;'.$row['prod_price'].'</span>
     </div>
     <div class="hover-content">
     <div class="social">
     <a href="single_product.php?prod_id='.$row['prod_id'].'"><i class="fa fa-shopping-cart"></i></a>
     <a href="wishlist_GET.php?prod_id='.$row['prod_id'].'"><i class="fa fa-heart"></i></a>
     </div>
     </div>
     </div>
     </div>
     </div>
     </div>';

 }
}
else{

    echo '<h3>No products were found!</h3>';
}
?>


<?php
require 'connect.php';
$sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='6' AND status='1'");
$sql->execute();
$sql->setFetchMode(PDO::FETCH_ASSOC);
if($sql->rowCount()>0){
    foreach (($sql->fetchAll()) as $key => $row) {

     echo '<div class="col-lg-3 col-md-6 col-sm-12 filter-item bat">
     <div class="single-team-style-one">
     <div class="image-block">
     <img src="admin/products/'.$row['pro_image_1'].'" class="home_product_img" alt="awesome image">
     </div>
     <div class="text-block">
     <div class="inner-block">
     <div class="main-content">
     <h3>'.$row['prod_name'].'</h3>
     <span>&#8377;'.$row['prod_price'].'</span>
     </div>
     <div class="hover-content">
     <div class="social">
     <a href="single_product.php?prod_id='.$row['prod_id'].'"><i class="fa fa-shopping-cart"></i></a>
     <a href="wishlist_GET.php?prod_id='.$row['prod_id'].'"><i class="fa fa-heart"></i></a>
     </div>
     </div>
     </div>
     </div>
     </div>
     </div>';

 }
}
else{

    echo '<h3>No products were found!</h3>';
}
?>



<!-- 					 <div class="col-lg-3 col-md-6 col-sm-12 filter-item bat">
                        <div class="single-team-style-one">
                            <div class="image-block">
                                <img src="css/images/products/product-10.jpg" alt="awesome image">
                            </div>
                            <div class="text-block">
                                <div class="inner-block">
                                    <div class="main-content">
                                         <h3>Product 2</h3>
                                        <span>&#8377; 20,000.00</span>
                                    </div>
                                    <div class="hover-content">
                                        <div class="social">
                                           <a href="#"><i class="fa fa-shopping-cart"></i></a>
                                            <a href="#"><i class="fa fa-heart"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					 <div class="col-lg-3 col-md-6 col-sm-12 filter-item ldr">
                        <div class="single-team-style-one">
                            <div class="image-block">
                                <img src="css/images/products/product-9.jpg" alt="awesome image">
                            </div>
                            <div class="text-block">
                                <div class="inner-block">
                                    <div class="main-content">
                                         <h3>Product 3</h3>
                                        <span>&#8377; 20,000.00</span>
                                    </div>
                                    <div class="hover-content">
                                        <div class="social">
                                            <a href="#"><i class="fa fa-shopping-cart"></i></a>
                                            <a href="#"><i class="fa fa-heart"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 filter-item stb">
                        <div class="single-team-style-one">
                            <div class="image-block">
                                <img src="css/images/products/product-14.jpg" alt="awesome image">
                            </div>
                            <div class="text-block">
                                <div class="inner-block">
                                    <div class="main-content">
                                        <h3>Product 4</h3>
                                        <span>&#8377; 20,000.00</span>
                                    </div>
                                    <div class="hover-content">
                                        <div class="social">
                                            <a href="#"><i class="fa fa-shopping-cart"></i></a>
                                            <a href="#"><i class="fa fa-heart"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 filter-item inv">
                        <div class="single-team-style-one">
                            <div class="image-block">
                                <img src="css/images/products/product-2.jpg" alt="awesome image">
                            </div>
                            <div class="text-block">
                                <div class="inner-block">
                                    <div class="main-content">
                                        <h3>Product 5</h3>
                                        <span>&#8377; 20,000.00</span>
                                    </div>
                                    <div class="hover-content">
                                        <div class="social">
                                            <a href="#"><i class="fa fa-shopping-cart"></i></a>
                                            <a href="#"><i class="fa fa-heart"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 filter-item gd">
                        <div class="single-team-style-one">
                            <div class="image-block">
                                <img src="css/images/products/product-9.jpg" alt="awesome image">
                            </div>
                            <div class="text-block">
                                <div class="inner-block">
                                    <div class="main-content">
                                        <h3>Product 6</h3>
                                        <span>&#8377; 20,000.00</span>
                                    </div>
                                    <div class="hover-content">
                                        <div class="social">
                                             <a href="#"><i class="fa fa-shopping-cart"></i></a>
                                            <a href="#"><i class="fa fa-heart"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="col-lg-3 col-md-6 col-sm-12 filter-item pt">
                        <div class="single-team-style-one">
                            <div class="image-block">
                                <img src="css/images/products/product-2.jpg" alt="awesome image">
                            </div>
                            <div class="text-block">
                                <div class="inner-block">
                                    <div class="main-content">
                                         <h3>Product 7</h3>
                                        <span>&#8377; 20,000.00</span>
                                    </div>
                                    <div class="hover-content">
                                        <div class="social">
                                            <a href="#"><i class="fa fa-shopping-cart"></i></a>
                                            <a href="#"><i class="fa fa-heart"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->

                </div>
                <div class="more-btn text-center">
                    <a href="shop_page.php" class="thm-btn home-four hvr-sweep-to-left">Shop<i class="energy-icon-right-arrow"></i></a>
                </div>
            </div><!-- /.container -->
        </section><!-- /.sec-pad -->
        <div class="block block--highlighted block-categories block-categories--layout--compact">
         <div class="container">
          <div class="block-header">

           <?php 
           require 'connect.php';
           $data6 = $conn->query("SELECT SUM(pro_qty) as Total6 FROM products_db WHERE main_category='1' AND status='1'")->fetchAll();
           foreach ($data6 as $row6) { 

             ?>

             <h3 class="block-header__title">Popular Categories</h3>
             <div class="block-header__divider"></div>
         </div>
         <div class="block-categories__list">
           <div class="block-categories__item category-card category-card--layout--compact">
            <div class="category-card__body">
             <div class="category-card__image"><a href="#"><img src="assets/css/images/categories/category-1.jpg" alt=""></a></div>
             <div class="category-card__content">
              <div class="category-card__name"><a href="online_ups.php">UPS</a></div>
              <div class="category-card__all"><a href="#">Show All</a></div>
              <div class="category-card__products"><?php echo $row6['Total6']; ?> Products</div>

          <?php } ?>
      </div>
  </div>
</div>

<?php 
require 'connect.php';
$data6 = $conn->query("SELECT SUM(pro_qty) as Total6 FROM products_db WHERE main_category='2' AND status='1'")->fetchAll();
foreach ($data6 as $row6) { 

 ?>

 <div class="block-categories__item category-card category-card--layout--compact">
    <div class="category-card__body">
     <div class="category-card__image"><a href="#"><img src="assets/css/images/categories/category-2.jpg" alt=""></a></div>
     <div class="category-card__content">
      <div class="category-card__name"><a href="luminous_solar_division.php">SOLAR DIVISION</a></div>
      <div class="category-card__all"><a href="#">Show All</a></div>
      <div class="category-card__products"><?php echo $row6['Total6']; ?> Products</div>

  <?php } ?>

</div>
</div>
</div>


<?php 
require 'connect.php';
$data6 = $conn->query("SELECT SUM(pro_qty) as Total6 FROM products_db WHERE main_category='3' AND status='1'")->fetchAll();
foreach ($data6 as $row6) { 

 ?>                     
 <div class="block-categories__item category-card category-card--layout--compact">
    <div class="category-card__body">
     <div class="category-card__image"><a href="#"><img src="assets/css/images/categories/category-4.jpg" alt=""></a></div>
     <div class="category-card__content">
      <div class="category-card__name"><a href="stabilizers.php">STABILIZERS</a></div>
      <div class="category-card__all"><a href="#">Show All</a></div>
      <div class="category-card__products"><?php echo $row6['Total6']; ?> Products</div>
  <?php } ?>
</div>
</div>
</div>

<?php 
require 'connect.php';
$data6 = $conn->query("SELECT SUM(pro_qty) as Total6 FROM products_db WHERE main_category='4' AND status='1'")->fetchAll();
foreach ($data6 as $row6) { 

 ?>                    
 <div class="block-categories__item category-card category-card--layout--compact">
    <div class="category-card__body">
     <div class="category-card__image"><a href="#"><img src="assets/css/images/categories/category-3.jpg" alt=""></a></div>
     <div class="category-card__content">
      <div class="category-card__name"><a href="inverters_and_ups.php">INVERTERS</a></div>
      <div class="category-card__all"><a href="#">Show All</a></div>
      <div class="category-card__products"><?php echo $row6['Total6']; ?> Products</div>
  <?php } ?>
</div>
</div>
</div>

<?php 
require 'connect.php';
$data6 = $conn->query("SELECT SUM(pro_qty) as Total6 FROM products_db WHERE main_category='7' AND status='1'")->fetchAll();
foreach ($data6 as $row6) { 

 ?> 
<div class="block-categories__item category-card category-card--layout--compact">
    <div class="category-card__body">
     <div class="category-card__image"><a href="#"><img src="assets/css/images/categories/category-5.jpg" alt=""></a></div>
     <div class="category-card__content">
      <div class="category-card__name"><a href="#">TRANSFORMERS</a></div>
      <div class="category-card__all"><a href="#">Show All</a></div>
      <div class="category-card__products"><?php echo $row6['Total6']; ?> Products</div>
  <?php } ?>      
  </div>
</div>
</div>

<?php 
require 'connect.php';
$data6 = $conn->query("SELECT SUM(pro_qty) as Total6 FROM products_db WHERE main_category='8' AND status='1'")->fetchAll();
foreach ($data6 as $row6) { 

 ?> 
<div class="block-categories__item category-card category-card--layout--compact">
    <div class="category-card__body">
     <div class="category-card__image"><a href="#"><img src="assets/css/images/categories/category-6.jpg" alt=""></a></div>
     <div class="category-card__content">
      <div class="category-card__name"><a href="#">BATTERIES</a></div>
      <div class="category-card__all"><a href="#">Show All</a></div>
      <div class="category-card__products"><?php echo $row6['Total6']; ?> Products</div>
  <?php } ?>  

  </div>
</div>
</div>
</div>
</div>
</div>

<section class="easy-steps gray-bg home-four">
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-lg-6 d-flex overlayed-background" style="background-image: url(new/solartech-demo/css/images/home-pages/easy-step-bg1.jpg);">
                <div class="title-block my-auto text-center">
                    <a href="" class="video-popup hvr-pulse-grow"><i class="fa fa-play"></i></a>
                    <h3>Video About Our Working Process</h3>
                </div><!-- /.title-block -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6">
                <div class="content-box">
                    <div class="single-content-box">
                        <h3>Consult <br> Our Expert Team</h3>
                        <p>Perfectly simple and easy to distinguish in a free hour, when our power off choice isuntrammelled and when nothing perfect.</p>
                    </div><!-- /.single-content-box -->
                    <div class="single-content-box">
                        <h3>Ordering <br> Your Equipments</h3>
                        <p>Perfectly simple and easy to distinguish in a free hour, when our power off choice isuntrammelled and when nothing perfect.</p>
                    </div><!-- /.single-content-box -->
                    <div class="single-content-box">
                        <h3>Get Installed Your <br>Product</h3>
                        <p>Perfectly simple and easy to distinguish in a free hour, when our power off choice isuntrammelled and when nothing perfect.</p>
                    </div><!-- /.single-content-box -->
                    <div class="easy-steps-count-box">
                        <div class="single-count-box">
                            <div class="count-text">01</div><!-- /.count-text -->
                            <i class="energy-icon-manager"></i>
                        </div><!-- /.single-count-box -->
                        <div class="single-count-box">
                            <div class="count-text">02</div><!-- /.count-text -->
                            <i class="energy-icon-order"></i>
                        </div><!-- /.single-count-box -->
                        <div class="single-count-box">
                            <div class="count-text">03</div><!-- /.count-text -->
                            <i class="energy-icon-mill"></i>
                        </div><!-- /.single-count-box -->
                    </div><!-- /.easy-steps-count-box -->
                </div><!-- /.content-box -->                        
            </div><!-- /.col-lg-6 -->
        </div><!-- /.row no-gutters -->
    </div><!-- /.container -->
</section><!-- /.easy-steps -->





<div class="brand-wrapper gradient-bg">
    <div class="container">
        <div class="brand-carousel-one owl-theme owl-carousel">
            <div class="item">
                <img src="css/images/home-pages/brand-1-1.png" alt="">
            </div><!-- /.item -->
            <div class="item">
                <img src="css/images/home-pages/brand-1-2.png" alt="">
            </div><!-- /.item -->
            <div class="item">
                <img src="css/images/home-pages/brand-1-3.png" alt="">
            </div><!-- /.item -->
            <div class="item">
                <img src="css/images/home-pages/brand-1-4.png" alt="">
            </div><!-- /.item -->
            <div class="item">
                <img src="css/images/home-pages/brand-1-5.png" alt="">
            </div><!-- /.item -->
            <div class="item">
                <img src="css/images/home-pages/brand-1-1.png" alt="">
            </div><!-- /.item -->
            <div class="item">
                <img src="css/images/home-pages/brand-1-2.png" alt="">
            </div><!-- /.item -->
            <div class="item">
                <img src="css/images/home-pages/brand-1-3.png" alt="">
            </div><!-- /.item -->
            <div class="item">
                <img src="css/images/home-pages/brand-1-4.png" alt="">
            </div><!-- /.item -->
            <div class="item">
                <img src="css/images/home-pages/brand-1-5.png" alt="">
            </div><!-- /.item -->
            <div class="item">
                <img src="css/images/home-pages/brand-1-1.png" alt="">
            </div><!-- /.item -->
            <div class="item">
                <img src="css/images/home-pages/brand-1-2.png" alt="">
            </div><!-- /.item -->
            <div class="item">
                <img src="css/images/home-pages/brand-1-3.png" alt="">
            </div><!-- /.item -->
            <div class="item">
                <img src="css/images/home-pages/brand-1-4.png" alt="">
            </div><!-- /.item -->
            <div class="item">
                <img src="css/images/home-pages/brand-1-5.png" alt="">
            </div><!-- /.item -->
        </div><!-- /.brand-carousel-one owl-theme owl-carousel -->
    </div><!-- /.container -->
</div><!-- /.brand-wrapper -->
</div>
<?php site_footer(); ?>