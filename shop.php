<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/site_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
<style>
.container-outer img{
margin-top:-55px;
height:100px;
}
.static-content h3{
	font-size:1.6em!important;
	
}
.rate-review-box .rate-box p{
	text-align:center!important;
	font-size:1.2em!important;
}
.rate-review-box .review-box ul li i{
	text-align:center!important;
		font-size:0.9em!important;
}
</style>
    <div class="page-wrapper">
		<!-- Header Here -->
		<?php bottom_menu(); ?>
		<?php site_top_menu(); ?>
		<?php slide_slider(); ?>                 
<!--End mainmenu area-->    

<!--Start breadcrumb area-->     
<section class="breadcrumb-area" style="background-image: url(css/images/resources/breadcrumb-bg.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title float-left">
                       <h1>Our Products</h1>
                    </div>
                    <div class="breadcrumb-menu float-right">
                        <ul class="clearfix">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Our Products</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>
<!--End breadcrumb area-->

<!--Start Shop area-->
<section id="shop-area" class="main-shop-area">
    <div class="container">
        <div class="row">
            <!--Start sidebar Wrapper-->
            <div class="col-xl-4 col-lg-4 col-md-8 col-sm-12 float-left">
                <div class="shop-sidebar-wrapper">
                    <div class="single-sidebar-box">
                        <div class="shop-sidebar-title">
                            <h3>Filter by Price</h3>
                        </div>
                        <div class="price-ranger">
                            <div id="slider-range"></div>
                            <div class="ranger-min-max-block">
                                <input type="submit" value="Filter">
                                <span>Price:</span>
                                <input type="text" readonly class="min"> 
                                <span>-</span>
                                <input type="text" readonly class="max">
                            </div>
                        </div>
                    </div>
					                    <div class="single-sidebar-box pdbtm">
                        <div class="shop-sidebar-title">
                            <h3>Other Products</h3>
                        </div>
                        <ul class="categories clearfix">
                            <li><a href="#">LUMINUOUS SOLAR DIVISION</a></li>
                            <li><a href="#">STABILIZERS</a></li>
                            <li><a href="#">INVERTERS & UPS</a></li>
                            <li><a href="#">GENSET DIVISION</a></li>
                            <li><a href="#">BATTERIES</a></li>
                            <li><a href="#">POWER TRANSFROMER</a></li>
                        </ul>
                    </div>
                    <!--End single sidebar-->
                </div>    
            </div>
            <!--End Sidebar Wrapper-->  
            <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 float-right">
                <div class="shop-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="showing-result-shorting">
                                <div class="shorting">
                                    <select class="selectmenu">
                                        <option selected="selected">Default Sorting</option>
                                        <option>Default Sorting one</option>
                                        <option>Default Sorting Two</option>
                                        <option>Default Sorting Three</option>
                                    </select>
                                </div>
                                <div class="showing">
                                    <p>Showing 1-9 of 35 results</p>
                                </div> 
                            </div>     
                        </div>
                    </div>
                    <div class="row">
                        <!--Start single product item-->
                        <div class="col-md-4 col-sm-12">
                            <div class="single-product-item text-center">
                                <div class="img-holder">
                                    <img src="product-15.jpg" alt="Awesome Product Image">
                                </div>
                                <div class="title-holder text-center">
                                    <div class="static-content">
                                        <h3 class="title text-center"><a href="shop-single.php">Luminous Batteries</a></h3>
                                        <div class="rate-review-box">
                                            <div class="rate-box float-center">
                                                <p>&#8377; 29,000.00</p>    
                                            </div>
                                            
                                        </div>
										<div class="review-box float-center">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                        </div>
                                    </div>
                                    <div class="overlay-content">
                                        <ul>
                                            <li><a href="shop-single.php"><span class="icon-cart"></span></a></li>
                                            <li><a href="#"><span class="icon-heart"></span></a></li>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End single product item-->
                        <!--Start single product item-->
                        <div class="col-md-4 col-sm-12">
                            <div class="single-product-item text-center">
                                <div class="img-holder">
                                    <img src="product-15.jpg" alt="Awesome Product Image">
                                </div>
                                <div class="title-holder text-center">
                                    <div class="static-content">
                                        <h3 class="title text-center"><a href="shop-single.php">Luminous Batteries</a></h3>
                                        <div class="rate-review-box">
                                            <div class="rate-box">
                                                <p>&#8377; 45,000.00</p>    
                                            </div>
                                        </div>
										    <div class="review-box">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                    </div>
                                    <div class="overlay-content">
                                        <ul>
                                            <li><a href="shop-single.php"><span class="icon-cart"></span></a></li>
                                            <li><a href="#"><span class="icon-heart"></span></a></li>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End single product item-->
                        
                        <!--Start single product item-->
                        <div class="col-md-4 col-sm-12">
                            <div class="single-product-item text-center">
                                <div class="img-holder">
                                    <img src="product-15.jpg" alt="Awesome Product Image">
                                </div>
                                <div class="title-holder text-center">
                                    <div class="static-content">
                                        <h3 class="title text-center"><a href="shop-single.php">Luminous Batteries</a></h3>
                                        <div class="rate-review-box">
                                            <div class="rate-box">
                                                <p>&#8377; 24,000.00</p>    
                                            </div>
                                        </div>
										     <div class="review-box">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                    </div>
                                    <div class="overlay-content">
                                        <ul>
                                            <li><a href="shop-single.php"><span class="icon-cart"></span></a></li>
                                            <li><a href="#"><span class="icon-heart"></span></a></li>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End single product item-->
                        <!--Start single product item-->
                        <div class="col-md-4 col-sm-12">
                            <div class="single-product-item text-center">
                                <div class="img-holder">
                                    <img src="product-15.jpg" alt="Awesome Product Image">
                                </div>
                                <div class="title-holder text-center">
                                    <div class="static-content">
                                        <h3 class="title text-center"><a href="shop-single.php">Luminous Batteries</a></h3>
                                        <div class="rate-review-box">
                                            <div class="rate-box">
                                                <p>&#8377; 29,000.00</p>    
                                            </div>
                                          
                                        </div>
										  <div class="review-box">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                    </div>
                                    <div class="overlay-content">
                                        <ul>
                                            <li><a href="shop-single.php"><span class="icon-cart"></span></a></li>
                                            <li><a href="#"><span class="icon-heart"></span></a></li>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End single product item-->
                        
                        <!--Start single product item-->
                        <div class="col-md-4 col-sm-12">
                            <div class="single-product-item text-center">
                                <div class="img-holder">
                                    <img src="product-15.jpg" alt="Awesome Product Image">
                                </div>
                                <div class="title-holder text-center">
                                    <div class="static-content">
                                        <h3 class="title text-center"><a href="shop-single.php">Luminous Batteries</a></h3>
                                        <div class="rate-review-box">
                                            <div class="rate-box">
                                                <p>&#8377;34,0000.00</p>    
                                            </div>
                                           
                                        </div>
										 <div class="review-box">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                    </div>
                                    <div class="overlay-content">
                                        <ul>
                                            <li><a href="shop-single.php"><span class="icon-cart"></span></a></li>
                                            <li><a href="#"><span class="icon-heart"></span></a></li>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End single product item-->
                        <!--Start single product item-->
                        <div class="col-md-4 col-sm-12">
                            <div class="single-product-item text-center">
                                <div class="img-holder">
                                    <img src="product-15.jpg" alt="Awesome Product Image">
                                </div>
                                <div class="title-holder text-center">
                                    <div class="static-content">
                                        <h3 class="title text-center"><a href="shop-single.php">Luminous Batteries</a></h3>
                                        <div class="rate-review-box">
                                            <div class="rate-box">
                                                <p>&#8377;24,000.00</p>   
                                            </div>

                                        </div>
										    <div class="review-box">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                    </div>
                                    <div class="overlay-content">
                                        <ul>
                                            <li><a href="shop-single.php"><span class="icon-cart"></span></a></li>
                                            <li><a href="#"><span class="icon-heart"></span></a></li>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End single product item-->
                        
                        <!--Start single product item-->
                        <div class="col-md-4 col-sm-12">
                            <div class="single-product-item text-center">
                                <div class="img-holder">
                                    <img src="product-15.jpg" alt="Awesome Product Image">
                                </div>
                                <div class="title-holder text-center">
                                    <div class="static-content">
                                        <h3 class="title text-center"><a href="shop-single.php">Luminous Batteries</a></h3>
                                        <div class="rate-review-box">
                                            <div class="rate-box">
                                                <p>&#8377;29,000.00</p>    
                                            </div>
                                        </div>
										    <div class="review-box">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                    </div>
                                    <div class="overlay-content">
                                        <ul>
                                            <li><a href="shop-single.php"><span class="icon-cart"></span></a></li>
                                            <li><a href="#"><span class="icon-heart"></span></a></li>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End single product item-->
                        <!--Start single product item-->
                        <div class="col-md-4 col-sm-12">
                            <div class="single-product-item text-center">
                                <div class="img-holder">
                                    <img src="product-15.jpg" alt="Awesome Product Image">
                                </div>
                                <div class="title-holder text-center">
                                    <div class="static-content">
                                        <h3 class="title text-center"><a href="shop-single.php">Luminous Batteries</a></h3>
                                        <div class="rate-review-box">
                                            <div class="rate-box">
                                                <p>&#8377;24,000.00</p>   
                                            </div>
                                        </div>
										    <div class="review-box">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                    </div>
                                    <div class="overlay-content">
                                        <ul>
                                            <li><a href="shop-single.php"><span class="icon-cart"></span></a></li>
                                            <li><a href="#"><span class="icon-heart"></span></a></li>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End single product item-->
                       
                    </div>
                    <!--Start post pagination-->
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="post-pagination mainshop text-center">
                                <li><a href="#"><i class="fa fa-caret-left" aria-hidden="true"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <!--End post pagination-->
                </div>
            </div>
            
        </div>
    </div>
</section> 
<!--End Shop area-->  

<!--Start footer area-->  
<?php site_footer(); ?>