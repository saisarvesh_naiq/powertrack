<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/stabilizers_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
<style>
.container-outer img{
margin-top:-55px;
height:100px;
}
</style>
    <div class="page-wrapper">
		<!-- Header Here -->
		<?php bottom_menu(); ?>
		<?php site_top_menu(); ?>
		<?php slide_slider(); ?>


    <div style="margin-top: 170px;">
    	<?php stabilizers_slider(); ?>
    </div>
				

	<div class="container">
	   

	   <div class="product_intro">
	   	<div class="row">
	   		<div class="col-md-12 text-center">
	   				   	<h1>Stabilizers</h1>
	   				   	<p>Provides perfectly stable output even under severe conditions of unbalanced voltage conditions. Ideal to protect the electrical and electronic equipments from high and low voltage. Individual phase control and bypass protection. It is a custom built item which can be manufactured with required dimensions. Single stabilizer would serve all the domestic needs. Quick servicing and maintenance free. Suitable for 3 phases and single phase applications. It is a power saving device and reduces excess power consumption.</p>
	   		</div>
	   	</div>
	   </div>


<!--        <div class="stabilizer_filter">
           <div class="row">
               <div class="col-md-12 text-center">
                   <div class="row">

                       <div class="col-md-3">                          
                           <div class="row">
                             <div class="col-md-2"></div>
                             <div class="col-md-4">
                               <img src="assets/images/products/ac_stabilizer.png">
                             </div>
                             <div class="col-md-4">
                               <h1>AC Stabilizer</h1>
                             </div>
                             <div class="col-md-2"></div>
                           </div>  

                            <div class="row">
                               <div class="col-md-4"></div>
                               <div class="col-md-8">
                           <p style="margin-top: 20px;"><a href="">Upto 1.5 Ton</a></p>
                           <p><a href=""> Upto 2 Ton</a></p>
       

                               </div>
                           </div>         
                       </div>


                       <div class="col-md-3">                          
                           <div class="row">
                             <div class="col-md-2"></div>
                             <div class="col-md-4">
                               <img src="assets/images/products/ac_stabilizer.png">
                             </div>
                             <div class="col-md-4">
                               <h1>Refrigerator Stabilizers</h1>
                             </div>
                             <div class="col-md-2"></div>
                           </div>  

                            <div class="row">
                               <div class="col-md-4"></div>
                               <div class="col-md-8">
                           <p style="margin-top: 20px;"><a href=""> Upto 300 L</a></p>
                           <p><a href=""> Upto 450 L</a></p>
                               </div>
                           </div>         
                       </div>


                       <div class="col-md-3">                          
                           <div class="row">
                             <div class="col-md-2"></div>
                             <div class="col-md-4">
                               <img src="assets/images/products/ac_stabilizer.png">
                             </div>
                             <div class="col-md-4">
                               <h1>TV Stabilizers</h1>
                             </div>
                             <div class="col-md-2"></div>
                           </div>  

                            <div class="row">
                               <div class="col-md-4"></div>
                               <div class="col-md-8">
                                    <p style="margin-top: 20px;"><a href=""> Upto 32 Inches</a></p>
                           <p><a href=""> Upto 70 Inches</a></p>
                               </div>
                           </div>         
                       </div>


                       <div class="col-md-3">                          
                           <div class="row">
                             <div class="col-md-2"></div>
                             <div class="col-md-4">
                               <img src="assets/images/products/ac_stabilizer.png">
                             </div>
                             <div class="col-md-4">
                               <h1>Mainline Stabilizers</h1>
                             </div>
                             <div class="col-md-2"></div>
                           </div>  

                            <div class="row">
                               <div class="col-md-4"></div>
                               <div class="col-md-8">
                                    <p style="margin-top: 20px;"><a href="">Upto 5KVA</a></p>
                           <p><a href=""> 8 - 10KVA</a></p>
                               </div>
                           </div>         
                       </div>


                   </div>
                   <button class="view_all_btn">View all products</button>
               </div>
           </div>
       </div> -->

           <?php
     require 'connect.php';
      $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='3' AND status='1'");
      $sql->execute();
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      if($sql->rowCount()>0){
        foreach (($sql->fetchAll()) as $key => $row) {


          echo '<div class="product_details">
          <div class="container">
            
             <div class="row">
              <div class="col-md-6 text-center" style="padding-left:50px;padding-right:50px;">
                <img src="admin/products/'.$row['pro_image_1'].'">
              </div>
              <div class="col-md-6 product_details_content">
                <h1>'.$row['prod_name'].'</h1>
                <p>'.$row['prod_details'].'</p>

                    <div class="row" style="margin-top: 60px">
                      <div class="col-md-2"></div>
                      <div class="col-md-4 text-center">
                        <a href="single_product.php?prod_id='.$row['prod_id'].'"><button class="brochure_btn" >View Product</button></a>
                      </div>
                      <div class="col-md-4 text-center">
                        <button class="quote_button">Get A Quote</button>
                      </div>
                      <div class="col-md-2"></div>
                    </div>

              </div>
             </div>

            
            <div class="product_features">
             <div class="row">
              <div class="col-md-2"></div>
                 
                 <div class="col-md-8">
                  <div class="row">
                    <div class="col-md-3 text-center feature_divs">
                      <img src="assets/images/products/highly_reliable.png">
                    </div>
                    <div class="col-md-3 text-center feature_divs">
                      <img src="assets/images/products/selectable_charging.png">
                    </div>
                    <div class="col-md-3 text-center feature_divs">
                      <img src="assets/images/products/cold_start.png">
                    </div>
                    <div class="col-md-3 text-center feature_divs">
                      <img src="assets/images/products/AVR.png">
                    </div>
                  </div>
                 </div>

              <div class="col-md-2"></div>
             </div>
             </div>

          </div>
        </div>';

        }
      }

      ?>








	</div>






<?php site_footer(); ?>
