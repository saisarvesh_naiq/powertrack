<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/site_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
<style>
.container-outer img{
margin-top:-55px;
height:100px;
}
</style>
    <div class="page-wrapper">
		<!-- Header Here -->
		<?php bottom_menu(); ?>
		<?php site_top_menu(); ?>
		<?php slide_slider(); ?>                   
<!--End mainmenu area-->    

<!--Start breadcrumb area-->     
<section class="breadcrumb-area" style="background-image: url(css/images/resources/breadcrumb-bg.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="inner-content clearfix">
                    <div class="title float-left">
                       <h1>About Company</h1>
                    </div>
                    <div class="breadcrumb-menu float-right">
                        <ul class="clearfix">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">About</li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>
<!--End breadcrumb area-->

<!--Start WhoWe Are Area-->
<section class="whowe-are-area">
    <div class="container">
        <div class="sec-title text-center">
            <p>Who We Are</p>
            <div class="title">Welcome to Powertrac Corporation</div>
            <div class="border-box center"></div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="company-info-box">
                    <p>We continue to drive forward, offering new technology, products, and business methods. Our vast product offering encompasses electrical, Critical Power Backup, Power Storage, Power Generation, and network power systems and components.</p>
                    <h3>Vinay Verma</h3>
                    <span>CEO & Founder</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="whowe-are-image">
                    <img src="34.jpg" alt="Awesome Image">    
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="whowe-are-image">
                    <img src="36.jpg" alt="Awesome Image">    
                </div>
            </div>
        </div>
        <div class="row whowe-are">
            <!--Start Single Whowe Are Box-->
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-whowe-are-box text-center">
                    <div class="count-box">01</div>
                    <div class="icon">
                        <span class="icon-null-2"></span>
                    </div>
                    <div class="text">
                        <h3>Mission</h3>
                        <p>Powertrac Corporation is committed to providing the highest quality products, competitively priced, with services exceeding our customers’ expectations.</p>
                    </div>
                </div>
            </div>
            <!--End Single Whowe Are Box-->
            <!--Start Single Whowe Are Box-->
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-whowe-are-box text-center">
                    <div class="count-box">02</div>
                    <div class="icon">
                        <span class="icon-null-3"></span>
                    </div>
                    <div class="text">
                        <h3>VISION</h3>
                        <p>Quality and Uninterrupted power is a cornerstone of our quality of life. There will be a continuing need for products and services to harness its use.</p>
                    </div>
                </div>
            </div>
            <!--End Single Whowe Are Box-->
            <!--Start Single Whowe Are Box-->
            <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                <div class="single-whowe-are-box text-center">
                    <div class="count-box">03</div>
                    <div class="icon">
                        <span class="icon-career"></span>
                    </div>
                    <div class="text">
                        <h3>QUALITY POLICY</h3>
                        <p>The policy of Powertrac Corporation is to empower all employees to create, implement, and improve our services to meet and exceed external and internal customer expectations.</p>
                    </div>
                </div>
            </div>
            <!--End Single Whowe Are Box-->
        </div>
    </div>    
</section> 
<!--End WhoWe Are Area--> 

<!--Stsrt Mission Vision Area-->
<section class="mission-vision-area">
    <div class="container-fluid inner-content">
        <div class="row mar0">
            <div class="col-xl-6 pd0">
                <div class="video-holder-box">
                    <div class="img-holder">
                        <img src="css/images/resources/video-gallery-bg.jpg" alt="Awesome Image">
                        <div class="icon-holder">
                            <div class="icon">
                                <div class="inner text-center">
                                    <a class="html5lightbox wow zoomIn" data-wow-delay="300ms" data-wow-duration="1500ms" title="Solartech Video Gallery" href="https://www.youtube.com/watch?v=p25gICT63ek">
                                        <span class="icon-null-4"></span>
                                    </a>
                                </div>   
                            </div>
                        </div>
                    </div>    
                </div>   
            </div>
            <div class="col-xl-6 pd0">
                <div class="mission-vision-content">
                    <div class="row mar0">
                        <div class="col-xl-6 col-lg-6 col-md-6 pd0">
                            <div class="single-box">
                                <img src="css/images/resources/mission.jpg" alt="Awesome Image">
                                <div class="title">
                                    <h3>Our Mission</h3>
                                </div>
                                <div class="overlay-content">
                                    <div class="top">
                                        <div class="icon">
                                            <span class="icon-null-5"></span>
                                        </div>
                                        <div class="big-title">m</div>
                                    </div>
                                    <div class="text">
                                        <h3>Our Mission</h3>
                                        <p>When our power of choice is untrammelled and nothing prevents our being able to do what we like best every pleasure to be welcomed.</p>
                                    </div>    
                                </div>
                            </div>    
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 pd0">
                            <div class="single-box">
                                <img src="css/images/resources/vision.jpg" alt="Awesome Image">
                                <div class="title">
                                    <h3>Our Vision</h3>
                                </div>
                                <div class="overlay-content">
                                    <div class="top">
                                        <div class="icon">
                                            <span class="icon-career"></span>
                                        </div>
                                        <div class="big-title">v</div>
                                    </div>
                                    <div class="text">
                                        <h3>Our Vision</h3>
                                        <p>When our power of choice is untrammelled and nothing prevents our being able to do what we like best every pleasure to be welcomed.</p>
                                    </div>    
                                </div>
                            </div>    
                        </div>     
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Mission Vision Area-->

<!--Start Choose area-->
<section class="choose-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="why-whoose-content">
                    <div class="sec-title">
                        <p>Why Choose us</p>
                        <div class="title">How to choose<br> better one for your place</div>
                        <div class="border-box"></div>
                    </div>
                    <div class="inner-content">
                        <p>Our power of choice untrammelled when nothing saying through shrinking prevents our being able to do what we like best.</p>
                        <ul>
                            <li><span class="icon-smile"></span>Free, Friendly Service</li>
                            <li><span class="icon-smile"></span>Educational Approach</li>
                            <li><span class="icon-smile"></span>Engaging, Online Platform</li>
                            <li><span class="icon-smile"></span>Multiple Offers to Choose</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="whychoose-right-content">
                    <ul>
                        <li>
                            <div class="icon">
                                <span class="icon-save"></span>    
                            </div>
                            <div class="text">
                                <h3>Efficiency & Power</h3>
                                <p>Certain circumstances and owing to the claims of duty or the obligations of business it will frequently.</p>
                            </div>
                        </li>
                        <li>
                            <div class="icon">
                                <span class="icon-label"></span>    
                            </div>
                            <div class="text">
                                <h3>Warranty & Gurantee</h3>
                                <p>Indignation and dislike men who are beguiled demoralized by the charms of pleasure of the moment.</p>
                            </div>
                        </li>
                        <li>
                            <div class="icon">
                                <span class="icon-money-1"></span>    
                            </div>
                            <div class="text">
                                <h3>Bankability</h3>
                                <p>Have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters.</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Choose area-->

<!--Start Fact Counter Area-->
<section class="fact-counter-area" style="background-image:url(css/images/parallax-background/fact-counter-bg.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <ul class="fact-counter style2">
                    <li class="single-fact-counter style2 wow fadeInLeft" data-wow-delay="100ms" data-wow-duration="1500ms">
                        <div class="count-box">
                            <h1>
                                <i class="flaticon-add"></i>
                                <span class="timer" data-from="1" data-to="5258" data-speed="5000" data-refresh-interval="50">5258</span>
                            </h1>
                            <div class="title">
                                <h3>5.258 Projects Completed<br> Successfully</h3>
                            </div>
                        </div>
                    </li>
                    <li class="single-fact-counter style2 wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="count-box">
                            <h1>
                                <i class="flaticon-percentage"></i>
                                <span class="timer" data-from="1" data-to="97" data-speed="5000" data-refresh-interval="50">97</span>
                            </h1>
                            <div class="title">
                                <h3>Positive Feedback<br> By 97.41% of Customers</h3>
                            </div>
                        </div>
                    </li>
                    <li class="single-fact-counter style2 wow fadeInLeft" data-wow-delay="500ms" data-wow-duration="1500ms">
                        <div class="count-box">
                            <h1>
                                <i class="flaticon-add"></i>
                                <span class="timer" data-from="1" data-to="30" data-speed="5000" data-refresh-interval="50">30</span>
                            </h1>
                            <div class="title">
                                <h3>Have 30+ Years Experience<br> in this Field</h3>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>  
<!--Start Fact Counter Area-->  
<!--Start Brand area-->  
 <section class="dark-bg-three sec-pad-with-content-margin-30 home-five">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="video-box-one">
                            <img src="css/images/home-pages/video-1-1.jpg" alt="">
                            <div class="box">
                                <a href="https://www.youtube.com/" class="video-popup hvr-pulse-grow">
                                    <i class="fa fa-play"></i><!-- /.fa fa-play -->
                                </a>
                            </div><!-- /.box -->
                        </div><!-- /.video-box-one -->
                    </div><!-- /.col-lg-5 -->
                    <div class="col-lg-7">
                        <div class="sec-title-five mb-0">
                                <h2 class="light">
								Our 
                                        <span class="bold-text">Testimonials</span>
                                    
                                </h2>
                        </div><!-- /.sec-title-four -->
                        <div class="testimonials-carousel-three owl-theme owl-carousel" data-nav="true" data-dots="false">
                            <div class="item">
                                <div class="single-testimonial-style-two">
                                    <div class="top-box">
                                        <p>Mr Vinay Verma of Powertrac has carried out work of invertors’ installation for my house and office. Since then we were least bothered about electricity failure in our colony. He has a very prompt technically sound service team which ensures minimum break down time. We are very happy with the work quality, completion schedule and work ethic was found to be of high standards. More so, we were highly impressed by the helpful attitude and genuine service of Mr Vinay Verma. We would highly recommend Powertrac as invertor installation center.</p>
                                        <i class="energy-icon-quote box-icon"></i><!-- /.box-icon -->
                                    </div><!-- /.top-box -->
                                    <div class="name-box">
                                        <div class="image-block">
                                            <img src="images/home-pages/testi-1-1.jpg" alt="">
                                        </div><!-- /.image-block -->
                                        <div class="text-block">
                                            <h3>Soiru A Nayak </h3>
                                            <span>Sai Fabricators</span>
                                        </div><!-- /.text-block -->
                                    </div><!-- /.name-box -->
                                </div><!-- /.single-testimonial-style-two -->
                            </div><!-- /.item -->
                                                        <div class="item">
                                <div class="single-testimonial-style-two">
                                    <div class="top-box">
                                        <p>Mr Vinay Verma of Powertrac has carried out work of invertors’ installation for my house and office. Since then we were least bothered about electricity failure in our colony. He has a very prompt technically sound service team which ensures minimum break down time. We are very happy with the work quality, completion schedule and work ethic was found to be of high standards. More so, we were highly impressed by the helpful attitude and genuine service of Mr Vinay Verma. We would highly recommend Powertrac as invertor installation center.</p>
                                        <i class="energy-icon-quote box-icon"></i><!-- /.box-icon -->
                                    </div><!-- /.top-box -->
                                    <div class="name-box">
                                        <div class="image-block">
                                            <img src="images/home-pages/testi-1-1.jpg" alt="">
                                        </div><!-- /.image-block -->
                                        <div class="text-block">
                                            <h3>Soiru A Nayak </h3>
                                            <span>Sai Fabricators</span>
                                        </div><!-- /.text-block -->
                                    </div><!-- /.name-box -->
                                </div><!-- /.single-testimonial-style-two -->
                            </div><!-- /.item --><!-- /.item -->
                                                        <div class="item">
                                <div class="single-testimonial-style-two">
                                    <div class="top-box">
                                        <p>Mr Vinay Verma of Powertrac has carried out work of invertors’ installation for my house and office. Since then we were least bothered about electricity failure in our colony. He has a very prompt technically sound service team which ensures minimum break down time. We are very happy with the work quality, completion schedule and work ethic was found to be of high standards. More so, we were highly impressed by the helpful attitude and genuine service of Mr Vinay Verma. We would highly recommend Powertrac as invertor installation center.</p>
                                        <i class="energy-icon-quote box-icon"></i><!-- /.box-icon -->
                                    </div><!-- /.top-box -->
                                    <div class="name-box">
                                        <div class="image-block">
                                            <img src="images/home-pages/testi-1-1.jpg" alt="">
                                        </div><!-- /.image-block -->
                                        <div class="text-block">
                                            <h3>Soiru A Nayak </h3>
                                            <span>Sai Fabricators</span>
                                        </div><!-- /.text-block -->
                                    </div><!-- /.name-box -->
                                </div><!-- /.single-testimonial-style-two -->
                            </div><!-- /.item --><!-- /.item -->
                        </div><!-- /.testimonials-carousel-three -->
                    </div><!-- /.col-lg-7 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section> 
<?php site_footer(); ?>