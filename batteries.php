<?php
include_once("template-parts/header.php");
include_once("template-parts/footer.php");
include_once("template-parts/top_menu.php");
include_once("template-parts/main_header.php");
include_once("template-parts/industrial_batteries_slider.php");
include_once("template-parts/slide_slider.php");
site_header(); ?>
<body>
  <style>
    .container-outer img{
      margin-top:-55px;
      height:100px;
    }
  </style>
  <div class="page-wrapper">
    <!-- Header Here -->
    <?php bottom_menu(); ?>
    <?php site_top_menu(); ?>
    <?php slide_slider(); ?>


    <div style="margin-top: 170px;">
    	<?php industrial_batteries_slider(); ?>
    </div>
    

    <div class="container">
      

      <div class="product_intro">
       <div class="row">
        <div class="col-md-12 text-center">
          <h1><span style="color: #b7c919;">SMF, VRL & Industrial Batteries</span> Powertrac Corporation</h1>
          <p>After 10 years of experience in VRL with Shin Kobe, Exide has finally launched new Exide Powersafe NXT
          Exide Industries Ltd. India (EIL), Motive Power cells comes in a huge range of the Normal and the Enhanced version. The design has been optimized to maximize the utilization of the positive and negative electrodes. Usage of Advanced components for the manufacturing of electrodes gives higher discharge efficiency.</p>
        </div>
      </div>
    </div>



     <?php
     require 'connect.php';
      $sql= $conn->prepare("SELECT * FROM products_db WHERE main_category='6' AND status='1'");
      $sql->execute();
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      if($sql->rowCount()>0){
        foreach (($sql->fetchAll()) as $key => $row) {

          echo '<div class="product_details">
       <div class="container">
        
         <div class="row">
          <div class="col-md-6 text-center">
           <img src="admin/products/'.$row['pro_image_1'].'">
         </div>
         <div class="col-md-6 product_details_content">
           <h1>'.$row['prod_name'].'</h1>
           <p>'.$row['prod_details'].'</p>

          <div class="row" style="margin-top: 60px">
           <div class="col-md-2"></div>
           <div class="col-md-4 text-center">
            <a href="single_product.php?prod_id='.$row['prod_id'].'"><button class="brochure_btn" >View Product</button></a>
          </div>
          <div class="col-md-4 text-center">
            <button class="quote_button">Get A Quote</button>
          </div>
          <div class="col-md-2"></div>
        </div>

      </div>
    </div>

    
    <div class="product_features">
     <div class="row">
      <div class="col-md-2"></div>
      
      <div class="col-md-8">
        <div class="row">
         <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/highly_reliable.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/selectable_charging.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/cold_start.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/AVR.png">
        </div>
      </div>
    </div>

    <div class="col-md-2"></div>
  </div>
</div>

</div>
</div>';
}
}

?>



     







<div class="product_intro">
  <div class="row">
    <div class="col-md-12">
      <h1><span style="color: #b7c919;">POWER & INFRASTRUCTURAL PROJECTS</span></h1>
      <p>For the core industries, Exide has a standalone range of five standby batteries benchmarked to the highest international standards. Referred to as the "formidable five" in industrial circles, these are the top of the line.<br>
        1. Exide Plante Batteries with Transparent SAN Container<br>
        2. Exide TBS Tubular Batteries in Transparent SAN Container<br>
        3. Exide SMF Powersafe VRLA Batteries<br>
        4. Exide Tubular Batteries (both HDP and NDP) in PP & Transparent SAN Containers<br>
        5. Exide Gel Tubular Batteries<br>
        These are the heart of core industries like Power, Oil & Gas, Steel, Cement, Telecom, Railways, Urban Metro, IT, Financial Institutions, and Non-Conventional Energy Systems.

      </p>
    </div>
  </div>
</div>





<div class="product_details">
  <div class="container">
    
   <div class="row">
    <div class="col-md-6 text-center">
      <img src="assets/images/products/NDP_and_HDP.jpg" style="padding-top: 40px;">
    </div>
    <div class="col-md-6 product_details_content">
      <h1>NDP & HDP TUBULAR RANGE</h1>
      <p>Exide Tubular batteries in PP & Transparent SAN containers are time-tested and have an outstanding track record. The batteries have Tubular plates specially cast in High Pressure HADI machines to battle corrosion effectively and ensure longevity. These low-maintenance batteries are extremely abuse-resistant and can operate under arduous conditions with great reliability and efficiency.<br>
        Exide Tubular cells have built their reputation on their long standing ability to provide instant power to equipment. These batteries have excellent discharge performance, besides offering long and reliable service life. These are ideal for frequent charge/ discharge cycles and have low self-discharge, superior all round voltage profile and energy (Wh) output. These Tubular batteries recharge rapidly and have higher Ampere-hour and Watt-hour efficiencies. These batteries conform to IS 1651: 2013.
        Exide Tubular batteries are ideal for cyclic applications and are very commonly used in Transmission & Distribution, Power, Cement, Steel, Oil & Gas and Petrochemical sectors.
      </p>
      <p><strong>APPLICATION</strong></p>
      <ul>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Power Plants</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Substations</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>UPS Systems</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Process Industries</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>C & I</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Solar</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Railway Signalling</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Telecommunication</p></li>
      </ul>

      <div class="row" style="margin-top: 60px">
        <div class="col-md-2"></div>
        <div class="col-md-4 text-center">
          <button class="brochure_btn">View Brochure</button>
        </div>
        <div class="col-md-4 text-center">
          <button class="quote_button">Get A Quote</button>
        </div>
        <div class="col-md-2"></div>
      </div>

    </div>
  </div>

  
  <div class="product_features">
   <div class="row">
    <div class="col-md-2"></div>
    
    <div class="col-md-8">
      <div class="row">
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/highly_reliable.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/selectable_charging.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/cold_start.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/AVR.png">
        </div>
      </div>
    </div>

    <div class="col-md-2"></div>
  </div>
</div>

</div>
</div>



<div class="product_details2">
  <div class="container">
    
   <div class="row">
    <div class="col-md-6 product_details_content">
      <h1>TBS / OPZS RANGE</h1>
      <p style="padding-top: 20px;">A truly reliable standby battery needs to consider a divergent mix of variables like load, its duration and DC bus voltage window, expected frequency and depth of discharge, ambient temperature, criticality of operations and charging facility. Exide TBS tubular batteries are a great choice since these are tailored to meet all this need. These are rugged batteries with Exide Torr Tubular technology. The facility of measuring specific gravity of the electrolyte, voltage and temperature of cells allows anomalies to be detected well in advance. This makes preventive maintenance very easy and readiness during emergency discharge is always ensured.
      Exide TBS batteries offer a service life of around 15 years in diverse standby float applications. The batteries have been designed to be user-friendly, require very low maintenance and can withstand high ambient temperatures. These batteries conform to IS 1651 : 2013</p>
      <p><strong>APPLICATION</strong></p>
      <ul>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Power Plants</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Substations</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Oil and Gas Pipelines</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Switchgear Operations</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p> UPS Systems</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p> Non-Conventional Energy Systems</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Railway Signalling</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Telecommunication</p></li>
      </ul>

      <div class="row" style="margin-top: 60px">
        <div class="col-md-2"></div>
        <div class="col-md-4 text-center">
          <button class="brochure_btn">View Brochure</button>
        </div>
        <div class="col-md-4 text-center">
          <button class="quote_button">Get A Quote</button>
        </div>
        <div class="col-md-2"></div>
      </div>

    </div>
    <div class="col-md-6 text-center">
      <img src="assets/images/products/TBS-OPZS.jpg">
    </div>
  </div>

  
  <div class="product_features">
   <div class="row">
    <div class="col-md-2"></div>
    
    <div class="col-md-8">
      <div class="row">
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/highly_reliable.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/selectable_charging.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/cold_start.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/AVR.png">
        </div>
      </div>
    </div>

    <div class="col-md-2"></div>
  </div>
</div>

</div>
</div>



<div class="product_intro">
  <div class="row">
    <div class="col-md-12">
      <h1><span style="color: #b7c919;">ONLINE/OFFLINE UPS SYSTEMS</span></h1>
      <p>To facilitate smooth functioning in banking, insurance and in other service institutions, it has become necessary to maintain the latest financial software packages, global networking facilities and user-friendly ATM services. This, in turn, has led to a large-scale computerization and consequent need for back-up power or Uninterrupted Power Supply (UPS) Systems. The integral part of a UPS is the battery bank. The battery acts as a back-up power source that supports the UPS system. The most popular battery system being used with UPS is the Lead Acid battery system. Broadly, there are two types of Lead Acid batteries suitable for SMF VRLA batteries and Tubular batteries. VRLA batteries are flat plate batteries that do not require periodic topping-up with water and normally do not emit any fumes or gases on a continuous basis. They are best suitable for applications where back-up requirement is usually short, normally not exceeding 30 mins to 1 hour. These batteries are automatic choice where ambient temperature is not very high and space is a constraint. Exide's Powersafe VRLA batteries provide ideal back-up power for UPS systems. Tubular batteries use a special technology by which the active material is encapsulated in polyester tubes to prevent shedding. These types of batteries are recommended for back-up power for UPS where environmental conditions are tough and high ambient temperature are common. Batteries for Institutional UPS System application are available in the following ranges</p>
    </div>
  </div>
</div>



<div class="product_details">
  <div class="container">
    
   <div class="row">
    <div class="col-md-6 text-center">
      <img src="assets/images/products/flooded_6el_range.jpg" style="padding-top: 40px;">
    </div>
    <div class="col-md-6 product_details_content">
      <h1>Flooded 6EL Range</h1>
      <p>The new EL range is manufactured as per Exide's proprietary Torr Tubular Technology using one of the world's most exclusive, advanced and state-of-the-art "HADI" high pressure spine casting (at 100 bar) machines, which is not commonly available. With our hi-tech R&D centre at Kolkata, Exide has developed the high corrosion-resistant and robust spine technology using the HADI process which ensures a superfine grain structure for strength, long life and highest reliability.</p>
      <p><strong>APPLICATIONS OF FLOODED 6EL RANGE</strong></p>
      <ul>
        <li style="list-style-type:circle;margin-top: -30px;"><p>UPS Systems</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Telecommunication Systems</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Office Automation Equipment</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Fire Alarm and Security Systems</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Electronic PABX Systems</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Cable Television Equipment</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p> Electronic Attendance and Cash Registers</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Process Instrumentation and Control</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Railway Signalling</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p> Power Plants and Substations</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Cellular Phones and Pagers (Base Stations and Transmitters)</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Geophysical Equipment</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>PCO Monitors (Electronic)</p></li>
      </ul>

      <div class="row" style="margin-top: 60px">
        <div class="col-md-2"></div>
        <div class="col-md-4 text-center">
          <button class="brochure_btn">View Brochure</button>
        </div>
        <div class="col-md-4 text-center">
          <button class="quote_button">Get A Quote</button>
        </div>
        <div class="col-md-2"></div>
      </div>

    </div>
  </div>

  
  <div class="product_features">
   <div class="row">
    <div class="col-md-2"></div>
    
    <div class="col-md-8">
      <div class="row">
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/highly_reliable.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/selectable_charging.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/cold_start.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/AVR.png">
        </div>
      </div>
    </div>

    <div class="col-md-2"></div>
  </div>
</div>

</div>
</div>



<div class="product_details2">
  <div class="container">
    
   <div class="row">
    <div class="col-md-6 product_details_content">
      <h1>Flooded 6EL plus Range</h1>
      <p style="padding-top: 20px;">The new EL plus range is manufactured as per Exide's proprietary Torr Tubular Technology using one of the world's most exclusive, advanced and state of the art "HADI" high pressure spine casting (at 100 bar) machines. The designed float life of Exide EL plus extends well beyond 5 years and on an average 8-10 years of EL plus Range. The cyclic life of EL plus is also very high to the tune of 1200 cycles at 80% depth of discharge. The combination of low antimony alloy and ceramic vent plug keeps the water addition requirement very low.</p>

      <p><strong>APPLICATIONS OF FLOODED 6EL PLUS RANGE</strong></p>
      <ul>
        <li style="list-style-type:circle;margin-top: -10px;"><p>UPS Systems</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Telecommunication Systems</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Office Automation Equipment</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Fire Alarm and Security Systems</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Electronic PABX Systems</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Cable Television Equipment</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p> Electronic Attendance and Cash Registers</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Process Instrumentation and Control</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Railway Signalling</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p> Power Plants and Substations</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Cellular Phones and Pagers (Base Stations and Transmitters)</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Geophysical Equipment</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>PCO Monitors (Electronic)</p></li>
      </ul>

      <div class="row" style="margin-top: 60px">
        <div class="col-md-2"></div>
        <div class="col-md-4 text-center">
          <button class="brochure_btn">View Brochure</button>
        </div>
        <div class="col-md-4 text-center">
          <button class="quote_button">Get A Quote</button>
        </div>
        <div class="col-md-2"></div>
      </div>

    </div>
    <div class="col-md-6 text-center">
      <img src="assets/images/products/flooded_6el_plus_range.jpg">
    </div>
  </div>

  
  <div class="product_features">
   <div class="row">
    <div class="col-md-2"></div>
    
    <div class="col-md-8">
      <div class="row">
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/highly_reliable.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/selectable_charging.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/cold_start.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/AVR.png">
        </div>
      </div>
    </div>

    <div class="col-md-2"></div>
  </div>
</div>

</div>
</div>


<div class="product_details">
  <div class="container">
    
   <div class="row">
    <div class="col-md-6 text-center">
      <img src="assets/images/products/flooded_6el_master_range.jpg" style="padding-top: 40px;">
    </div>
    <div class="col-md-6 product_details_content">
      <h1>Flooded 6EL Master Range</h1>
      <p>The new EL Master range are manufactured as per Exide's proprietary Torr technology using one of the world's most exclusive, advanced and state-of-the-art 'HADI' high pressure spine casting (at 100 bar) machines, which is not commonly available. Exide has developed the high'corrosion resistant and robust spine technology using the HADI process, which ensures a super-fine grain structure for strength, long life and high reliability.</p>

      <p><strong>APPLICATIONS OF FLOODED 6EL MASTER RANGE</strong></p>
      <ul>
        <li style="list-style-type:circle;margin-top: -30px;"><p>UPS and Inverter Systems</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Telecommunication Systems</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Office Automation Equipment</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Fire Alarm and Security Systems</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Electronic PABX Systems</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Cable Television Equipment</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p> Electronic Attendance and Cash Registers</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Process Instrumentation and Control</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Railway Signalling</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p> Power Plants and Substations</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Cellular Phones and Pagers (Base Stations and Transmitters)</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Geophysical Equipment</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>PCO Monitors (Electronic)</p></li>
      </ul>


      <div class="row" style="margin-top: 60px">
        <div class="col-md-2"></div>
        <div class="col-md-4 text-center">
          <button class="brochure_btn">View Brochure</button>
        </div>
        <div class="col-md-4 text-center">
          <button class="quote_button">Get A Quote</button>
        </div>
        <div class="col-md-2"></div>
      </div>

    </div>
  </div>

  
  <div class="product_features">
   <div class="row">
    <div class="col-md-2"></div>
    
    <div class="col-md-8">
      <div class="row">
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/highly_reliable.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/selectable_charging.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/cold_start.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/AVR.png">
        </div>
      </div>
    </div>

    <div class="col-md-2"></div>
  </div>
</div>

</div>
</div>



<div class="product_details2">
  <div class="container">
    
   <div class="row">
    <div class="col-md-6 product_details_content">
      <h1>VRLA Exide Powersafe plus Range</h1>
      <p style="padding-top: 20px;">Presenting NEW Improved Exide Powersafe Plus, Sealed, Maintenance-free VRLA batteries with enhanced design features which give a higher performance and reliability, better suited to Indian conditions. Exide Powersafe is a product of collaborative efforts of in-house R&D of Exide Industries Ltd., the largest lead-acid battery manufacturer of India and M/s. Shin-Kobe Electric Machinery Co., Japan, the makers of world renowned Hitachi batteries. No wonder it is approved by all major OEMs and Institutional customers and is considered to be the Power Pack of the future.</p>


      <p><strong>APPLICATIONS</strong></p>
      <ul>
        <li style="list-style-type:circle;margin-top: -10px;"><p>New Improved Sealed Maintenance free</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Free from Orientation Constraints</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Eco-Friendly</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Easy Handling and No Installation Constraints</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Ready to Use</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Long Service Life</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Low Self-Discharge</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Excellent Charge Retention and Recovering Ability</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Superior High Rate Discharge</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Wide Range</p></li>

      </ul>


      <div class="row" style="margin-top: 60px">
        <div class="col-md-2"></div>
        <div class="col-md-4 text-center">
          <button class="brochure_btn">View Brochure</button>
        </div>
        <div class="col-md-4 text-center">
          <button class="quote_button">Get A Quote</button>
        </div>
        <div class="col-md-2"></div>
      </div>

    </div>
    <div class="col-md-6 text-center">
      <img src="assets/images/products/VRLA_exide.jpg">
    </div>
  </div>

  
  <div class="product_features">
   <div class="row">
    <div class="col-md-2"></div>
    
    <div class="col-md-8">
      <div class="row">
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/highly_reliable.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/selectable_charging.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/cold_start.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/AVR.png">
        </div>
      </div>
    </div>

    <div class="col-md-2"></div>
  </div>
</div>

</div>
</div>


<div class="product_details">
  <div class="container">
    
   <div class="row">
    <div class="col-md-6 text-center">
      <img src="assets/images/products/advance_vrla_exide.jpg" style="padding-top: 40px;">
    </div>
    <div class="col-md-6 product_details_content">
      <h1>Advance VRLA Exide Powersafe NXT Range</h1>
      <p>After 10 years of experience in VRL with Shin Kobe, we launched the new Exide Powersafe NXT range with excellent cycle life and the unique feature of 5 hours quick recharge option.</p>

      <p><strong>FEATURES OF ADVANCE VRLA EXIDE POWERSAFE NXT RANGE</strong></p>
      <ul>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Deep cycle application</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Fast recovery from deep recharge</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Extended cycle life</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Fast recharge capability</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Excellent charge retention</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>International size</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Free from orientation constraints</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p>Eco-friendly</p></li>
        <li style="list-style-type:circle;margin-top: -30px;"><p> The legendary Exide Torr Tubular reliability now comes maintenance free from the state of the art manufacturing line. Exide launches new Powersafe XHD series with unique Gel Technology mainly for UPS Application. These batteries are designed with Tubular Positive plate & GEL electrolyte. Tubular Positive plates, well established for partial state of charge operation make Powersafe XHD series a durable Battery for deep cycle application even in heavy power cut areas. The Gel electrolyte makes it compact and maintenance free with no topping up for life. Powersafe XHD is a VRLA backed by Tubular Strength.</p></li>
      </ul>

      <div class="row" style="margin-top: 60px">
        <div class="col-md-2"></div>
        <div class="col-md-4 text-center">
          <button class="brochure_btn">View Brochure</button>
        </div>
        <div class="col-md-4 text-center">
          <button class="quote_button">Get A Quote</button>
        </div>
        <div class="col-md-2"></div>
      </div>

    </div>
  </div>

  
  <div class="product_features">
   <div class="row">
    <div class="col-md-2"></div>
    
    <div class="col-md-8">
      <div class="row">
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/highly_reliable.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/selectable_charging.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/cold_start.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/AVR.png">
        </div>
      </div>
    </div>

    <div class="col-md-2"></div>
  </div>
</div>

</div>
</div>



<div class="product_details2">
  <div class="container">
    
   <div class="row">
    <div class="col-md-6 product_details_content">
      <h1>CHLORIDE SAFEPOWER & CHLORIDE DRAGON BATTERY</h1>
      <p style="padding-top: 20px;">Presenting Chloride Safepower (7-12) & Chloride Dragon (4-6), sealed Maintenance Free VRLA Batteries for your UPS & Emergency Lights. These are the product of in-house R&D efforts of Exide Industries LTD. , the largest Lead-Acid Battery manufacturer of India.</p>


      <p><strong>APPLICATIONS</strong></p>
      <ul>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Sealed Maintenance Free</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Free from Orientation Constraints</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Eco-Friendly</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Easy Handling and No Installation Constraints</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Ready to Use</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Long Service Life</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Low Self-Discharge</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Excellent Charge Retention and Recovering Ability</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>Superior High Rate Discharge</p></li>
        <li style="list-style-type:circle;margin-top: -10px;"><p>High Reliability</p></li>

      </ul>

      <div class="row" style="margin-top: 60px">
        <div class="col-md-2"></div>
        <div class="col-md-4 text-center">
          <button class="brochure_btn">View Brochure</button>
        </div>
        <div class="col-md-4 text-center">
          <button class="quote_button">Get A Quote</button>
        </div>
        <div class="col-md-2"></div>
      </div>

    </div>
    <div class="col-md-6 text-center">
      <img src="assets/images/products/chloride_safepowder.jpg">
    </div>
  </div>

  
  <div class="product_features">
   <div class="row">
    <div class="col-md-2"></div>
    
    <div class="col-md-8">
      <div class="row">
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/highly_reliable.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/selectable_charging.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/cold_start.png">
        </div>
        <div class="col-md-3 text-center feature_divs">
          <img src="assets/images/products/AVR.png">
        </div>
      </div>
    </div>

    <div class="col-md-2"></div>
  </div>
</div>

</div>
</div>













</div>































<?php site_footer(); ?>
